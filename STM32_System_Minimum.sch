EESchema Schematic File Version 2
LIBS:Remote_tester-rescue
LIBS:BJT
LIBS:Capacitor
LIBS:Diode_Rectifier
LIBS:Diode_Schottky
LIBS:Diode_Switching
LIBS:Diode_Zener
LIBS:EEPROM
LIBS:Inductor
LIBS:Microcontrollers
LIBS:Miscellaneous_Connectors
LIBS:Miscellaneous_Devices
LIBS:Modules
LIBS:MOSFET
LIBS:MOV
LIBS:Others
LIBS:Power
LIBS:PTC
LIBS:Regulator
LIBS:Resistor
LIBS:TBU
LIBS:TVS
LIBS:onsemi
LIBS:battery_management
LIBS:device
LIBS:opamp
LIBS:Remote_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title "STM32 System Minimum"
Date "2017-08-01"
Rev "1.0.3"
Comp "PT. Fusi Global Teknologi"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4300 4750 2    50   ~ 0
PH0
Text Label 4300 4850 2    50   ~ 0
PH1
Text Label 4300 3000 2    50   ~ 0
BOOT1
Text Label 4300 5150 2    50   ~ 0
BOOT0
Text HLabel 2600 1150 0    50   Input ~ 0
+3.3V
Text HLabel 2450 6150 0    50   Input ~ 0
GND
Text Label 4700 1450 0    50   ~ 0
BOOT1
Text Label 4700 1150 0    50   ~ 0
BOOT0
Text Label 4550 5100 0    50   ~ 0
PH1
Text Label 4550 5650 0    50   ~ 0
PH0
Text Label 6550 3050 0    50   ~ 0
TCK
Text Label 6550 3250 0    50   ~ 0
TMS
Text Label 6550 3350 0    50   ~ 0
NRST
Text Label 6550 3450 0    50   ~ 0
SWO
Text Label 1150 3750 0    50   ~ 0
TMS1
Text Label 4300 5050 2    50   ~ 0
NRST1
Text Label 4300 3100 2    50   ~ 0
SWO1
Text Label 1150 3850 0    50   ~ 0
TCK1
Text Label 6300 1600 0    50   ~ 0
NRST1
$Comp
L VAA #PWR01
U 1 1 578C6408
P 5300 2450
F 0 "#PWR01" H 5300 2400 50  0001 C CNN
F 1 "VAA" H 5300 2600 50  0000 C CNN
F 2 "" H 5300 2450 50  0000 C CNN
F 3 "" H 5300 2450 50  0000 C CNN
	1    5300 2450
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 578AFAF1
P 5450 2100
F 0 "#FLG02" H 5450 2050 50  0001 C CNN
F 1 "PWR_FLAG" H 5450 2300 50  0000 C CNN
F 2 "" H 5450 2100 50  0000 C CNN
F 3 "" H 5450 2100 50  0000 C CNN
	1    5450 2100
	1    0    0    -1  
$EndComp
$Comp
L FERRITE-BEAD FB1
U 1 1 578B0197
P 4850 2550
F 0 "FB1" H 4850 2650 50  0000 C CNN
F 1 "FERRITE-BEAD" H 4850 2450 50  0000 C CNN
F 2 "Ferrite_Beads:FB_0805" H 4750 2570 60  0001 C CNN
F 3 "" H 4850 2670 60  0000 C CNN
F 4 "FERRITE BEAD 40 OHM 0805 1LN" H 4850 2550 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 4850 2550 60  0001 C CNN "Packaging"
F 6 "Laird-Signal Integrity Products" H 4850 2550 60  0001 C CNN "Manufacturer"
F 7 "MI0805K400R-10" H 4850 2550 60  0001 C CNN "Part Number"
F 8 "240-2389-2-ND" H 4850 2550 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/laird-signal-integrity-products/MI0805K400R-10/240-2389-2-ND/806627" H 4850 2550 60  0001 C CNN "1st Distributor Link"
	1    4850 2550
	1    0    0    -1  
$EndComp
$Comp
L NP-1uF C10
U 1 1 578B05E2
P 5450 2850
F 0 "C10" H 5490 2950 50  0000 L CNN
F 1 "1uF" H 5490 2740 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 5488 2700 30  0001 C CNN
F 3 "" H 5450 2850 60  0000 C CNN
F 4 "CAP CER 1UF 16V X7R 0603" H 5450 2850 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5450 2850 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 5450 2850 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71C105KE15D" H 5450 2850 60  0001 C CNN "Part Number"
F 8 "490-10734-2-ND" H 5450 2850 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71C105KE15D/490-10734-2-ND/4905375" H 5450 2850 60  0001 C CNN "1st Distributor Link"
	1    5450 2850
	1    0    0    -1  
$EndComp
$Comp
L HEADER-1X6 P1
U 1 1 578B146B
P 7000 3200
F 0 "P1" H 6900 3550 50  0000 L CNN
F 1 "HEADER-1X6" H 7000 2850 50  0000 C CNN
F 2 "Headers:JST-XH_1x06_Male_Straight_TH" V 6850 3100 60  0001 C CNN
F 3 "" H 7000 3200 60  0000 C CNN
F 4 "CONN HEADER XH TOP 6POS 2.5MM" H 7000 3200 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 7000 3200 60  0001 C CNN "Packaging"
F 6 "B6B-XH-A(LF)(SN)" H 7000 3200 60  0001 C CNN "Part Number"
F 7 "JST Sales America Inc." H 7000 3200 60  0001 C CNN "Manufacturer"
F 8 "455-2271-ND" H 7000 3200 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/jst-sales-america-inc/B6B-XH-A(LF)(SN)/455-2271-ND/1000381" H 7000 3200 60  0001 C CNN "1st Distributor Link"
	1    7000 3200
	1    0    0    -1  
$EndComp
$Comp
L 100K/0.100W R4
U 1 1 578B53C7
P 6700 1300
F 0 "R4" H 6700 1400 50  0000 C CNN
F 1 "100K/0.100W" H 6700 1200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6700 1230 30  0001 C CNN
F 3 "" V 6700 1300 30  0000 C CNN
F 4 "RES SMD 100K OHM 5% 1/10W 0603" H 6700 1300 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 6700 1300 60  0001 C CNN "Packaging"
F 6 "Yageo" H 6700 1300 60  0001 C CNN "Manufacturer"
F 7 "RC0603JR-07100KL" H 6700 1300 60  0001 C CNN "Part Number"
F 8 "311-100KGRTR-ND" H 6700 1300 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-07100KL/311-100KGRTR-ND/726698" H 6700 1300 60  0001 C CNN "1st Distributor Link"
	1    6700 1300
	0    1    1    0   
$EndComp
Text Notes 7600 2650 2    50   ~ 0
Reset Circuit
Text Notes 7600 3950 2    50   ~ 0
STM32 Programmer Port
$Comp
L NP-22pF C19
U 1 1 578B7E40
P 4750 5950
F 0 "C19" H 4790 6050 50  0000 L CNN
F 1 "22pF" H 4790 5840 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 4788 5800 30  0001 C CNN
F 3 "" H 4750 5950 60  0000 C CNN
F 4 "CAP CER 22PF 50V C0G/NP0 0603" H 4750 5950 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 4750 5950 60  0001 C CNN "Packaging"
F 6 "06035A220JAT2A" H 4750 5950 60  0001 C CNN "Part Number"
F 7 "AVX Corporation" H 4750 5950 60  0001 C CNN "Manufacturer"
F 8 "478-1167-2-ND" H 4750 5950 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/avx-corporation/06035A220JAT2A/478-1167-2-ND/563277" H 4750 5950 60  0001 C CNN "1st Distributor Link"
	1    4750 5950
	1    0    0    -1  
$EndComp
$Comp
L 220/0.100W R3
U 1 1 578B7FF3
P 5150 5100
F 0 "R3" H 5150 5200 50  0000 C CNN
F 1 "220/0.100W" H 5150 5000 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5150 5030 30  0001 C CNN
F 3 "" V 5150 5100 30  0000 C CNN
F 4 "RES SMD 220 OHM 5% 1/10W 0603" H 5150 5100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5150 5100 60  0001 C CNN "Packaging"
F 6 "RC0603JR-07220RL" H 5150 5100 60  0001 C CNN "Part Number"
F 7 "Yageo" H 5150 5100 60  0001 C CNN "Manufacturer"
F 8 "311-220GRTR-ND" H 5150 5100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-07220RL/311-220GRTR-ND/726741" H 5150 5100 60  0001 C CNN "1st Distributor Link"
	1    5150 5100
	1    0    0    -1  
$EndComp
Text Notes 6050 6650 2    50   ~ 0
STM32 System Minimum Circuit
Text HLabel 6600 1000 0    50   Input ~ 0
+3.3V
Text HLabel 5350 1650 0    50   Input ~ 0
GND
Text HLabel 5050 6350 0    50   Input ~ 0
GND
Text HLabel 5050 3250 0    50   Input ~ 0
GND
Text HLabel 4450 2450 0    50   Input ~ 0
+3.3V
Text HLabel 6450 3150 0    50   Input ~ 0
GND
Text HLabel 6600 2200 0    50   Input ~ 0
GND
$Comp
L 10K/0.100W R2
U 1 1 578B1EB9
P 5150 1450
F 0 "R2" H 5150 1550 50  0000 C CNN
F 1 "10K/0.100W" H 5150 1350 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5150 1380 30  0001 C CNN
F 3 "" V 5150 1450 30  0000 C CNN
F 4 "RES SMD 10K OHM 5% 1/10W 0603" H 5150 1450 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5150 1450 60  0001 C CNN "Packaging"
F 6 "Yageo" H 5150 1450 60  0001 C CNN "Manufacturer"
F 7 "RC0603JR-0710KL" H 5150 1450 60  0001 C CNN "Part Number"
F 8 "311-10KGRTR-ND" H 5150 1450 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-0710KL/311-10KGRTR-ND/726700" H 5150 1450 60  0001 C CNN "1st Distributor Link"
	1    5150 1450
	1    0    0    -1  
$EndComp
Text HLabel 4050 3300 2    50   Output ~ 0
PB5
Text Notes 4600 3100 0    50   ~ 0
X7R
Text Notes 5200 3100 0    50   ~ 0
X7R
Text Notes 5500 3100 0    50   ~ 0
X7R
Text Notes 3100 1700 0    50   ~ 0
X7R
Text Notes 3400 1700 0    50   ~ 0
X7R
Text Notes 3700 1700 0    50   ~ 0
X7R
Text Notes 4000 1700 0    50   ~ 0
X7R
Text Notes 6750 2150 0    50   ~ 0
X7R
Text HLabel 4050 4100 2    50   Input ~ 0
PB13
Text HLabel 2950 1750 0    50   Input ~ 0
GND
Text HLabel 1350 3150 0    50   Input ~ 0
SPI1_MOSI
Text HLabel 1350 2950 0    50   Output ~ 0
SPI1_SCK
Text HLabel 1350 4950 0    50   Output ~ 0
PC8
Text HLabel 1350 5050 0    50   Output ~ 0
PC9
$Comp
L NP-100nF C16
U 1 1 591851C6
P 3950 1450
F 0 "C16" H 3990 1550 50  0000 L CNN
F 1 "100nF" H 3990 1340 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3988 1300 30  0001 C CNN
F 3 "" H 3950 1450 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3950 1450 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3950 1450 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 3950 1450 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 3950 1450 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 3950 1450 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3950 1450 60  0001 C CNN "1st Distributor Link"
	1    3950 1450
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C15
U 1 1 59186F40
P 3650 1450
F 0 "C15" H 3690 1550 50  0000 L CNN
F 1 "100nF" H 3690 1340 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3688 1300 30  0001 C CNN
F 3 "" H 3650 1450 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3650 1450 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3650 1450 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 3650 1450 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 3650 1450 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 3650 1450 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3650 1450 60  0001 C CNN "1st Distributor Link"
	1    3650 1450
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C14
U 1 1 59186F86
P 3350 1450
F 0 "C14" H 3390 1550 50  0000 L CNN
F 1 "100nF" H 3390 1340 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3388 1300 30  0001 C CNN
F 3 "" H 3350 1450 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3350 1450 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3350 1450 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 3350 1450 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 3350 1450 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 3350 1450 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3350 1450 60  0001 C CNN "1st Distributor Link"
	1    3350 1450
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C18
U 1 1 59186FD4
P 3050 1450
F 0 "C18" H 3090 1550 50  0000 L CNN
F 1 "100nF" H 3090 1340 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3088 1300 30  0001 C CNN
F 3 "" H 3050 1450 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3050 1450 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3050 1450 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 3050 1450 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 3050 1450 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 3050 1450 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3050 1450 60  0001 C CNN "1st Distributor Link"
	1    3050 1450
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C17
U 1 1 59187022
P 4550 2850
F 0 "C17" H 4590 2950 50  0000 L CNN
F 1 "100nF" H 4590 2740 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 4588 2700 30  0001 C CNN
F 3 "" H 4550 2850 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 4550 2850 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 4550 2850 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 4550 2850 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 4550 2850 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 4550 2850 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 4550 2850 60  0001 C CNN "1st Distributor Link"
	1    4550 2850
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C9
U 1 1 5918708B
P 5150 2850
F 0 "C9" H 5190 2950 50  0000 L CNN
F 1 "100nF" H 5190 2740 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 5188 2700 30  0001 C CNN
F 3 "" H 5150 2850 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 5150 2850 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5150 2850 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 5150 2850 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 5150 2850 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 5150 2850 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 5150 2850 60  0001 C CNN "1st Distributor Link"
	1    5150 2850
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C12
U 1 1 591870E2
P 6700 1900
F 0 "C12" H 6740 2000 50  0000 L CNN
F 1 "100nF" H 6740 1790 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 6738 1750 30  0001 C CNN
F 3 "" H 6700 1900 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 6700 1900 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 6700 1900 60  0001 C CNN "Packaging"
F 6 "Murata Electronics North America" H 6700 1900 60  0001 C CNN "Manufacturer"
F 7 "GRM188R71H104KA93D" H 6700 1900 60  0001 C CNN "Part Number"
F 8 "490-1519-2-ND" H 6700 1900 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 6700 1900 60  0001 C CNN "1st Distributor Link"
	1    6700 1900
	1    0    0    -1  
$EndComp
$Comp
L BUTTON PB29
U 1 1 5919977A
P 7100 1600
F 0 "PB29" H 7250 1710 50  0000 C CNN
F 1 "BUTTON" H 7100 1520 50  0000 C CNN
F 2 "Push_Buttons:Button_4p_W-5.2mm_L-5.2mm_Normal_SMD" H 7150 1610 60  0001 C CNN
F 3 "" H 7100 1600 60  0000 C CNN
F 4 "SWITCH TACTILE SPST-NO 0.02A 15V" H 7100 1600 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 7100 1600 60  0001 C CNN "Packaging"
F 6 "PTS525SM15SMTR2 LFS" H 7100 1600 60  0001 C CNN "Part Number"
F 7 "C&K" H 7100 1600 60  0001 C CNN "Manufacturer"
F 8 "CKN9104TR-ND" H 7100 1600 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/c-k/PTS525SM15SMTR2-LFS/CKN9104TR-ND/1146855" H 7100 1600 60  0001 C CNN "1st Distributor Link"
	1    7100 1600
	1    0    0    -1  
$EndComp
Text HLabel 6450 2950 0    50   Input ~ 0
VCC
Text Notes 6200 3800 0    50   ~ 0
NB: \nUsing VCC = 5V when programming
$Comp
L NP-22pF C11
U 1 1 591EF7A0
P 5550 5950
F 0 "C11" H 5590 6050 50  0000 L CNN
F 1 "22pF" H 5590 5840 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 5588 5800 30  0001 C CNN
F 3 "" H 5550 5950 60  0000 C CNN
F 4 "CAP CER 22PF 50V C0G/NP0 0603" H 5550 5950 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5550 5950 60  0001 C CNN "Packaging"
F 6 "06035A220JAT2A" H 5550 5950 60  0001 C CNN "Part Number"
F 7 "AVX Corporation" H 5550 5950 60  0001 C CNN "Manufacturer"
F 8 "478-1167-2-ND" H 5550 5950 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/avx-corporation/06035A220JAT2A/478-1167-2-ND/563277" H 5550 5950 60  0001 C CNN "1st Distributor Link"
	1    5550 5950
	1    0    0    -1  
$EndComp
Text Notes 4800 6200 0    50   ~ 0
C0G, NPO
Text Notes 5600 6200 0    50   ~ 0
C0G, NPO
$Comp
L CRYSTAL-2 X1
U 1 1 591E3D20
P 5150 5650
F 0 "X1" H 5150 5900 50  0000 C CNN
F 1 "8MHz" H 5150 5800 50  0000 C CNN
F 2 "Packages_Custom:Custom_4p_W-5.0mm_L-3.2mm_SMD" H 5050 5700 60  0001 C CNN
F 3 "" H 5150 5650 60  0000 C CNN
F 4 "CRYSTAL 8.0000MHZ 18PF SMD" H 5150 5650 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5150 5650 60  0001 C CNN "Packaging"
F 6 "ABM3B-8.000MHZ-B2-T" H 5150 5650 60  0001 C CNN "Part Number"
F 7 "Abracon LLC" H 5150 5650 60  0001 C CNN "Manufacturer"
F 8 "535-9720-2-ND" H 5150 5650 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/abracon-llc/ABM3B-8.000MHZ-B2-T/535-9720-2-ND/1873166" H 5150 5650 60  0001 C CNN "1st Distributor Link"
	1    5150 5650
	1    0    0    -1  
$EndComp
$Comp
L FERRITE-BEAD FB2
U 1 1 59226BC3
P 3750 6200
F 0 "FB2" H 3750 6300 50  0000 C CNN
F 1 "FERRITE-BEAD" H 3750 6100 50  0000 C CNN
F 2 "Ferrite_Beads:FB_0805" H 3650 6220 60  0001 C CNN
F 3 "" H 3750 6320 60  0000 C CNN
F 4 "FERRITE BEAD 40 OHM 0805 1LN" H 3750 6200 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3750 6200 60  0001 C CNN "Packaging"
F 6 "Laird-Signal Integrity Products" H 3750 6200 60  0001 C CNN "Manufacturer"
F 7 "MI0805K400R-10" H 3750 6200 60  0001 C CNN "Part Number"
F 8 "240-2389-2-ND" H 3750 6200 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/laird-signal-integrity-products/MI0805K400R-10/240-2389-2-ND/806627" H 3750 6200 60  0001 C CNN "1st Distributor Link"
	1    3750 6200
	1    0    0    -1  
$EndComp
Text Label 4250 5350 2    50   ~ 0
AGND
Text Label 4000 6200 0    50   ~ 0
AGND
Text HLabel 3500 6200 0    50   Input ~ 0
GND
$Comp
L PWR_FLAG #FLG03
U 1 1 5923BEE0
P 4250 6150
F 0 "#FLG03" H 4250 6100 50  0001 C CNN
F 1 "PWR_FLAG" H 4250 6350 50  0000 C CNN
F 2 "" H 4250 6150 50  0000 C CNN
F 3 "" H 4250 6150 50  0000 C CNN
	1    4250 6150
	1    0    0    -1  
$EndComp
Text HLabel 1350 5350 0    50   Output ~ 0
PC12
Text HLabel 1350 4550 0    50   Output ~ 0
SPI1_~SS
$Comp
L PGB1010603 D4
U 1 1 5942C40E
P 7450 3250
F 0 "D4" H 7450 3350 50  0000 C CNN
F 1 "PGB1010603" H 7450 3150 50  0000 C CNN
F 2 "TVSs:PGB1010603MR" H 7450 3250 60  0001 C CNN
F 3 "" H 7450 3250 60  0000 C CNN
F 4 "TVS DIODE 24VWM 150VC 0603" H 7450 3250 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 7450 3250 60  0001 C CNN "Packaging"
F 6 "PGB1010603MR" H 7450 3250 60  0001 C CNN "Part Number"
F 7 "Littelfuse Inc." H 7450 3250 60  0001 C CNN "Manufacturer"
F 8 "F2594TR-ND" H 7450 3250 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/littelfuse-inc/PGB1010603MR/F2594TR-ND/715755" H 7450 3250 60  0001 C CNN "1st Distributor Link"
	1    7450 3250
	0    1    1    0   
$EndComp
Text HLabel 7350 2950 0    50   Input ~ 0
VCC
Text HLabel 7350 3650 0    50   Input ~ 0
GND
$Comp
L TEST-RESCUE-Remote_tester TP32
U 1 1 597FCF11
P 7300 4300
AR Path="/597FCF11" Ref="TP32"  Part="1" 
AR Path="/599C14AC/597FCF11" Ref="TP32"  Part="1" 
F 0 "TP32" H 7300 4400 50  0000 C CNN
F 1 "TEST" H 7300 4200 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 7300 4200 10  0001 C CNN
F 3 "" H 7500 4300 60  0000 C CNN
	1    7300 4300
	1    0    0    -1  
$EndComp
Text Label 7050 4300 0    50   ~ 0
TCK2
Text Label 7050 4600 0    50   ~ 0
TMS2
Text Label 7050 4900 0    50   ~ 0
NRST2
Text Label 7050 5200 0    50   ~ 0
SWO2
$Comp
L TEST-RESCUE-Remote_tester TP33
U 1 1 597FE304
P 7300 4600
AR Path="/597FE304" Ref="TP33"  Part="1" 
AR Path="/599C14AC/597FE304" Ref="TP33"  Part="1" 
F 0 "TP33" H 7300 4700 50  0000 C CNN
F 1 "TEST" H 7300 4500 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 7300 4500 10  0001 C CNN
F 3 "" H 7500 4600 60  0000 C CNN
	1    7300 4600
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP34
U 1 1 597FE33E
P 7300 4900
AR Path="/597FE33E" Ref="TP34"  Part="1" 
AR Path="/599C14AC/597FE33E" Ref="TP34"  Part="1" 
F 0 "TP34" H 7300 5000 50  0000 C CNN
F 1 "TEST" H 7300 4800 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 7300 4800 10  0001 C CNN
F 3 "" H 7500 4900 60  0000 C CNN
	1    7300 4900
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP35
U 1 1 597FE3A0
P 7300 5200
AR Path="/597FE3A0" Ref="TP35"  Part="1" 
AR Path="/599C14AC/597FE3A0" Ref="TP35"  Part="1" 
F 0 "TP35" H 7300 5300 50  0000 C CNN
F 1 "TEST" H 7300 5100 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 7300 5100 10  0001 C CNN
F 3 "" H 7500 5200 60  0000 C CNN
	1    7300 5200
	1    0    0    -1  
$EndComp
Text Notes 7600 5650 2    50   ~ 0
Test Point: Programmer
$Comp
L STM32L100RC-LQFP64 IC11
U 1 1 578C48E8
P 2700 4050
F 0 "IC11" H 1550 5950 50  0000 L CNN
F 1 "STM32L100RC-LQFP64" H 1550 2150 50  0000 L CNN
F 2 "Packages_LQFP:LQFP-64" H 2700 4050 10  0001 C CIN
F 3 "" H 2700 4100 60  0000 C CNN
F 4 "IC MCU 32BIT 128KB FLASH 64LQFP" H 2700 4050 60  0001 C CNN "Description"
F 5 "Tray" H 2700 4050 60  0001 C CNN "Packaging"
F 6 "STMicroelectronics" H 2700 4050 60  0001 C CNN "Manufacturer"
F 7 "STM32L100RBT6" H 2700 4050 60  0001 C CNN "Part Number"
F 8 "497-13631-ND" H 2700 4050 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/stmicroelectronics/STM32L100RBT6/497-13631-ND/3973519" H 2700 4050 60  0001 C CNN "1st Distributor Link"
	1    2700 4050
	1    0    0    -1  
$EndComp
Text HLabel 1350 3550 0    50   Output ~ 0
PA11
Text HLabel 1350 3650 0    50   Input ~ 0
PA12
Text HLabel 1350 2550 0    50   Input ~ 0
PA1
Text HLabel 1350 2650 0    50   Input ~ 0
PA2
Text HLabel 1350 2750 0    50   Input ~ 0
PA3
Text HLabel 1350 2850 0    50   Input ~ 0
PA4
Text HLabel 1350 3050 0    50   Input ~ 0
PA6
Text HLabel 1350 3250 0    50   Input ~ 0
PA8
Text HLabel 1350 3350 0    50   Input ~ 0
TX
Text HLabel 1350 3450 0    50   Input ~ 0
RX
Text HLabel 1350 3950 0    50   Input ~ 0
PA15
Text HLabel 1350 4150 0    50   Input ~ 0
PC0
Text HLabel 4050 3400 2    50   Input ~ 0
SCL
Text HLabel 4050 3500 2    50   Input ~ 0
SDA
Text HLabel 1350 4250 0    50   Input ~ 0
PC1
Text HLabel 1350 4350 0    50   Input ~ 0
PC2
Text HLabel 1350 4450 0    50   Input ~ 0
PC3
Text HLabel 1350 4650 0    50   Input ~ 0
PC5
Text HLabel 1350 4750 0    50   Input ~ 0
PC6
Text HLabel 1350 4850 0    50   Input ~ 0
PC7
Text HLabel 1350 5450 0    50   Input ~ 0
PC13
Text HLabel 1350 5550 0    50   Input ~ 0
PC14
Text HLabel 1350 5650 0    50   Input ~ 0
PC15
Text HLabel 4050 2800 2    50   Input ~ 0
PB0
Text HLabel 4050 2900 2    50   Input ~ 0
PB1
Text HLabel 4100 3200 2    50   Input ~ 0
PB4
Text HLabel 4050 3600 2    50   Input ~ 0
PB8
Text HLabel 4050 3700 2    50   Input ~ 0
PB9
Text HLabel 4050 4000 2    50   Input ~ 0
PB12
Text HLabel 4050 4200 2    50   Input ~ 0
PB14
Text HLabel 4050 4300 2    50   Input ~ 0
PB15
Text HLabel 4050 4550 2    50   Input ~ 0
PD2
$Comp
L DIP-SWITCH-4-PIN-SPST SW1
U 1 1 599FEF1C
P 8400 1200
F 0 "SW1" H 8100 1600 50  0000 C CNN
F 1 "DIP-SWITCH-4-PIN-SPST" H 8400 800 50  0000 C CNN
F 2 "Switches_DIP:Switch_DIP_x4_Slide" H 8400 800 10  0001 C CNN
F 3 "" H 8400 1100 60  0000 C CNN
	1    8400 1200
	1    0    0    -1  
$EndComp
$Comp
L DIP-SWITCH-4-PIN-SPST SW2
U 1 1 599FEF71
P 8400 2450
F 0 "SW2" H 8100 2850 50  0000 C CNN
F 1 "DIP-SWITCH-4-PIN-SPST" H 8400 2050 50  0000 C CNN
F 2 "Switches_DIP:Switch_DIP_x4_Slide" H 8400 2050 10  0001 C CNN
F 3 "" H 8400 2350 60  0000 C CNN
	1    8400 2450
	1    0    0    -1  
$EndComp
Text Label 9000 2150 2    50   ~ 0
TCK2
Text Label 9000 2350 2    50   ~ 0
TMS2
Text Label 9000 2550 2    50   ~ 0
NRST2
Text Label 9000 2750 2    50   ~ 0
SWO2
Text Label 8950 1100 2    50   ~ 0
TMS1
Text Label 8950 1300 2    50   ~ 0
NRST1
Text Label 8950 1500 2    50   ~ 0
SWO1
Text Label 8950 900  2    50   ~ 0
TCK1
Text Label 7850 900  0    50   ~ 0
TCK
Text Label 7850 1100 0    50   ~ 0
TMS
Text Label 7850 1300 0    50   ~ 0
NRST
Text Label 7850 1500 0    50   ~ 0
SWO
Text Label 7850 2150 0    50   ~ 0
TCK
Text Label 7850 2350 0    50   ~ 0
TMS
Text Label 7850 2550 0    50   ~ 0
NRST
Text Label 7850 2750 0    50   ~ 0
SWO
$Sheet
S 8650 3400 1650 2100
U 59AB2BD2
F0 "FT2232H" 60
F1 "FT2232H.sch" 60
F2 "TX" O L 8650 3800 60 
F3 "RX" I L 8650 4000 60 
F4 "GND" I R 10300 3650 60 
F5 "VUSB" O L 8650 3600 60 
F6 "+3.3V" I R 10300 3850 60 
F7 "TXDEN" O R 10300 4200 60 
F8 "~RTS" O R 10300 4350 60 
F9 "~CTS" I R 10300 4500 60 
F10 "~DTR" O R 10300 4650 60 
F11 "~DSR" I R 10300 4800 60 
F12 "~DCD" I R 10300 4950 60 
F13 "~RI" I R 10300 5100 60 
F14 "TCK" O L 8650 4250 60 
F15 "SWDIO" B L 8650 4400 60 
F16 "TMS" O L 8650 4550 60 
$EndSheet
NoConn ~ 8350 3600
NoConn ~ 10600 4200
NoConn ~ 10600 4350
NoConn ~ 10600 4500
NoConn ~ 10600 4650
NoConn ~ 10600 4800
NoConn ~ 10600 4950
NoConn ~ 10600 5100
NoConn ~ 8350 4550
Text Label 1350 5150 2    50   ~ 0
TX3
Text Label 1350 5250 2    50   ~ 0
RX3
Text Label 8350 3800 2    50   ~ 0
RX3
Text Label 8350 4000 2    50   ~ 0
TX3
Text Label 8200 4400 0    50   ~ 0
TMS_FTDI
Text Label 8200 4250 0    50   ~ 0
TCK_FTDI
Text Label 10600 3650 0    50   ~ 0
GND
Text Label 10650 3850 0    50   ~ 0
+3.3V
$Comp
L DIP-SWITCH-4-PIN-SPST SW3
U 1 1 59ABD1A7
P 9950 1300
F 0 "SW3" H 9950 1800 50  0000 C CNN
F 1 "DIP-SWITCH-4-PIN-SPST" H 9950 900 50  0000 C CNN
F 2 "Switches_DIP:Switch_DIP_x4_Slide" H 9950 900 10  0001 C CNN
F 3 "" H 9950 1200 60  0000 C CNN
	1    9950 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 1950 2850 1950
Wire Wire Line
	2650 1950 2650 2050
Connection ~ 2650 1950
Wire Wire Line
	2750 1950 2750 2050
Connection ~ 2700 1950
Wire Wire Line
	2850 1950 2850 2050
Connection ~ 2750 1950
Wire Wire Line
	2450 6150 2850 6150
Wire Wire Line
	2750 6050 2750 6150
Wire Wire Line
	2650 6050 2650 6150
Connection ~ 2700 6150
Wire Wire Line
	5550 6250 5550 6150
Wire Wire Line
	5550 5100 5550 5750
Wire Wire Line
	5550 5650 5450 5650
Wire Wire Line
	4750 5650 4750 5750
Wire Wire Line
	4550 5650 4850 5650
Connection ~ 5550 5650
Connection ~ 4750 5650
Wire Wire Line
	4550 3150 5450 3150
Connection ~ 2650 6150
Wire Wire Line
	4000 3000 4300 3000
Wire Wire Line
	4000 5150 4300 5150
Wire Wire Line
	4700 1450 4950 1450
Wire Wire Line
	4700 1150 5450 1150
Wire Wire Line
	4000 3500 4050 3500
Wire Wire Line
	6550 3050 6750 3050
Wire Wire Line
	6550 3250 6750 3250
Wire Wire Line
	6550 3350 6750 3350
Wire Wire Line
	6550 3450 6750 3450
Wire Wire Line
	1150 3750 1400 3750
Wire Wire Line
	4000 5050 4300 5050
Wire Wire Line
	4000 3100 4300 3100
Wire Wire Line
	1150 3850 1400 3850
Wire Wire Line
	6700 1500 6700 1700
Wire Wire Line
	6700 1000 6700 1100
Wire Wire Line
	6600 2200 7500 2200
Wire Wire Line
	7500 2200 7500 1600
Connection ~ 6700 2200
Wire Wire Line
	6300 1600 6800 1600
Connection ~ 6700 1600
Wire Wire Line
	5150 3050 5150 3250
Connection ~ 5150 3150
Wire Wire Line
	4000 3300 4050 3300
Wire Wire Line
	4000 3400 4050 3400
Wire Wire Line
	5450 1150 5450 1650
Wire Wire Line
	4000 5350 4250 5350
Wire Wire Line
	2550 6050 2550 6150
Connection ~ 2550 6150
Wire Wire Line
	2850 6150 2850 6050
Connection ~ 2750 6150
Wire Wire Line
	4100 2450 4000 2450
Wire Wire Line
	4100 2200 4100 2450
Wire Wire Line
	4000 4850 4300 4850
Wire Wire Line
	4000 4750 4300 4750
Wire Wire Line
	3050 1650 3050 1750
Wire Wire Line
	2950 1750 3950 1750
Wire Wire Line
	3950 1750 3950 1650
Wire Wire Line
	3650 1650 3650 1750
Connection ~ 3650 1750
Wire Wire Line
	3350 1650 3350 1750
Connection ~ 3350 1750
Wire Wire Line
	3050 1150 3050 1250
Wire Wire Line
	2600 1150 3950 1150
Wire Wire Line
	3950 1150 3950 1250
Wire Wire Line
	3650 1250 3650 1150
Connection ~ 3650 1150
Wire Wire Line
	3350 1250 3350 1150
Connection ~ 3350 1150
Connection ~ 3050 1150
Wire Wire Line
	5450 3150 5450 3050
Wire Wire Line
	5450 1450 5350 1450
Connection ~ 5450 1450
Wire Wire Line
	4550 2450 4550 2650
Connection ~ 4550 2550
Wire Wire Line
	4550 3150 4550 3050
Wire Wire Line
	4000 2550 4650 2550
Wire Wire Line
	5450 2550 5050 2550
Wire Wire Line
	5300 2550 5300 2450
Wire Wire Line
	5450 2100 5450 2650
Connection ~ 5300 2550
Connection ~ 5450 2550
Wire Wire Line
	5150 2550 5150 2650
Connection ~ 5150 2550
Wire Wire Line
	4100 2200 5450 2200
Connection ~ 5450 2200
Wire Wire Line
	7500 1600 7400 1600
Wire Notes Line
	6150 750  7600 750 
Wire Notes Line
	7600 750  7600 2550
Wire Notes Line
	7600 2550 6150 2550
Wire Notes Line
	6150 2550 6150 750 
Wire Notes Line
	6150 2750 6150 3850
Wire Wire Line
	4550 5100 4950 5100
Wire Wire Line
	5350 5100 5550 5100
Wire Wire Line
	4750 6250 5550 6250
Wire Wire Line
	5150 6350 5150 6250
Connection ~ 5150 6250
Wire Wire Line
	4750 6250 4750 6150
Wire Notes Line
	800  750  6050 750 
Wire Notes Line
	800  6550 6050 6550
Wire Notes Line
	800  6550 800  750 
Wire Wire Line
	2700 1150 2700 1950
Connection ~ 2700 1150
Wire Wire Line
	2550 2050 2550 1950
Wire Wire Line
	6600 1000 6700 1000
Wire Wire Line
	5450 1650 5350 1650
Wire Wire Line
	5050 6350 5150 6350
Wire Wire Line
	5150 3250 5050 3250
Wire Wire Line
	4450 2450 4550 2450
Wire Wire Line
	6700 2200 6700 2100
Wire Wire Line
	6450 3150 6750 3150
Wire Wire Line
	1400 2450 1350 2450
Wire Wire Line
	1350 5150 1400 5150
Wire Wire Line
	4050 3900 4000 3900
Wire Wire Line
	4050 3800 4000 3800
Wire Notes Line
	6050 6550 6050 750 
Wire Wire Line
	4000 3600 4050 3600
Wire Wire Line
	4000 3700 4050 3700
Connection ~ 3050 1750
Wire Wire Line
	1350 2950 1400 2950
Wire Wire Line
	1350 3150 1400 3150
Wire Wire Line
	1350 5050 1400 5050
Wire Wire Line
	1400 4950 1350 4950
Wire Wire Line
	6450 2950 6750 2950
Wire Notes Line
	6150 2750 7600 2750
Wire Notes Line
	6150 3850 7600 3850
Wire Notes Line
	7600 3850 7600 2750
Wire Wire Line
	5100 5950 5100 6000
Wire Wire Line
	5100 6000 5200 6000
Wire Wire Line
	5200 5950 5200 6250
Connection ~ 5200 6250
Connection ~ 5200 6000
Wire Wire Line
	3950 6200 4250 6200
Wire Wire Line
	3550 6200 3500 6200
Wire Wire Line
	4250 6200 4250 6150
Wire Wire Line
	1400 3550 1350 3550
Wire Wire Line
	1400 3650 1350 3650
Wire Wire Line
	1350 5350 1400 5350
Wire Wire Line
	4000 4200 4050 4200
Wire Wire Line
	4050 4100 4000 4100
Wire Wire Line
	1350 4550 1400 4550
Wire Wire Line
	7450 3050 7450 2950
Wire Wire Line
	7450 2950 7350 2950
Wire Wire Line
	7350 3650 7450 3650
Wire Wire Line
	7450 3650 7450 3450
Wire Wire Line
	1400 5250 1350 5250
Wire Wire Line
	7050 4300 7300 4300
Wire Wire Line
	7050 4600 7300 4600
Wire Wire Line
	7050 4900 7300 4900
Wire Wire Line
	7050 5200 7300 5200
Wire Notes Line
	6700 4050 7600 4050
Wire Notes Line
	7600 4050 7600 5550
Wire Notes Line
	7600 5550 6700 5550
Wire Notes Line
	6700 5550 6700 4050
Wire Wire Line
	1400 2550 1350 2550
Wire Wire Line
	1400 2650 1350 2650
Wire Wire Line
	1400 2750 1350 2750
Wire Wire Line
	1350 2850 1400 2850
Wire Wire Line
	1350 3050 1400 3050
Wire Wire Line
	1400 3250 1350 3250
Wire Wire Line
	1350 3350 1400 3350
Wire Wire Line
	1350 3450 1400 3450
Wire Wire Line
	1350 3950 1400 3950
Wire Wire Line
	1350 4150 1400 4150
Wire Wire Line
	1350 4250 1400 4250
Wire Wire Line
	1350 4350 1400 4350
Wire Wire Line
	1350 4450 1400 4450
Wire Wire Line
	1350 4650 1400 4650
Wire Wire Line
	1350 4750 1400 4750
Wire Wire Line
	1350 4850 1400 4850
Wire Wire Line
	1350 5450 1400 5450
Wire Wire Line
	1350 5550 1400 5550
Wire Wire Line
	1350 5650 1400 5650
Wire Wire Line
	4050 4000 4000 4000
Wire Wire Line
	4050 4300 4000 4300
Wire Wire Line
	4050 4550 4000 4550
Wire Wire Line
	4100 3200 4000 3200
Wire Wire Line
	4050 2900 4000 2900
Wire Wire Line
	4050 2800 4000 2800
Wire Wire Line
	9000 2150 8750 2150
Wire Wire Line
	9000 2350 8750 2350
Wire Wire Line
	9000 2550 8750 2550
Wire Wire Line
	9000 2750 8750 2750
Wire Wire Line
	8950 900  8750 900 
Wire Wire Line
	8950 1100 8750 1100
Wire Wire Line
	8950 1300 8750 1300
Wire Wire Line
	8950 1500 8750 1500
Wire Wire Line
	7850 900  8050 900 
Wire Wire Line
	7850 1100 8050 1100
Wire Wire Line
	7850 1300 8050 1300
Wire Wire Line
	7850 1500 8050 1500
Wire Wire Line
	7850 2150 8050 2150
Wire Wire Line
	7850 2350 8050 2350
Wire Wire Line
	7850 2550 8050 2550
Wire Wire Line
	7850 2750 8050 2750
Wire Wire Line
	8650 3600 8350 3600
Wire Wire Line
	8650 3800 8350 3800
Wire Wire Line
	8650 4000 8350 4000
Wire Wire Line
	8200 4250 8650 4250
Wire Wire Line
	8200 4400 8650 4400
Wire Wire Line
	8650 4550 8350 4550
Wire Wire Line
	10300 3650 10600 3650
Wire Wire Line
	10300 3850 10650 3850
Wire Wire Line
	10300 4200 10600 4200
Wire Wire Line
	10300 4350 10600 4350
Wire Wire Line
	10300 4500 10600 4500
Wire Wire Line
	10300 4650 10600 4650
Wire Wire Line
	10300 4800 10600 4800
Wire Wire Line
	10300 4950 10600 4950
Wire Wire Line
	10300 5100 10600 5100
Wire Wire Line
	9600 1000 9300 1000
Wire Wire Line
	9600 1200 9300 1200
Wire Wire Line
	9600 1400 9300 1400
Wire Wire Line
	9600 1600 9300 1600
Wire Wire Line
	10300 1000 10750 1000
Wire Wire Line
	10300 1200 10750 1200
Wire Wire Line
	10300 1400 10750 1400
Wire Wire Line
	10300 1600 10750 1600
Text Label 9300 1000 0    50   ~ 0
TCK_FTDI
Text Label 9300 1400 0    50   ~ 0
TCK_FTDI
Text Label 9300 1200 0    50   ~ 0
TMS_FTDI
Text Label 9300 1600 0    50   ~ 0
TMS_FTDI
Text Label 10750 1000 2    50   ~ 0
TCK1
Text Label 10750 1200 2    50   ~ 0
TMS1
Text Label 10750 1400 2    50   ~ 0
TCK2
Text Label 10750 1600 2    50   ~ 0
TMS2
Text HLabel 4050 3800 2    50   Input ~ 0
SDA2
Text HLabel 4050 3900 2    50   Input ~ 0
SCL2
$Comp
L BUTTON PB1
U 1 1 59B807AA
P 3700 7150
F 0 "PB1" H 3850 7260 50  0000 C CNN
F 1 "BUTTON" H 3700 7070 50  0000 C CNN
F 2 "Push_Buttons:Button_4p_W-5.2mm_L-5.2mm_Normal_SMD" H 3750 7160 60  0001 C CNN
F 3 "" H 3700 7150 60  0000 C CNN
	1    3700 7150
	1    0    0    -1  
$EndComp
Text HLabel 3100 7150 0    50   Input ~ 0
+3.3V
Text HLabel 4800 7150 2    50   Input ~ 0
GND
Wire Wire Line
	3400 7150 3100 7150
$Comp
L RES R102
U 1 1 59B84810
P 4500 7150
F 0 "R102" H 4500 7250 50  0000 C CNN
F 1 "100K" H 4500 7150 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4500 7080 30  0001 C CNN
F 3 "" V 4500 7150 30  0000 C CNN
	1    4500 7150
	1    0    0    -1  
$EndComp
$Comp
L RES R101
U 1 1 59B848E1
P 3750 7500
F 0 "R101" H 3750 7600 50  0000 C CNN
F 1 "330" H 3750 7500 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 3750 7430 30  0001 C CNN
F 3 "" V 3750 7500 30  0000 C CNN
	1    3750 7500
	1    0    0    -1  
$EndComp
Text Label 1350 2450 2    50   ~ 0
PA0
Text Label 3250 7500 2    50   ~ 0
PA0
Wire Wire Line
	3950 7500 4150 7500
Wire Wire Line
	4150 7500 4150 7150
Wire Wire Line
	4000 7150 4300 7150
Connection ~ 4150 7150
Wire Wire Line
	4700 7150 4800 7150
Wire Wire Line
	3550 7500 3250 7500
$EndSCHEMATC
