EESchema Schematic File Version 2
LIBS:Remote_tester-rescue
LIBS:BJT
LIBS:Capacitor
LIBS:Diode_Rectifier
LIBS:Diode_Schottky
LIBS:Diode_Switching
LIBS:Diode_Zener
LIBS:EEPROM
LIBS:Inductor
LIBS:Microcontrollers
LIBS:Miscellaneous_Connectors
LIBS:Miscellaneous_Devices
LIBS:Modules
LIBS:MOSFET
LIBS:MOV
LIBS:Others
LIBS:Power
LIBS:PTC
LIBS:Regulator
LIBS:Resistor
LIBS:TBU
LIBS:TVS
LIBS:onsemi
LIBS:battery_management
LIBS:device
LIBS:opamp
LIBS:Remote_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title "Keypad Scanner"
Date "2017-08-01"
Rev "1.1.1"
Comp "PT. Fusi Global Teknologi"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4200 7350 2    50   ~ 0
Keypad circuit (combination of rows and colums)
Text Notes 800  7700 0    50   ~ 0
NB:\n- As for membrane keypad usage, ENIG surface finishing is a must.\n- Better to use greater than 3mm pills for faster perfomance (the larger the better)
$Comp
L TEST-RESCUE-Remote_tester TP6
U 1 1 5980A640
P 6050 1100
AR Path="/5980A640" Ref="TP6"  Part="1" 
AR Path="/59A0F043/5980A640" Ref="TP6"  Part="1" 
F 0 "TP6" H 6050 1200 50  0000 C CNN
F 1 "TEST" H 6050 1000 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 1000 10  0001 C CNN
F 3 "" H 6250 1100 60  0000 C CNN
	1    6050 1100
	1    0    0    -1  
$EndComp
Text Label 5700 1400 0    50   ~ 0
ROW1
Text Label 5700 1700 0    50   ~ 0
ROW2
Text Label 5700 1100 0    50   ~ 0
ROW0
Text Label 5700 2000 0    50   ~ 0
ROW3
Text Label 5700 2300 0    50   ~ 0
ROW4
Text Label 5700 2600 0    50   ~ 0
ROW5
Text Label 5700 2900 0    50   ~ 0
ROW6
Text Label 5700 3200 0    50   ~ 0
ROW7
Text Label 5700 3500 0    50   ~ 0
SDA
Text Label 5700 3800 0    50   ~ 0
SCL
Text Label 5700 4100 0    50   ~ 0
~RESET
$Comp
L TEST-RESCUE-Remote_tester TP7
U 1 1 5980B307
P 6050 1400
AR Path="/5980B307" Ref="TP7"  Part="1" 
AR Path="/59A0F043/5980B307" Ref="TP7"  Part="1" 
F 0 "TP7" H 6050 1500 50  0000 C CNN
F 1 "TEST" H 6050 1300 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 1300 10  0001 C CNN
F 3 "" H 6250 1400 60  0000 C CNN
	1    6050 1400
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP8
U 1 1 5980B3AA
P 6050 1700
AR Path="/5980B3AA" Ref="TP8"  Part="1" 
AR Path="/59A0F043/5980B3AA" Ref="TP8"  Part="1" 
F 0 "TP8" H 6050 1800 50  0000 C CNN
F 1 "TEST" H 6050 1600 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 1600 10  0001 C CNN
F 3 "" H 6250 1700 60  0000 C CNN
	1    6050 1700
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP9
U 1 1 5980B438
P 6050 2000
AR Path="/5980B438" Ref="TP9"  Part="1" 
AR Path="/59A0F043/5980B438" Ref="TP9"  Part="1" 
F 0 "TP9" H 6050 2100 50  0000 C CNN
F 1 "TEST" H 6050 1900 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 1900 10  0001 C CNN
F 3 "" H 6250 2000 60  0000 C CNN
	1    6050 2000
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP10
U 1 1 5980B4C9
P 6050 2300
AR Path="/5980B4C9" Ref="TP10"  Part="1" 
AR Path="/59A0F043/5980B4C9" Ref="TP10"  Part="1" 
F 0 "TP10" H 6050 2400 50  0000 C CNN
F 1 "TEST" H 6050 2200 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 2200 10  0001 C CNN
F 3 "" H 6250 2300 60  0000 C CNN
	1    6050 2300
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP11
U 1 1 5980B55D
P 6050 2600
AR Path="/5980B55D" Ref="TP11"  Part="1" 
AR Path="/59A0F043/5980B55D" Ref="TP11"  Part="1" 
F 0 "TP11" H 6050 2700 50  0000 C CNN
F 1 "TEST" H 6050 2500 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 2500 10  0001 C CNN
F 3 "" H 6250 2600 60  0000 C CNN
	1    6050 2600
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP12
U 1 1 5980B5F4
P 6050 2900
AR Path="/5980B5F4" Ref="TP12"  Part="1" 
AR Path="/59A0F043/5980B5F4" Ref="TP12"  Part="1" 
F 0 "TP12" H 6050 3000 50  0000 C CNN
F 1 "TEST" H 6050 2800 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 2800 10  0001 C CNN
F 3 "" H 6250 2900 60  0000 C CNN
	1    6050 2900
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP13
U 1 1 5980B692
P 6050 3200
AR Path="/5980B692" Ref="TP13"  Part="1" 
AR Path="/59A0F043/5980B692" Ref="TP13"  Part="1" 
F 0 "TP13" H 6050 3300 50  0000 C CNN
F 1 "TEST" H 6050 3100 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 3100 10  0001 C CNN
F 3 "" H 6250 3200 60  0000 C CNN
	1    6050 3200
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP14
U 1 1 5980B72F
P 6050 3500
AR Path="/5980B72F" Ref="TP14"  Part="1" 
AR Path="/59A0F043/5980B72F" Ref="TP14"  Part="1" 
F 0 "TP14" H 6050 3600 50  0000 C CNN
F 1 "TEST" H 6050 3400 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 3400 10  0001 C CNN
F 3 "" H 6250 3500 60  0000 C CNN
	1    6050 3500
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP15
U 1 1 5980B7D7
P 6050 3800
AR Path="/5980B7D7" Ref="TP15"  Part="1" 
AR Path="/59A0F043/5980B7D7" Ref="TP15"  Part="1" 
F 0 "TP15" H 6050 3900 50  0000 C CNN
F 1 "TEST" H 6050 3700 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 3700 10  0001 C CNN
F 3 "" H 6250 3800 60  0000 C CNN
	1    6050 3800
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP16
U 1 1 5980B87A
P 6050 4100
AR Path="/5980B87A" Ref="TP16"  Part="1" 
AR Path="/59A0F043/5980B87A" Ref="TP16"  Part="1" 
F 0 "TP16" H 6050 4200 50  0000 C CNN
F 1 "TEST" H 6050 4000 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6050 4000 10  0001 C CNN
F 3 "" H 6250 4100 60  0000 C CNN
	1    6050 4100
	1    0    0    -1  
$EndComp
Text Label 6800 1100 2    50   ~ 0
COL0
Text Label 6800 1400 2    50   ~ 0
COL1
Text Label 6800 1700 2    50   ~ 0
COL2
Text Label 6800 2000 2    50   ~ 0
COL3
Text Label 6800 2600 2    50   ~ 0
COL5
Text Label 6800 2900 2    50   ~ 0
COL6
Text Label 6800 3200 2    50   ~ 0
COL7
Text Label 6800 3500 2    50   ~ 0
COL8
Text Label 6800 3800 2    50   ~ 0
COL9
Text Label 6800 2300 2    50   ~ 0
COL4
Text Label 6800 4100 2    50   ~ 0
~INT
$Comp
L TEST-RESCUE-Remote_tester TP17
U 1 1 5980C51C
P 6450 1100
AR Path="/5980C51C" Ref="TP17"  Part="1" 
AR Path="/59A0F043/5980C51C" Ref="TP17"  Part="1" 
F 0 "TP17" H 6450 1200 50  0000 C CNN
F 1 "TEST" H 6450 1000 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 1000 10  0001 C CNN
F 3 "" H 6650 1100 60  0000 C CNN
	1    6450 1100
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP18
U 1 1 5980C8C8
P 6450 1400
AR Path="/5980C8C8" Ref="TP18"  Part="1" 
AR Path="/59A0F043/5980C8C8" Ref="TP18"  Part="1" 
F 0 "TP18" H 6450 1500 50  0000 C CNN
F 1 "TEST" H 6450 1300 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 1300 10  0001 C CNN
F 3 "" H 6650 1400 60  0000 C CNN
	1    6450 1400
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP19
U 1 1 5980C9A8
P 6450 1700
AR Path="/5980C9A8" Ref="TP19"  Part="1" 
AR Path="/59A0F043/5980C9A8" Ref="TP19"  Part="1" 
F 0 "TP19" H 6450 1800 50  0000 C CNN
F 1 "TEST" H 6450 1600 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 1600 10  0001 C CNN
F 3 "" H 6650 1700 60  0000 C CNN
	1    6450 1700
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP20
U 1 1 5980CAFE
P 6450 2000
AR Path="/5980CAFE" Ref="TP20"  Part="1" 
AR Path="/59A0F043/5980CAFE" Ref="TP20"  Part="1" 
F 0 "TP20" H 6450 2100 50  0000 C CNN
F 1 "TEST" H 6450 1900 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 1900 10  0001 C CNN
F 3 "" H 6650 2000 60  0000 C CNN
	1    6450 2000
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP21
U 1 1 5980CBB0
P 6450 2300
AR Path="/5980CBB0" Ref="TP21"  Part="1" 
AR Path="/59A0F043/5980CBB0" Ref="TP21"  Part="1" 
F 0 "TP21" H 6450 2400 50  0000 C CNN
F 1 "TEST" H 6450 2200 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 2200 10  0001 C CNN
F 3 "" H 6650 2300 60  0000 C CNN
	1    6450 2300
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP22
U 1 1 5980CC69
P 6450 2600
AR Path="/5980CC69" Ref="TP22"  Part="1" 
AR Path="/59A0F043/5980CC69" Ref="TP22"  Part="1" 
F 0 "TP22" H 6450 2700 50  0000 C CNN
F 1 "TEST" H 6450 2500 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 2500 10  0001 C CNN
F 3 "" H 6650 2600 60  0000 C CNN
	1    6450 2600
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP23
U 1 1 5980CD21
P 6450 2900
AR Path="/5980CD21" Ref="TP23"  Part="1" 
AR Path="/59A0F043/5980CD21" Ref="TP23"  Part="1" 
F 0 "TP23" H 6450 3000 50  0000 C CNN
F 1 "TEST" H 6450 2800 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 2800 10  0001 C CNN
F 3 "" H 6650 2900 60  0000 C CNN
	1    6450 2900
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP24
U 1 1 5980CDE0
P 6450 3200
AR Path="/5980CDE0" Ref="TP24"  Part="1" 
AR Path="/59A0F043/5980CDE0" Ref="TP24"  Part="1" 
F 0 "TP24" H 6450 3300 50  0000 C CNN
F 1 "TEST" H 6450 3100 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 3100 10  0001 C CNN
F 3 "" H 6650 3200 60  0000 C CNN
	1    6450 3200
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP25
U 1 1 5980CE9E
P 6450 3500
AR Path="/5980CE9E" Ref="TP25"  Part="1" 
AR Path="/59A0F043/5980CE9E" Ref="TP25"  Part="1" 
F 0 "TP25" H 6450 3600 50  0000 C CNN
F 1 "TEST" H 6450 3400 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 3400 10  0001 C CNN
F 3 "" H 6650 3500 60  0000 C CNN
	1    6450 3500
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP26
U 1 1 5980CF63
P 6450 3800
AR Path="/5980CF63" Ref="TP26"  Part="1" 
AR Path="/59A0F043/5980CF63" Ref="TP26"  Part="1" 
F 0 "TP26" H 6450 3900 50  0000 C CNN
F 1 "TEST" H 6450 3700 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 3700 10  0001 C CNN
F 3 "" H 6650 3800 60  0000 C CNN
	1    6450 3800
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP27
U 1 1 5980D027
P 6450 4100
AR Path="/5980D027" Ref="TP27"  Part="1" 
AR Path="/59A0F043/5980D027" Ref="TP27"  Part="1" 
F 0 "TP27" H 6450 4200 50  0000 C CNN
F 1 "TEST" H 6450 4000 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6450 4000 10  0001 C CNN
F 3 "" H 6650 4100 60  0000 C CNN
	1    6450 4100
	-1   0    0    1   
$EndComp
Text Notes 6950 4500 2    50   ~ 0
Test Point: Keypad
$Comp
L MMBT3904 SHTD1
U 1 1 59A2D009
P 1550 1450
F 0 "SHTD1" H 1450 1300 50  0000 R CNN
F 1 "TR1" H 1550 1600 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 1550 10  0001 C CNN
F 3 "" H 1550 1450 60  0000 C CNN
	1    1550 1450
	1    0    0    -1  
$EndComp
Text Label 1900 1250 2    50   ~ 0
ROW0
Text Label 1900 1650 2    50   ~ 0
COL9
Text Label 750  1450 2    50   ~ 0
PM1
$Comp
L MMBT3904 PWR1
U 1 1 59A33DCF
P 4850 1450
F 0 "PWR1" H 4750 1300 50  0000 R CNN
F 1 "TR2" H 4850 1600 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 1550 10  0001 C CNN
F 3 "" H 4850 1450 60  0000 C CNN
	1    4850 1450
	1    0    0    -1  
$EndComp
Text Label 5200 1250 2    50   ~ 0
ROW2
Text Label 5200 1650 2    50   ~ 0
COL9
Text Label 4050 1450 2    50   ~ 0
PM2
$Comp
L MMBT3904 A1
U 1 1 59A33E5E
P 1550 2050
F 0 "A1" H 1450 1900 50  0000 R CNN
F 1 "TR3" H 1550 2200 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 2150 10  0001 C CNN
F 3 "" H 1550 2050 60  0000 C CNN
	1    1550 2050
	1    0    0    -1  
$EndComp
Text Label 1900 1850 2    50   ~ 0
ROW0
Text Label 1900 2250 2    50   ~ 0
COL8
Text Label 750  2050 2    50   ~ 0
PM3
$Comp
L MMBT3904 B1
U 1 1 59A34193
P 3200 2050
F 0 "B1" H 3100 1900 50  0000 R CNN
F 1 "TR4" H 3200 2200 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 2150 10  0001 C CNN
F 3 "" H 3200 2050 60  0000 C CNN
	1    3200 2050
	1    0    0    -1  
$EndComp
Text Label 3550 1850 2    50   ~ 0
ROW1
Text Label 3550 2250 2    50   ~ 0
COL8
Text Label 2400 2050 2    50   ~ 0
PM4
$Comp
L MMBT3904 C13
U 1 1 59A3423A
P 4850 2050
F 0 "C13" H 4750 1900 50  0000 R CNN
F 1 "TR5" H 4850 2200 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 2150 10  0001 C CNN
F 3 "" H 4850 2050 60  0000 C CNN
	1    4850 2050
	1    0    0    -1  
$EndComp
Text Label 5200 1850 2    50   ~ 0
ROW2
Text Label 5200 2250 2    50   ~ 0
COL8
Text Label 4050 2050 2    50   ~ 0
PM5
$Comp
L MMBT3904 D3
U 1 1 59A343A5
P 1550 2650
F 0 "D3" H 1450 2500 50  0000 R CNN
F 1 "TR6" H 1550 2800 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 2750 10  0001 C CNN
F 3 "" H 1550 2650 60  0000 C CNN
	1    1550 2650
	1    0    0    -1  
$EndComp
Text Label 1900 2450 2    50   ~ 0
ROW0
Text Label 1900 2850 2    50   ~ 0
COL7
Text Label 750  2650 2    50   ~ 0
PM6
$Comp
L MMBT3904 E1
U 1 1 59A343B1
P 3200 2650
F 0 "E1" H 3100 2500 50  0000 R CNN
F 1 "TR7" H 3200 2800 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 2750 10  0001 C CNN
F 3 "" H 3200 2650 60  0000 C CNN
	1    3200 2650
	1    0    0    -1  
$EndComp
Text Label 3550 2450 2    50   ~ 0
ROW1
Text Label 3550 2850 2    50   ~ 0
COL7
Text Label 2400 2650 2    50   ~ 0
PM7
$Comp
L MMBT3904 F1
U 1 1 59A343BD
P 4850 2650
F 0 "F1" H 4750 2500 50  0000 R CNN
F 1 "TR8" H 4850 2800 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 2750 10  0001 C CNN
F 3 "" H 4850 2650 60  0000 C CNN
	1    4850 2650
	1    0    0    -1  
$EndComp
Text Label 5200 2450 2    50   ~ 0
ROW2
Text Label 5200 2850 2    50   ~ 0
COL7
Text Label 4050 2650 2    50   ~ 0
PM8
$Comp
L MMBT3904 G1
U 1 1 59A34A83
P 1550 3250
F 0 "G1" H 1450 3100 50  0000 R CNN
F 1 "TR9" H 1550 3400 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 3350 10  0001 C CNN
F 3 "" H 1550 3250 60  0000 C CNN
	1    1550 3250
	1    0    0    -1  
$EndComp
Text Label 1900 3050 2    50   ~ 0
ROW0
Text Label 1900 3450 2    50   ~ 0
COL6
Text Label 750  3250 2    50   ~ 0
PM9
$Comp
L MMBT3904 H1
U 1 1 59A34A8F
P 3200 3250
F 0 "H1" H 3100 3100 50  0000 R CNN
F 1 "TR10" H 3200 3400 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 3350 10  0001 C CNN
F 3 "" H 3200 3250 60  0000 C CNN
	1    3200 3250
	1    0    0    -1  
$EndComp
Text Label 3550 3050 2    50   ~ 0
ROW1
Text Label 3550 3450 2    50   ~ 0
COL6
Text Label 2400 3250 2    50   ~ 0
PM10
$Comp
L MMBT3904 I1
U 1 1 59A34A9B
P 4850 3250
F 0 "I1" H 4750 3100 50  0000 R CNN
F 1 "TR11" H 4850 3400 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 3350 10  0001 C CNN
F 3 "" H 4850 3250 60  0000 C CNN
	1    4850 3250
	1    0    0    -1  
$EndComp
Text Label 5200 3050 2    50   ~ 0
ROW2
Text Label 5200 3450 2    50   ~ 0
COL6
Text Label 4050 3250 2    50   ~ 0
PM11
$Comp
L MMBT3904 CH1
U 1 1 59A34AA7
P 1550 3850
F 0 "CH1" H 1450 3700 50  0000 R CNN
F 1 "TR12" H 1550 4000 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 3950 10  0001 C CNN
F 3 "" H 1550 3850 60  0000 C CNN
	1    1550 3850
	1    0    0    -1  
$EndComp
Text Label 1900 3650 2    50   ~ 0
ROW0
Text Label 1900 4050 2    50   ~ 0
COL5
Text Label 750  3850 2    50   ~ 0
PM12
$Comp
L MMBT3904 CH2
U 1 1 59A34AB3
P 3200 3850
F 0 "CH2" H 3100 3700 50  0000 R CNN
F 1 "TR13" H 3200 4000 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 3950 10  0001 C CNN
F 3 "" H 3200 3850 60  0000 C CNN
	1    3200 3850
	1    0    0    -1  
$EndComp
Text Label 3550 3650 2    50   ~ 0
ROW1
Text Label 3550 4050 2    50   ~ 0
COL5
Text Label 2400 3850 2    50   ~ 0
PM13
$Comp
L MMBT3904 CH3
U 1 1 59A34ABF
P 4850 3850
F 0 "CH3" H 4750 3700 50  0000 R CNN
F 1 "TR14" H 4850 4000 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 3950 10  0001 C CNN
F 3 "" H 4850 3850 60  0000 C CNN
	1    4850 3850
	1    0    0    -1  
$EndComp
Text Label 5200 3650 2    50   ~ 0
ROW2
Text Label 5200 4050 2    50   ~ 0
COL5
Text Label 4050 3850 2    50   ~ 0
PM14
$Comp
L MMBT3904 CH4
U 1 1 59A35157
P 1550 4400
F 0 "CH4" H 1450 4250 50  0000 R CNN
F 1 "TR15" H 1550 4550 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 4500 10  0001 C CNN
F 3 "" H 1550 4400 60  0000 C CNN
	1    1550 4400
	1    0    0    -1  
$EndComp
Text Label 1900 4200 2    50   ~ 0
ROW0
Text Label 1900 4600 2    50   ~ 0
COL4
Text Label 750  4400 2    50   ~ 0
PM15
$Comp
L MMBT3904 CH5
U 1 1 59A35163
P 3200 4400
F 0 "CH5" H 3100 4250 50  0000 R CNN
F 1 "TR16" H 3200 4550 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 4500 10  0001 C CNN
F 3 "" H 3200 4400 60  0000 C CNN
	1    3200 4400
	1    0    0    -1  
$EndComp
Text Label 3550 4200 2    50   ~ 0
ROW1
Text Label 3550 4600 2    50   ~ 0
COL4
Text Label 2400 4400 2    50   ~ 0
PM16
$Comp
L MMBT3904 CH6
U 1 1 59A3516F
P 4850 4400
F 0 "CH6" H 4750 4250 50  0000 R CNN
F 1 "TR17" H 4850 4550 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 4500 10  0001 C CNN
F 3 "" H 4850 4400 60  0000 C CNN
	1    4850 4400
	1    0    0    -1  
$EndComp
Text Label 5200 4200 2    50   ~ 0
ROW2
Text Label 5200 4600 2    50   ~ 0
COL4
Text Label 4050 4400 2    50   ~ 0
PM17
$Comp
L MMBT3904 CH7
U 1 1 59A3517B
P 1550 5000
F 0 "CH7" H 1450 4850 50  0000 R CNN
F 1 "TR18" H 1550 5150 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 5100 10  0001 C CNN
F 3 "" H 1550 5000 60  0000 C CNN
	1    1550 5000
	1    0    0    -1  
$EndComp
Text Label 1900 4800 2    50   ~ 0
ROW0
Text Label 1900 5200 2    50   ~ 0
COL3
Text Label 750  5000 2    50   ~ 0
PM18
$Comp
L MMBT3904 CH8
U 1 1 59A35187
P 3200 5000
F 0 "CH8" H 3100 4850 50  0000 R CNN
F 1 "TR19" H 3200 5150 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 5100 10  0001 C CNN
F 3 "" H 3200 5000 60  0000 C CNN
	1    3200 5000
	1    0    0    -1  
$EndComp
Text Label 3550 4800 2    50   ~ 0
ROW1
Text Label 3550 5200 2    50   ~ 0
COL3
Text Label 2400 5000 2    50   ~ 0
PM19
$Comp
L MMBT3904 CH9
U 1 1 59A35193
P 4850 5000
F 0 "CH9" H 4750 4850 50  0000 R CNN
F 1 "TR20" H 4850 5150 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 5100 10  0001 C CNN
F 3 "" H 4850 5000 60  0000 C CNN
	1    4850 5000
	1    0    0    -1  
$EndComp
Text Label 5200 4800 2    50   ~ 0
ROW2
Text Label 5200 5200 2    50   ~ 0
COL3
Text Label 4050 5000 2    50   ~ 0
PM20
$Comp
L MMBT3904 DISLIKE1
U 1 1 59A3519F
P 1550 5600
F 0 "DISLIKE1" H 1450 5450 50  0000 R CNN
F 1 "TR21" H 1550 5750 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 5700 10  0001 C CNN
F 3 "" H 1550 5600 60  0000 C CNN
	1    1550 5600
	1    0    0    -1  
$EndComp
Text Label 1900 5400 2    50   ~ 0
ROW0
Text Label 1900 5800 2    50   ~ 0
COL2
Text Label 750  5600 2    50   ~ 0
PM21
$Comp
L MMBT3904 CH0
U 1 1 59A351AB
P 3200 5600
F 0 "CH0" H 3100 5450 50  0000 R CNN
F 1 "TR22" H 3200 5750 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 5700 10  0001 C CNN
F 3 "" H 3200 5600 60  0000 C CNN
	1    3200 5600
	1    0    0    -1  
$EndComp
Text Label 3550 5400 2    50   ~ 0
ROW1
Text Label 3550 5800 2    50   ~ 0
COL2
Text Label 2400 5600 2    50   ~ 0
PM22
$Comp
L MMBT3904 LIKE1
U 1 1 59A351B7
P 4850 5600
F 0 "LIKE1" H 4750 5450 50  0000 R CNN
F 1 "TR23" H 4850 5750 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 5700 10  0001 C CNN
F 3 "" H 4850 5600 60  0000 C CNN
	1    4850 5600
	1    0    0    -1  
$EndComp
Text Label 5200 5400 2    50   ~ 0
ROW2
Text Label 5200 5800 2    50   ~ 0
COL2
Text Label 4050 5600 2    50   ~ 0
PM23
$Comp
L MMBT3904 VOL+1
U 1 1 59A351C3
P 1550 6200
F 0 "VOL+1" H 1450 6050 50  0000 R CNN
F 1 "TR24" H 1550 6350 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 6300 10  0001 C CNN
F 3 "" H 1550 6200 60  0000 C CNN
	1    1550 6200
	1    0    0    -1  
$EndComp
Text Label 1900 6000 2    50   ~ 0
ROW0
Text Label 1900 6400 2    50   ~ 0
COL1
Text Label 750  6200 2    50   ~ 0
PM24
$Comp
L MMBT3904 MUTE1
U 1 1 59A351CF
P 3200 6200
F 0 "MUTE1" H 3100 6050 50  0000 R CNN
F 1 "TR25" H 3200 6350 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 3050 6300 10  0001 C CNN
F 3 "" H 3200 6200 60  0000 C CNN
	1    3200 6200
	1    0    0    -1  
$EndComp
Text Label 3550 6000 2    50   ~ 0
ROW1
Text Label 3550 6400 2    50   ~ 0
COL1
Text Label 2400 6200 2    50   ~ 0
PM25
$Comp
L MMBT3904 CH+1
U 1 1 59A351DB
P 4850 6200
F 0 "CH+1" H 4750 6050 50  0000 R CNN
F 1 "TR26" H 4850 6350 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 6300 10  0001 C CNN
F 3 "" H 4850 6200 60  0000 C CNN
	1    4850 6200
	1    0    0    -1  
$EndComp
Text Label 5200 6000 2    50   ~ 0
ROW2
Text Label 5200 6400 2    50   ~ 0
COL1
Text Label 4050 6200 2    50   ~ 0
PM26
$Comp
L MMBT3904 VOL-1
U 1 1 59A35A53
P 1550 6700
F 0 "VOL-1" H 1450 6550 50  0000 R CNN
F 1 "TR27" H 1550 6850 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 1400 6800 10  0001 C CNN
F 3 "" H 1550 6700 60  0000 C CNN
	1    1550 6700
	1    0    0    -1  
$EndComp
Text Label 1900 6500 2    50   ~ 0
ROW0
Text Label 1900 6900 2    50   ~ 0
COL0
Text Label 750  6700 2    50   ~ 0
PM27
$Comp
L MMBT3904 CH-1
U 1 1 59A35A5F
P 4850 6700
F 0 "CH-1" H 4750 6550 50  0000 R CNN
F 1 "TR28" H 4850 6850 50  0000 R CNN
F 2 "Packages_SOT:SOT-23-3" H 4700 6800 10  0001 C CNN
F 3 "" H 4850 6700 60  0000 C CNN
	1    4850 6700
	1    0    0    -1  
$EndComp
Text Label 5200 6500 2    50   ~ 0
ROW2
Text Label 5200 6900 2    50   ~ 0
COL0
Text Label 4050 6700 2    50   ~ 0
PM28
$Comp
L 4051 IC7
U 1 1 59A4BE2B
P 8800 1300
F 0 "IC7" H 8400 1800 50  0000 L CNN
F 1 "4051" H 8400 800 50  0000 L CNN
F 2 "Packages_DIP:DIP-16_300" H 9250 1100 60  0001 C CNN
F 3 "" H 9250 1100 60  0000 C CNN
	1    8800 1300
	1    0    0    -1  
$EndComp
$Comp
L 4051 IC8
U 1 1 59A4C1F7
P 8800 2550
F 0 "IC8" H 8400 3050 50  0000 L CNN
F 1 "4051" H 8400 2050 50  0000 L CNN
F 2 "Packages_DIP:DIP-16_300" H 9250 2350 60  0001 C CNN
F 3 "" H 9250 2350 60  0000 C CNN
	1    8800 2550
	1    0    0    -1  
$EndComp
$Comp
L 4051 IC9
U 1 1 59A4C2BC
P 8800 3750
F 0 "IC9" H 8400 4250 50  0000 L CNN
F 1 "4051" H 8400 3250 50  0000 L CNN
F 2 "Packages_DIP:DIP-16_300" H 9250 3550 60  0001 C CNN
F 3 "" H 9250 3550 60  0000 C CNN
	1    8800 3750
	1    0    0    -1  
$EndComp
$Comp
L 4051 IC10
U 1 1 59A4C9B9
P 8800 5000
F 0 "IC10" H 8400 5500 50  0000 L CNN
F 1 "4051" H 8400 4500 50  0000 L CNN
F 2 "Packages_DIP:DIP-16_300" H 9250 4800 60  0001 C CNN
F 3 "" H 9250 4800 60  0000 C CNN
	1    8800 5000
	1    0    0    -1  
$EndComp
Text Label 8000 950  2    50   ~ 0
PM1
Text Label 8000 1150 2    50   ~ 0
PM3
Text Label 8000 1450 2    50   ~ 0
PM6
Text Label 8000 2200 2    50   ~ 0
PM9
Text Label 8000 2500 2    50   ~ 0
PM12
Text Label 8000 2800 2    50   ~ 0
PM15
Text Label 8000 3500 2    50   ~ 0
PM18
Text Label 8000 3800 2    50   ~ 0
PM21
Text Label 8000 4100 2    50   ~ 0
PM24
Text Label 8000 4850 2    50   ~ 0
PM27
Text Label 8000 1250 2    50   ~ 0
PM4
Text Label 8000 1550 2    50   ~ 0
PM7
Text Label 8000 2300 2    50   ~ 0
PM10
Text Label 8000 2600 2    50   ~ 0
PM13
Text Label 8000 2900 2    50   ~ 0
PM16
Text Label 8000 3600 2    50   ~ 0
PM19
Text Label 8000 3900 2    50   ~ 0
PM22
Text Label 8000 4650 2    50   ~ 0
PM25
Text Label 8000 1050 2    50   ~ 0
PM2
Text Label 8000 1350 2    50   ~ 0
PM5
Text Label 8000 1650 2    50   ~ 0
PM8
Text Label 8000 2400 2    50   ~ 0
PM11
Text Label 8000 2700 2    50   ~ 0
PM14
Text Label 8000 3400 2    50   ~ 0
PM17
Text Label 8000 3700 2    50   ~ 0
PM20
Text Label 8000 4000 2    50   ~ 0
PM23
Text Label 8000 4750 2    50   ~ 0
PM26
Text Label 8000 4950 2    50   ~ 0
PM28
Text HLabel 10650 700  1    50   Output ~ 0
VCC
Text HLabel 10050 6950 3    50   Input ~ 0
GND
Text HLabel 9350 4850 2    50   Input ~ 0
PC13
Text HLabel 9350 4950 2    50   Input ~ 0
PC14
Text HLabel 9350 5050 2    50   Input ~ 0
PC15
Text HLabel 9350 3600 2    50   Input ~ 0
PC13
Text HLabel 9350 3700 2    50   Input ~ 0
PC14
Text HLabel 9350 3800 2    50   Input ~ 0
PC15
Text HLabel 9350 2400 2    50   Input ~ 0
PC13
Text HLabel 9350 2500 2    50   Input ~ 0
PC14
Text HLabel 9350 2600 2    50   Input ~ 0
PC15
Text HLabel 9350 1150 2    50   Input ~ 0
PC13
Text HLabel 9350 1250 2    50   Input ~ 0
PC14
Text HLabel 9350 1350 2    50   Input ~ 0
PC15
Text HLabel 9700 1050 2    50   Input ~ 0
PC0
Text HLabel 9650 2300 2    50   Input ~ 0
PC1
Text HLabel 9650 3500 2    50   Input ~ 0
PC2
Text HLabel 9650 4750 2    50   Input ~ 0
PC3
Text Label 7900 5850 0    50   ~ 0
ROW3
Text Label 7900 5950 0    50   ~ 0
ROW4
Text Label 7900 6050 0    50   ~ 0
ROW5
Text Label 7900 6150 0    50   ~ 0
ROW6
Text Label 7900 6250 0    50   ~ 0
ROW7
Text Label 6000 7550 1    50   ~ 0
SDA
Text Label 6000 6700 1    50   ~ 0
SCL
Text Label 6000 5250 1    50   ~ 0
~RESET
Text Label 6000 5950 1    50   ~ 0
~INT
Text HLabel 6000 7200 1    50   Input ~ 0
SDA2
Text HLabel 6000 6350 1    50   Input ~ 0
SCL2
Text HLabel 6650 4800 1    50   Input ~ 0
PB4
Text HLabel 9600 5950 2    50   Input ~ 0
PB5
Text HLabel 6650 5500 1    50   Input ~ 0
PD2
$Comp
L RES R56
U 1 1 59A4D73D
P 1150 3850
F 0 "R56" H 1150 3950 50  0000 C CNN
F 1 "1K" H 1150 3850 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 3780 30  0001 C CNN
F 3 "" V 1150 3850 30  0000 C CNN
	1    1150 3850
	1    0    0    -1  
$EndComp
$Comp
L RES R53
U 1 1 59A4D667
P 1150 3250
F 0 "R53" H 1150 3350 50  0000 C CNN
F 1 "1K" H 1150 3250 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 3180 30  0001 C CNN
F 3 "" V 1150 3250 30  0000 C CNN
	1    1150 3250
	1    0    0    -1  
$EndComp
$Comp
L RES R50
U 1 1 59A4C959
P 1150 2650
F 0 "R50" H 1150 2750 50  0000 C CNN
F 1 "1K" H 1150 2650 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 2580 30  0001 C CNN
F 3 "" V 1150 2650 30  0000 C CNN
	1    1150 2650
	1    0    0    -1  
$EndComp
$Comp
L RES R47
U 1 1 59A4C851
P 1150 2050
F 0 "R47" H 1150 2150 50  0000 C CNN
F 1 "1K" H 1150 2050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 1980 30  0001 C CNN
F 3 "" V 1150 2050 30  0000 C CNN
	1    1150 2050
	1    0    0    -1  
$EndComp
$Comp
L RES R45
U 1 1 59A47DBC
P 1150 1450
F 0 "R45" H 1150 1550 50  0000 C CNN
F 1 "1K" H 1150 1450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 1380 30  0001 C CNN
F 3 "" V 1150 1450 30  0000 C CNN
	1    1150 1450
	1    0    0    -1  
$EndComp
$Comp
L RES R72
U 1 1 59A4E6D8
P 1150 6700
F 0 "R72" H 1150 6800 50  0000 C CNN
F 1 "1K" H 1150 6700 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 6630 30  0001 C CNN
F 3 "" V 1150 6700 30  0000 C CNN
	1    1150 6700
	1    0    0    -1  
$EndComp
$Comp
L RES R69
U 1 1 59A4E6DE
P 1150 6200
F 0 "R69" H 1150 6300 50  0000 C CNN
F 1 "1K" H 1150 6200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 6130 30  0001 C CNN
F 3 "" V 1150 6200 30  0000 C CNN
	1    1150 6200
	1    0    0    -1  
$EndComp
$Comp
L RES R65
U 1 1 59A4E6E4
P 1150 5600
F 0 "R65" H 1150 5700 50  0000 C CNN
F 1 "1K" H 1150 5600 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 5530 30  0001 C CNN
F 3 "" V 1150 5600 30  0000 C CNN
	1    1150 5600
	1    0    0    -1  
$EndComp
$Comp
L RES R62
U 1 1 59A4E6EA
P 1150 5000
F 0 "R62" H 1150 5100 50  0000 C CNN
F 1 "1K" H 1150 5000 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 4930 30  0001 C CNN
F 3 "" V 1150 5000 30  0000 C CNN
	1    1150 5000
	1    0    0    -1  
$EndComp
$Comp
L RES R59
U 1 1 59A4E6F0
P 1150 4400
F 0 "R59" H 1150 4500 50  0000 C CNN
F 1 "1K" H 1150 4400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1150 4330 30  0001 C CNN
F 3 "" V 1150 4400 30  0000 C CNN
	1    1150 4400
	1    0    0    -1  
$EndComp
$Comp
L RES R57
U 1 1 59A503EA
P 2800 3850
F 0 "R57" H 2800 3950 50  0000 C CNN
F 1 "1K" H 2800 3850 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 3780 30  0001 C CNN
F 3 "" V 2800 3850 30  0000 C CNN
	1    2800 3850
	1    0    0    -1  
$EndComp
$Comp
L RES R54
U 1 1 59A503F0
P 2800 3250
F 0 "R54" H 2800 3350 50  0000 C CNN
F 1 "1K" H 2800 3250 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 3180 30  0001 C CNN
F 3 "" V 2800 3250 30  0000 C CNN
	1    2800 3250
	1    0    0    -1  
$EndComp
$Comp
L RES R51
U 1 1 59A503F6
P 2800 2650
F 0 "R51" H 2800 2750 50  0000 C CNN
F 1 "1K" H 2800 2650 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 2580 30  0001 C CNN
F 3 "" V 2800 2650 30  0000 C CNN
	1    2800 2650
	1    0    0    -1  
$EndComp
$Comp
L RES R48
U 1 1 59A503FC
P 2800 2050
F 0 "R48" H 2800 2150 50  0000 C CNN
F 1 "1K" H 2800 2050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 1980 30  0001 C CNN
F 3 "" V 2800 2050 30  0000 C CNN
	1    2800 2050
	1    0    0    -1  
$EndComp
$Comp
L RES R70
U 1 1 59A50402
P 2800 6200
F 0 "R70" H 2800 6300 50  0000 C CNN
F 1 "1K" H 2800 6200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 6130 30  0001 C CNN
F 3 "" V 2800 6200 30  0000 C CNN
	1    2800 6200
	1    0    0    -1  
$EndComp
$Comp
L RES R67
U 1 1 59A50408
P 2800 5600
F 0 "R67" H 2800 5700 50  0000 C CNN
F 1 "1K" H 2800 5600 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 5530 30  0001 C CNN
F 3 "" V 2800 5600 30  0000 C CNN
	1    2800 5600
	1    0    0    -1  
$EndComp
$Comp
L RES R63
U 1 1 59A5040E
P 2800 5000
F 0 "R63" H 2800 5100 50  0000 C CNN
F 1 "1K" H 2800 5000 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 4930 30  0001 C CNN
F 3 "" V 2800 5000 30  0000 C CNN
	1    2800 5000
	1    0    0    -1  
$EndComp
$Comp
L RES R60
U 1 1 59A50414
P 2800 4400
F 0 "R60" H 2800 4500 50  0000 C CNN
F 1 "1K" H 2800 4400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2800 4330 30  0001 C CNN
F 3 "" V 2800 4400 30  0000 C CNN
	1    2800 4400
	1    0    0    -1  
$EndComp
$Comp
L RES R58
U 1 1 59A511FC
P 4450 3850
F 0 "R58" H 4450 3950 50  0000 C CNN
F 1 "1K" H 4450 3850 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 3780 30  0001 C CNN
F 3 "" V 4450 3850 30  0000 C CNN
	1    4450 3850
	1    0    0    -1  
$EndComp
$Comp
L RES R55
U 1 1 59A51202
P 4450 3250
F 0 "R55" H 4450 3350 50  0000 C CNN
F 1 "1K" H 4450 3250 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 3180 30  0001 C CNN
F 3 "" V 4450 3250 30  0000 C CNN
	1    4450 3250
	1    0    0    -1  
$EndComp
$Comp
L RES R52
U 1 1 59A51208
P 4450 2650
F 0 "R52" H 4450 2750 50  0000 C CNN
F 1 "1K" H 4450 2650 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 2580 30  0001 C CNN
F 3 "" V 4450 2650 30  0000 C CNN
	1    4450 2650
	1    0    0    -1  
$EndComp
$Comp
L RES R49
U 1 1 59A5120E
P 4450 2050
F 0 "R49" H 4450 2150 50  0000 C CNN
F 1 "1K" H 4450 2050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 1980 30  0001 C CNN
F 3 "" V 4450 2050 30  0000 C CNN
	1    4450 2050
	1    0    0    -1  
$EndComp
$Comp
L RES R46
U 1 1 59A51214
P 4450 1450
F 0 "R46" H 4450 1550 50  0000 C CNN
F 1 "1K" H 4450 1450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 1380 30  0001 C CNN
F 3 "" V 4450 1450 30  0000 C CNN
	1    4450 1450
	1    0    0    -1  
$EndComp
$Comp
L RES R73
U 1 1 59A5121A
P 4450 6700
F 0 "R73" H 4450 6800 50  0000 C CNN
F 1 "1K" H 4450 6700 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 6630 30  0001 C CNN
F 3 "" V 4450 6700 30  0000 C CNN
	1    4450 6700
	1    0    0    -1  
$EndComp
$Comp
L RES R71
U 1 1 59A51220
P 4450 6200
F 0 "R71" H 4450 6300 50  0000 C CNN
F 1 "1K" H 4450 6200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 6130 30  0001 C CNN
F 3 "" V 4450 6200 30  0000 C CNN
	1    4450 6200
	1    0    0    -1  
$EndComp
$Comp
L RES R68
U 1 1 59A51226
P 4450 5600
F 0 "R68" H 4450 5700 50  0000 C CNN
F 1 "1K" H 4450 5600 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 5530 30  0001 C CNN
F 3 "" V 4450 5600 30  0000 C CNN
	1    4450 5600
	1    0    0    -1  
$EndComp
$Comp
L RES R64
U 1 1 59A5122C
P 4450 5000
F 0 "R64" H 4450 5100 50  0000 C CNN
F 1 "1K" H 4450 5000 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 4930 30  0001 C CNN
F 3 "" V 4450 5000 30  0000 C CNN
	1    4450 5000
	1    0    0    -1  
$EndComp
$Comp
L RES R61
U 1 1 59A51232
P 4450 4400
F 0 "R61" H 4450 4500 50  0000 C CNN
F 1 "1K" H 4450 4400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4450 4330 30  0001 C CNN
F 3 "" V 4450 4400 30  0000 C CNN
	1    4450 4400
	1    0    0    -1  
$EndComp
$Comp
L RES R84
U 1 1 59A765D2
P 6300 7300
F 0 "R84" H 6300 7400 50  0000 C CNN
F 1 "10K" H 6300 7300 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6300 7230 30  0001 C CNN
F 3 "" V 6300 7300 30  0000 C CNN
	1    6300 7300
	1    0    0    -1  
$EndComp
$Comp
L RES R85
U 1 1 59A768DC
P 6300 6450
F 0 "R85" H 6300 6550 50  0000 C CNN
F 1 "10K" H 6300 6450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6300 6380 30  0001 C CNN
F 3 "" V 6300 6450 30  0000 C CNN
	1    6300 6450
	1    0    0    -1  
$EndComp
$Comp
L RES R86
U 1 1 59A76A9A
P 6300 5700
F 0 "R86" H 6300 5800 50  0000 C CNN
F 1 "220" H 6300 5700 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6300 5630 30  0001 C CNN
F 3 "" V 6300 5700 30  0000 C CNN
	1    6300 5700
	1    0    0    -1  
$EndComp
$Comp
L RES R87
U 1 1 59A76AA0
P 6300 5000
F 0 "R87" H 6300 5100 50  0000 C CNN
F 1 "220" H 6300 5000 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6300 4930 30  0001 C CNN
F 3 "" V 6300 5000 30  0000 C CNN
	1    6300 5000
	1    0    0    -1  
$EndComp
Text HLabel 6650 7450 3    50   Input ~ 0
+3.3V
Wire Wire Line
	6050 3200 5700 3200
Wire Wire Line
	6050 2000 5700 2000
Wire Wire Line
	6050 1700 5700 1700
Wire Wire Line
	6050 1400 5700 1400
Wire Wire Line
	6050 1100 5700 1100
Wire Wire Line
	5700 2900 6050 2900
Wire Wire Line
	5700 2600 6050 2600
Wire Wire Line
	5700 2300 6050 2300
Wire Wire Line
	6050 4100 5700 4100
Wire Wire Line
	5700 3800 6050 3800
Wire Wire Line
	5700 3500 6050 3500
Wire Wire Line
	6450 1100 6800 1100
Wire Wire Line
	6450 1400 6800 1400
Wire Wire Line
	6450 1700 6800 1700
Wire Wire Line
	6450 2000 6800 2000
Wire Wire Line
	6450 2300 6800 2300
Wire Wire Line
	6800 2600 6450 2600
Wire Wire Line
	6800 2900 6450 2900
Wire Wire Line
	6800 3200 6450 3200
Wire Wire Line
	6450 3500 6800 3500
Wire Wire Line
	6800 3800 6450 3800
Wire Wire Line
	6800 4100 6450 4100
Wire Notes Line
	5550 800  6950 800 
Wire Notes Line
	6950 800  6950 4400
Wire Notes Line
	6950 4400 5550 4400
Wire Notes Line
	5550 4400 5550 800 
Wire Wire Line
	1650 1250 1900 1250
Wire Wire Line
	1650 1650 1900 1650
Wire Wire Line
	950  1450 750  1450
Wire Wire Line
	4950 1250 5200 1250
Wire Wire Line
	4950 1650 5200 1650
Wire Wire Line
	4250 1450 4050 1450
Wire Wire Line
	1650 1850 1900 1850
Wire Wire Line
	1650 2250 1900 2250
Wire Wire Line
	950  2050 750  2050
Wire Wire Line
	3300 1850 3550 1850
Wire Wire Line
	3300 2250 3550 2250
Wire Wire Line
	2600 2050 2400 2050
Wire Wire Line
	4950 1850 5200 1850
Wire Wire Line
	4950 2250 5200 2250
Wire Wire Line
	4250 2050 4050 2050
Wire Wire Line
	1650 2450 1900 2450
Wire Wire Line
	1650 2850 1900 2850
Wire Wire Line
	950  2650 750  2650
Wire Wire Line
	3300 2450 3550 2450
Wire Wire Line
	3300 2850 3550 2850
Wire Wire Line
	2600 2650 2400 2650
Wire Wire Line
	4950 2450 5200 2450
Wire Wire Line
	4950 2850 5200 2850
Wire Wire Line
	4250 2650 4050 2650
Wire Wire Line
	1650 3050 1900 3050
Wire Wire Line
	1650 3450 1900 3450
Wire Wire Line
	950  3250 750  3250
Wire Wire Line
	3300 3050 3550 3050
Wire Wire Line
	3300 3450 3550 3450
Wire Wire Line
	2600 3250 2400 3250
Wire Wire Line
	4950 3050 5200 3050
Wire Wire Line
	4950 3450 5200 3450
Wire Wire Line
	4250 3250 4050 3250
Wire Wire Line
	1650 3650 1900 3650
Wire Wire Line
	1650 4050 1900 4050
Wire Wire Line
	950  3850 750  3850
Wire Wire Line
	3300 3650 3550 3650
Wire Wire Line
	3300 4050 3550 4050
Wire Wire Line
	2600 3850 2400 3850
Wire Wire Line
	4950 3650 5200 3650
Wire Wire Line
	4950 4050 5200 4050
Wire Wire Line
	4250 3850 4050 3850
Wire Wire Line
	1650 4200 1900 4200
Wire Wire Line
	1650 4600 1900 4600
Wire Wire Line
	950  4400 750  4400
Wire Wire Line
	3300 4200 3550 4200
Wire Wire Line
	3300 4600 3550 4600
Wire Wire Line
	2600 4400 2400 4400
Wire Wire Line
	4950 4200 5200 4200
Wire Wire Line
	4950 4600 5200 4600
Wire Wire Line
	4250 4400 4050 4400
Wire Wire Line
	1650 4800 1900 4800
Wire Wire Line
	1650 5200 1900 5200
Wire Wire Line
	950  5000 750  5000
Wire Wire Line
	3300 4800 3550 4800
Wire Wire Line
	3300 5200 3550 5200
Wire Wire Line
	2600 5000 2400 5000
Wire Wire Line
	4950 4800 5200 4800
Wire Wire Line
	4950 5200 5200 5200
Wire Wire Line
	4250 5000 4050 5000
Wire Wire Line
	1650 5400 1900 5400
Wire Wire Line
	1650 5800 1900 5800
Wire Wire Line
	950  5600 750  5600
Wire Wire Line
	3300 5400 3550 5400
Wire Wire Line
	3300 5800 3550 5800
Wire Wire Line
	2600 5600 2400 5600
Wire Wire Line
	4950 5400 5200 5400
Wire Wire Line
	4950 5800 5200 5800
Wire Wire Line
	4250 5600 4050 5600
Wire Wire Line
	1650 6000 1900 6000
Wire Wire Line
	1650 6400 1900 6400
Wire Wire Line
	950  6200 750  6200
Wire Wire Line
	3300 6000 3550 6000
Wire Wire Line
	3300 6400 3550 6400
Wire Wire Line
	2600 6200 2400 6200
Wire Wire Line
	4950 6000 5200 6000
Wire Wire Line
	4950 6400 5200 6400
Wire Wire Line
	4250 6200 4050 6200
Wire Wire Line
	1650 6500 1900 6500
Wire Wire Line
	1650 6900 1900 6900
Wire Wire Line
	950  6700 750  6700
Wire Wire Line
	4950 6500 5200 6500
Wire Wire Line
	4950 6900 5200 6900
Wire Wire Line
	4250 6700 4050 6700
Wire Notes Line
	400  800  5400 800 
Wire Notes Line
	400  7150 5400 7150
Wire Wire Line
	8000 950  8250 950 
Wire Wire Line
	8250 1050 8000 1050
Wire Wire Line
	8250 1150 8000 1150
Wire Wire Line
	8250 1250 8000 1250
Wire Wire Line
	8250 1350 8000 1350
Wire Wire Line
	8250 1450 8000 1450
Wire Wire Line
	8250 1550 8000 1550
Wire Wire Line
	8250 1650 8000 1650
Wire Wire Line
	8250 2200 8000 2200
Wire Wire Line
	8250 2300 8000 2300
Wire Wire Line
	8250 2400 8000 2400
Wire Wire Line
	8250 2500 8000 2500
Wire Wire Line
	8250 2600 8000 2600
Wire Wire Line
	8250 2700 8000 2700
Wire Wire Line
	8250 2800 8000 2800
Wire Wire Line
	8250 2900 8000 2900
Wire Wire Line
	8250 3400 8000 3400
Wire Wire Line
	8250 3500 8000 3500
Wire Wire Line
	8250 3600 8000 3600
Wire Wire Line
	8250 3700 8000 3700
Wire Wire Line
	8250 3800 8000 3800
Wire Wire Line
	8250 3900 8000 3900
Wire Wire Line
	8250 4000 8000 4000
Wire Wire Line
	8250 4100 8000 4100
Wire Wire Line
	8250 4650 8000 4650
Wire Wire Line
	8250 4750 8000 4750
Wire Wire Line
	8250 4850 8000 4850
Wire Wire Line
	8250 4950 8000 4950
Wire Wire Line
	9350 1450 10050 1450
Wire Wire Line
	10050 1450 10050 6950
Wire Wire Line
	9350 1550 10050 1550
Connection ~ 10050 1550
Wire Wire Line
	9350 1650 10050 1650
Connection ~ 10050 1650
Wire Wire Line
	9350 2700 10050 2700
Connection ~ 10050 2700
Wire Wire Line
	9350 2800 10050 2800
Connection ~ 10050 2800
Wire Wire Line
	9350 2900 10050 2900
Connection ~ 10050 2900
Wire Wire Line
	9350 3900 10050 3900
Connection ~ 10050 3900
Wire Wire Line
	9350 4000 10050 4000
Connection ~ 10050 4000
Wire Wire Line
	9350 4100 10050 4100
Connection ~ 10050 4100
Wire Wire Line
	9350 5150 10050 5150
Connection ~ 10050 5150
Wire Wire Line
	9350 5250 10050 5250
Connection ~ 10050 5250
Wire Wire Line
	9350 5350 10050 5350
Connection ~ 10050 5350
Wire Wire Line
	9350 950  10650 950 
Wire Wire Line
	10650 700  10650 5850
Wire Wire Line
	10650 4650 9350 4650
Connection ~ 10650 950 
Wire Wire Line
	9350 2200 10650 2200
Connection ~ 10650 2200
Wire Wire Line
	9350 3400 10650 3400
Connection ~ 10650 3400
Wire Wire Line
	9350 1050 9700 1050
Wire Wire Line
	9350 2300 9650 2300
Wire Wire Line
	9350 3500 9650 3500
Wire Wire Line
	9350 4750 9650 4750
Wire Wire Line
	8250 6250 7900 6250
Wire Wire Line
	8250 5850 7900 5850
Wire Wire Line
	7900 6150 8250 6150
Wire Wire Line
	7900 6050 8250 6050
Wire Wire Line
	7900 5950 8250 5950
Wire Wire Line
	6000 6700 6000 6350
Wire Wire Line
	6000 7550 6000 7200
Wire Notes Line
	5400 7150 5400 800 
Wire Notes Line
	400  7150 400  800 
Wire Wire Line
	6500 5000 6650 5000
Wire Wire Line
	6500 5700 6650 5700
Wire Wire Line
	6650 6450 6500 6450
Wire Wire Line
	6500 7300 6650 7300
Connection ~ 6650 7300
Wire Wire Line
	6100 7300 6000 7300
Connection ~ 6000 7300
Wire Wire Line
	6100 6450 6000 6450
Connection ~ 6000 6450
Wire Wire Line
	6100 5700 6000 5700
Wire Wire Line
	6100 5000 6000 5000
Wire Wire Line
	6000 5700 6000 5950
Wire Wire Line
	6000 5000 6000 5250
Wire Wire Line
	6650 5000 6650 4800
Wire Wire Line
	6650 5700 6650 5500
$Comp
L 4051 IC2
U 1 1 59AB78CB
P 8800 6200
F 0 "IC2" H 8400 6700 50  0000 L CNN
F 1 "4051" H 8400 5700 50  0000 L CNN
F 2 "Packages_DIP:DIP-16_300" H 9250 6000 60  0001 C CNN
F 3 "" H 9250 6000 60  0000 C CNN
	1    8800 6200
	1    0    0    -1  
$EndComp
Text HLabel 9350 6050 2    50   Input ~ 0
PC13
Text HLabel 9350 6150 2    50   Input ~ 0
PC14
Text HLabel 9350 6250 2    50   Input ~ 0
PC15
Wire Wire Line
	9350 6350 10050 6350
Connection ~ 10050 6350
Wire Wire Line
	9350 6450 10050 6450
Connection ~ 10050 6450
Wire Wire Line
	9350 6550 10050 6550
Connection ~ 10050 6550
Wire Wire Line
	10650 5850 9350 5850
Connection ~ 10650 4650
Wire Wire Line
	9350 5950 9600 5950
Wire Wire Line
	8250 5050 8000 5050
NoConn ~ 8250 5150
NoConn ~ 8250 5250
NoConn ~ 8250 5350
NoConn ~ 8250 6350
NoConn ~ 8250 6450
NoConn ~ 8250 6550
NoConn ~ 8000 5050
Wire Wire Line
	6650 7450 6650 6450
$EndSCHEMATC
