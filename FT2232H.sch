EESchema Schematic File Version 2
LIBS:Remote_tester-rescue
LIBS:BJT
LIBS:Capacitor
LIBS:Diode_Rectifier
LIBS:Diode_Schottky
LIBS:Diode_Switching
LIBS:Diode_Zener
LIBS:EEPROM
LIBS:Inductor
LIBS:Microcontrollers
LIBS:Miscellaneous_Connectors
LIBS:Miscellaneous_Devices
LIBS:Modules
LIBS:MOSFET
LIBS:MOV
LIBS:Others
LIBS:Power
LIBS:PTC
LIBS:Regulator
LIBS:Resistor
LIBS:TBU
LIBS:TVS
LIBS:onsemi
LIBS:battery_management
LIBS:device
LIBS:opamp
LIBS:Remote_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title "RS232 Interface Circuit"
Date "2016-07-18"
Rev "1.0"
Comp "PT. Fusi Global Teknologi"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 8550 3000 2    50   Output ~ 0
TX
Text HLabel 8550 3100 2    50   Input ~ 0
RX
Text HLabel 7250 5400 3    50   Input ~ 0
GND
$Comp
L PGB1010603 D5
U 1 1 57B97A73
P 2050 3500
F 0 "D5" H 2050 3600 50  0000 C CNN
F 1 "PGB1010603" H 2050 3400 50  0000 C CNN
F 2 "TVSs:PGB1010603MR" H 2050 3500 60  0001 C CNN
F 3 "" H 2050 3500 60  0000 C CNN
F 4 "TVS DIODE 24VWM 150VC 0603" H 2050 3500 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2050 3500 60  0001 C CNN "Packaging"
F 6 "PGB1010603MR" H 2050 3500 60  0001 C CNN "Part Number"
F 7 "Littelfuse Inc." H 2050 3500 60  0001 C CNN "Manufacturer"
F 8 "F2594TR-ND" H 2050 3500 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/littelfuse-inc/PGB1010603MR/F2594TR-ND/715755" H 2050 3500 60  0001 C CNN "1st Distributor Link"
	1    2050 3500
	0    -1   -1   0   
$EndComp
Text HLabel 2100 4400 0    50   Output ~ 0
VUSB
Text HLabel 9750 3250 0    50   Input ~ 0
+3.3V
$Comp
L 330/0.100W R77
U 1 1 59281404
P 9600 3650
F 0 "R77" H 9600 3750 50  0000 C CNN
F 1 "330/0.100W" H 9600 3550 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9600 3580 30  0001 C CNN
F 3 "" V 9600 3650 30  0000 C CNN
F 4 "RES SMD 330 OHM 5% 1/10W 0603" H 9600 3650 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 9600 3650 60  0001 C CNN "Packaging"
F 6 "RC0603JR-07330RL" H 9600 3650 60  0001 C CNN "Part Number"
F 7 "Yageo" H 9600 3650 60  0001 C CNN "Manufacturer"
F 8 "311-330GRTR-ND" H 9600 3650 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-07330RL/311-330GRTR-ND/726769" H 9600 3650 60  0001 C CNN "1st Distributor Link"
	1    9600 3650
	0    1    1    0   
$EndComp
$Comp
L LED LD41
U 1 1 59281832
P 9600 4100
F 0 "LD41" H 9600 4200 50  0000 C CNN
F 1 "LED" H 9600 4000 50  0000 C CNN
F 2 "LEDs:LED_Chip_SMD_0805" H 9500 4090 60  0001 C CNN
F 3 "" H 9600 4100 60  0000 C CNN
F 4 "LED BLUE CLEAR 0603 SMD" H 9600 4100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 9600 4100 60  0001 C CNN "Packaging"
F 6 "LTST-C193TBKT-5A" H 9600 4100 60  0001 C CNN "Part Number"
F 7 "Lite-On Inc." H 9600 4100 60  0001 C CNN "Manufacturer"
F 8 "160-1827-2-ND" H 9600 4100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/lite-on-inc/LTST-C193TBKT-5A/160-1827-2-ND/2053656" H 9600 4100 60  0001 C CNN "1st Distributor Link"
	1    9600 4100
	0    1    1    0   
$EndComp
$Comp
L LED LD42
U 1 1 5928194F
P 10100 4100
F 0 "LD42" H 10100 4200 50  0000 C CNN
F 1 "LED" H 10100 4000 50  0000 C CNN
F 2 "LEDs:LED_Chip_SMD_0805" H 10000 4090 60  0001 C CNN
F 3 "" H 10100 4100 60  0000 C CNN
F 4 "LED WHITE 0603 SMD" H 10100 4100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 10100 4100 60  0001 C CNN "Packaging"
F 6 "LTW-C191TS5" H 10100 4100 60  0001 C CNN "Part Number"
F 7 "Lite-On Inc." H 10100 4100 60  0001 C CNN "Manufacturer"
F 8 "160-1724-2-ND" H 10100 4100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/lite-on-inc/LTW-C191TS5/160-1724-2-ND/726332" H 10100 4100 60  0001 C CNN "1st Distributor Link"
	1    10100 4100
	0    1    1    0   
$EndComp
Text Label 8550 4200 0    50   ~ 0
~RXLED
Text Label 8550 4300 0    50   ~ 0
~TXLED
Text Label 9300 4350 0    50   ~ 0
~RXLED
Text Label 9800 4350 0    50   ~ 0
~TXLED
NoConn ~ 8500 4400
NoConn ~ 8500 4500
NoConn ~ 8500 4600
NoConn ~ 8500 4100
NoConn ~ 8500 4000
Text HLabel 8550 3900 2    50   Output ~ 0
TXDEN
Text HLabel 8550 3200 2    50   Output ~ 0
~RTS
Text HLabel 8550 3300 2    50   Input ~ 0
~CTS
Text HLabel 8550 3400 2    50   Output ~ 0
~DTR
Text HLabel 8550 3500 2    50   Input ~ 0
~DSR
Text HLabel 8550 3600 2    50   Input ~ 0
~DCD
Text HLabel 8550 3700 2    50   Input ~ 0
~RI
Wire Wire Line
	1950 3000 2050 3000
Wire Wire Line
	2050 3000 2050 3300
Wire Wire Line
	1850 3100 2350 3100
Wire Wire Line
	2350 3100 2350 3300
Wire Wire Line
	1850 3200 2650 3200
Wire Wire Line
	2050 3700 2050 3800
Wire Wire Line
	1700 3800 2650 3800
Wire Wire Line
	2350 3700 2350 3800
Connection ~ 2350 3800
Wire Wire Line
	2650 3800 2650 3700
Wire Wire Line
	2650 3200 2650 3300
Connection ~ 2050 3800
Wire Wire Line
	6900 5350 6900 5300
Wire Wire Line
	4900 5350 7600 5350
Wire Wire Line
	7600 5350 7600 5300
Wire Wire Line
	7500 5300 7500 5350
Connection ~ 7500 5350
Wire Wire Line
	7400 5300 7400 5350
Connection ~ 7400 5350
Wire Wire Line
	7300 5300 7300 5350
Connection ~ 7300 5350
Wire Wire Line
	7200 5300 7200 5350
Connection ~ 7200 5350
Wire Wire Line
	7100 5300 7100 5350
Connection ~ 7100 5350
Wire Wire Line
	7000 5300 7000 5350
Connection ~ 7000 5350
Wire Wire Line
	7250 5400 7250 5350
Connection ~ 7250 5350
Wire Wire Line
	9850 3250 9850 3350
Wire Wire Line
	9600 3350 10100 3350
Wire Wire Line
	10100 3350 10100 3450
Wire Wire Line
	9600 3350 9600 3450
Connection ~ 9850 3350
Wire Wire Line
	9750 3250 9850 3250
Wire Wire Line
	10100 3850 10100 3900
Wire Wire Line
	9600 3850 9600 3900
Wire Wire Line
	8500 4200 8800 4200
Wire Wire Line
	8500 4300 8750 4300
Wire Wire Line
	9300 4350 9600 4350
Wire Wire Line
	9800 4350 10100 4350
Wire Wire Line
	9600 4350 9600 4300
Wire Wire Line
	10100 4350 10100 4300
Wire Wire Line
	8550 3000 8500 3000
Wire Wire Line
	8550 3100 8500 3100
Wire Wire Line
	8550 3200 8500 3200
Wire Wire Line
	8550 3300 8500 3300
Wire Wire Line
	8550 3900 8500 3900
Wire Wire Line
	8550 3400 8500 3400
Wire Wire Line
	8550 3500 8500 3500
Wire Wire Line
	8550 3600 8500 3600
Wire Wire Line
	8550 3700 8500 3700
Wire Wire Line
	6700 5350 6700 5300
Connection ~ 6900 5350
Wire Wire Line
	6100 4900 6050 4900
Wire Wire Line
	6050 4900 6050 5350
Connection ~ 6700 5350
$Comp
L USB-OTG USB1
U 1 1 59287929
P 2450 4650
F 0 "USB1" H 2250 5000 50  0000 L CNN
F 1 "USB-OTG" H 2250 4300 50  0000 L CNN
F 2 "Connectors_USB:USB_Micro_B_Plug_x1_Horizontal_SMD_2" H 2300 4750 10  0001 C CNN
F 3 "" H 2300 4750 60  0000 C CNN
F 4 "CONN USB MICRO B RECPT SMT R/A" H 2450 4650 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2450 4650 60  0001 C CNN "Packaging"
F 6 "10118194-0001LF" H 2450 4650 60  0001 C CNN "Part Number"
F 7 "Amphenol FCI" H 2450 4650 60  0001 C CNN "Manufacturer"
F 8 "609-4618-2-ND" H 2450 4650 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/amphenol-fci/10118194-0001LF/609-4618-2-ND/2785389" H 2450 4650 60  0001 C CNN "1st Distributor Link"
	1    2450 4650
	1    0    0    -1  
$EndComp
Text Label 1950 4500 0    50   ~ 0
D-
Text Label 1950 4600 0    50   ~ 0
D+
Wire Wire Line
	1950 4500 2100 4500
Wire Wire Line
	1950 4600 2100 4600
NoConn ~ 2100 4700
Text HLabel 1950 3000 0    50   Input ~ 0
VUSB
Text Label 1850 3100 0    50   ~ 0
D-
Text Label 1850 3200 0    50   ~ 0
D+
Text Label 5950 2200 0    50   ~ 0
D-
Text Label 5950 2300 0    50   ~ 0
D+
Wire Wire Line
	5950 2200 6100 2200
Wire Wire Line
	5950 2300 6100 2300
Text HLabel 8550 1200 2    50   Output ~ 0
TCK
Wire Wire Line
	8500 1200 8550 1200
Wire Wire Line
	8500 1300 8800 1300
Wire Wire Line
	8500 1400 8800 1400
Wire Wire Line
	8550 1500 8500 1500
NoConn ~ 8500 1600
NoConn ~ 8500 1700
NoConn ~ 8500 1800
NoConn ~ 8500 1900
NoConn ~ 8500 2100
NoConn ~ 8500 2200
NoConn ~ 8500 2300
NoConn ~ 8500 2400
NoConn ~ 8500 2500
NoConn ~ 8500 2600
NoConn ~ 8500 2700
NoConn ~ 8500 2800
Text HLabel 2050 4800 0    50   Input ~ 0
GND
Wire Wire Line
	2050 4800 2100 4800
Text Label 8600 1300 0    50   ~ 0
TDI
Text Label 8600 1400 0    50   ~ 0
TDO
Wire Wire Line
	8900 1500 8800 1500
Wire Wire Line
	8800 1500 8800 1400
Wire Wire Line
	8800 1300 8800 1200
Wire Wire Line
	8800 1200 8900 1200
Text HLabel 9500 1350 2    50   BiDi ~ 0
SWDIO
Wire Wire Line
	9500 1350 9400 1350
Wire Wire Line
	9400 1200 9400 1500
Wire Wire Line
	9400 1200 9300 1200
Wire Wire Line
	9400 1500 9300 1500
Connection ~ 9400 1350
$Comp
L +1V8 #PWR04
U 1 1 593D77C4
P 5700 1100
F 0 "#PWR04" H 5700 1050 50  0001 C CNN
F 1 "+1V8" H 5700 1250 50  0000 C CNN
F 2 "" H 5700 1100 50  0000 C CNN
F 3 "" H 5700 1100 50  0000 C CNN
	1    5700 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1100 5700 1500
Wire Wire Line
	5700 1400 6100 1400
Wire Wire Line
	6000 1200 6100 1200
$Comp
L NP-3.3uF C23
U 1 1 593E8055
P 5700 1700
F 0 "C23" H 5740 1800 50  0000 L CNN
F 1 "3.3uF" H 5740 1590 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_1206" H 5738 1550 30  0001 C CNN
F 3 "" H 5700 1700 60  0000 C CNN
F 4 "CAP CER 3.3UF 16V X7R 1206" H 5700 1700 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5700 1700 60  0001 C CNN "Packaging"
F 6 "C1206C335K4RACTU" H 5700 1700 60  0001 C CNN "Part Number"
F 7 "KEMET" H 5700 1700 60  0001 C CNN "Manufacturer"
F 8 "399-4944-2-ND" H 5700 1700 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/kemet/C1206C335K4RACTU/399-4944-2-ND/1090849" H 5700 1700 60  0001 C CNN "1st Distributor Link"
	1    5700 1700
	1    0    0    -1  
$EndComp
Connection ~ 5700 1400
Text HLabel 5600 2000 0    50   Input ~ 0
GND
Wire Wire Line
	5600 2000 5700 2000
Wire Wire Line
	5700 2000 5700 1900
Text HLabel 6000 1200 0    50   Input ~ 0
+3.3V
$Comp
L +1V8 #PWR05
U 1 1 593E8969
P 7200 800
F 0 "#PWR05" H 7200 750 50  0001 C CNN
F 1 "+1V8" H 7200 950 50  0000 C CNN
F 2 "" H 7200 800 50  0000 C CNN
F 3 "" H 7200 800 50  0000 C CNN
	1    7200 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 850  7100 900 
Wire Wire Line
	7100 850  7300 850 
Wire Wire Line
	7200 800  7200 900 
Wire Wire Line
	7300 850  7300 900 
Connection ~ 7200 850 
Text HLabel 7900 850  2    50   Input ~ 0
+3.3V
Wire Wire Line
	7500 850  7900 850 
Wire Wire Line
	7500 850  7500 900 
Wire Wire Line
	7600 900  7600 850 
Connection ~ 7600 850 
Wire Wire Line
	7700 900  7700 850 
Connection ~ 7700 850 
Wire Wire Line
	7800 900  7800 850 
Connection ~ 7800 850 
$Comp
L +1V8 #PWR06
U 1 1 593E97CD
P 1450 1000
F 0 "#PWR06" H 1450 950 50  0001 C CNN
F 1 "+1V8" H 1450 1150 50  0000 C CNN
F 2 "" H 1450 1000 50  0000 C CNN
F 3 "" H 1450 1000 50  0000 C CNN
	1    1450 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1000 1450 1100
Text HLabel 1050 1550 0    50   Input ~ 0
GND
Wire Wire Line
	1050 1550 1800 1550
Wire Wire Line
	1450 1550 1450 1500
Wire Wire Line
	1100 1450 1100 1550
Wire Wire Line
	1800 1550 1800 1500
Connection ~ 1450 1550
Wire Wire Line
	1800 1050 1800 1100
Connection ~ 1450 1050
Connection ~ 1100 1550
Text HLabel 1050 2350 0    50   Input ~ 0
GND
Wire Wire Line
	1050 2350 2150 2350
Wire Wire Line
	1450 2350 1450 2300
Wire Wire Line
	1100 2300 1100 2350
Wire Wire Line
	1800 2350 1800 2300
Connection ~ 1450 2350
Wire Wire Line
	1100 1900 1100 1850
Wire Wire Line
	1050 1850 2150 1850
Wire Wire Line
	1800 1850 1800 1900
Connection ~ 1450 1850
Connection ~ 1100 2350
Wire Wire Line
	1450 1850 1450 1900
Text HLabel 1050 1850 0    50   Input ~ 0
+3.3V
Connection ~ 1100 1850
Wire Wire Line
	2150 1850 2150 1900
Connection ~ 1800 1850
Wire Wire Line
	2150 2350 2150 2300
Connection ~ 1800 2350
$Comp
L FERRITE-BEAD FB4
U 1 1 593EB090
P 3000 1750
F 0 "FB4" H 3000 1850 50  0000 C CNN
F 1 "FERRITE-BEAD" H 3000 1650 50  0000 C CNN
F 2 "Ferrite_Beads:FB_0805" H 2900 1770 60  0001 C CNN
F 3 "" H 3000 1870 60  0000 C CNN
F 4 "FERRITE BEAD 40 OHM 0805 1LN" H 3000 1750 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3000 1750 60  0001 C CNN "Packaging"
F 6 "MI0805K400R-10" H 3000 1750 60  0001 C CNN "Part Number"
F 7 "Laird-Signal Integrity Products" H 3000 1750 60  0001 C CNN "Manufacturer"
F 8 "240-2389-2-ND" H 3000 1750 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/laird-signal-integrity-products/MI0805K400R-10/240-2389-2-ND/806627" H 3000 1750 60  0001 C CNN "1st Distributor Link"
	1    3000 1750
	1    0    0    -1  
$EndComp
Text HLabel 2700 1750 0    50   Input ~ 0
+3.3V
Wire Wire Line
	2700 1750 2800 1750
Wire Wire Line
	3300 1850 3300 1750
Wire Wire Line
	3200 1750 3600 1750
Wire Wire Line
	3600 1750 3600 1850
Connection ~ 3300 1750
Text Label 3350 1750 0    50   ~ 0
VPLL
Text HLabel 2700 2350 0    50   Input ~ 0
GND
Wire Wire Line
	2700 2350 3600 2350
Wire Wire Line
	3600 2350 3600 2250
Wire Wire Line
	3300 2250 3300 2350
Connection ~ 3300 2350
Text HLabel 2700 1000 0    50   Input ~ 0
+3.3V
$Comp
L NP-100nF C21
U 1 1 593ECC56
P 3600 1300
F 0 "C21" H 3640 1400 50  0000 L CNN
F 1 "100nF" H 3640 1190 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3638 1150 30  0001 C CNN
F 3 "" H 3600 1300 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3600 1300 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3600 1300 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 3600 1300 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 3600 1300 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 3600 1300 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3600 1300 60  0001 C CNN "1st Distributor Link"
	1    3600 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1000 2800 1000
$Comp
L NP-4.7uF C45
U 1 1 593ECC5D
P 3300 1300
F 0 "C45" H 3340 1400 50  0000 L CNN
F 1 "4.7uF" H 3340 1190 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_1206" H 3338 1150 30  0001 C CNN
F 3 "" H 3300 1300 60  0000 C CNN
F 4 "CAP CER 4.7UF 16V X7R 1206" H 3300 1300 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3300 1300 60  0001 C CNN "Packaging"
F 6 "C1206C475K4RACTU" H 3300 1300 60  0001 C CNN "Part Number"
F 7 "KEMET" H 3300 1300 60  0001 C CNN "Manufacturer"
F 8 "399-3524-2-ND" H 3300 1300 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/kemet/C1206C475K4RACTU/399-3524-2-ND/789656" H 3300 1300 60  0001 C CNN "1st Distributor Link"
	1    3300 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 1100 3300 1000
Wire Wire Line
	3200 1000 3700 1000
Wire Wire Line
	3600 1000 3600 1100
Connection ~ 3300 1000
Text Label 3350 1000 0    50   ~ 0
VPHY
Text HLabel 2700 1600 0    50   Input ~ 0
GND
Wire Wire Line
	2700 1600 3600 1600
Wire Wire Line
	3600 1600 3600 1500
Wire Wire Line
	3300 1500 3300 1600
Connection ~ 3300 1600
Text Label 6900 900  1    50   ~ 0
VPLL
Wire Wire Line
	6900 900  6900 700 
Text Label 6800 900  1    50   ~ 0
VPHY
Wire Wire Line
	6800 900  6800 700 
$Comp
L PWR_FLAG #FLG07
U 1 1 593EE1DE
P 3700 950
F 0 "#FLG07" H 3700 900 50  0001 C CNN
F 1 "PWR_FLAG" H 3700 1150 50  0000 C CNN
F 2 "" H 3700 950 50  0000 C CNN
F 3 "" H 3700 950 50  0000 C CNN
	1    3700 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 1000 3700 950 
Connection ~ 3600 1000
Text HLabel 8550 1500 2    50   Output ~ 0
TMS
Text HLabel 1700 3800 0    50   Input ~ 0
GND
NoConn ~ 2100 4900
$Comp
L FT2232H IC14
U 1 1 593D705B
P 7300 3100
F 0 "IC14" H 6250 5200 50  0000 L CNN
F 1 "FT2232H" H 6250 1000 50  0000 L CNN
F 2 "Packages_LQFP:LQFP-64" H 7300 3100 50  0001 C CNN
F 3 "" H 7300 3100 50  0001 C CNN
F 4 "IC USB HS DUAL UART/FIFO 64-LQFP" H 7300 3100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 7300 3100 60  0001 C CNN "Packaging"
F 6 "FT2232HL-REEL" H 7300 3100 60  0001 C CNN "Part Number"
F 7 "FTDI, Future Technology Devices International Ltd" H 7300 3100 60  0001 C CNN "Manufacturer"
F 8 "768-1024-2-ND" H 7300 3100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/ftdi-future-technology-devices-international-ltd/FT2232HL-REEL/768-1024-2-ND/1986053" H 7300 3100 60  0001 C CNN "1st Distributor Link"
	1    7300 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 2500 6100 2500
Text HLabel 5500 2500 0    50   Input ~ 0
GND
Wire Wire Line
	5500 2500 5600 2500
Wire Wire Line
	6100 2700 6050 2700
Wire Wire Line
	6050 2700 6050 2850
Wire Wire Line
	6050 2850 6000 2850
Text HLabel 5500 2850 0    50   Input ~ 0
+3.3V
Wire Wire Line
	5500 2850 5600 2850
$Comp
L AT93C46-8S1 IC15
U 1 1 593E3C16
P 2700 6150
F 0 "IC15" H 2400 6450 50  0000 L CNN
F 1 "AT93C46-8S1" H 2400 5850 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 2850 6400 60  0001 C CNN
F 3 "" H 2850 6400 60  0000 C CNN
F 4 "IC EEPROM 1KBIT 2MHZ 8SOIC" H 2700 6150 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2700 6150 60  0001 C CNN "Packaging"
F 6 "CAT93C46VI-GT3" H 2700 6150 60  0001 C CNN "Part Number"
F 7 "ON Semiconductor" H 2700 6150 60  0001 C CNN "Manufacturer"
F 8 "CAT93C46VI-GT3TR-ND" H 2700 6150 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/on-semiconductor/CAT93C46VI-GT3/CAT93C46VI-GT3TR-ND/1631112" H 2700 6150 60  0001 C CNN "1st Distributor Link"
	1    2700 6150
	1    0    0    -1  
$EndComp
Text Label 5850 3800 0    50   ~ 0
EECS
Text Label 5850 3900 0    50   ~ 0
EECLK
Text Label 5850 4000 0    50   ~ 0
EEDATA
Wire Wire Line
	5850 3900 6100 3900
Wire Wire Line
	5850 3800 6100 3800
Wire Wire Line
	5850 4000 6100 4000
Text HLabel 3150 5750 0    50   Input ~ 0
+3.3V
Wire Wire Line
	3150 5750 3250 5750
Wire Wire Line
	3250 5750 3250 6200
Wire Wire Line
	3150 6000 3550 6000
Wire Wire Line
	3250 6200 3150 6200
Connection ~ 3250 6000
Wire Wire Line
	3550 6000 3550 6100
Text HLabel 3150 6600 0    50   Input ~ 0
GND
Wire Wire Line
	3150 6600 3550 6600
Wire Wire Line
	3550 6600 3550 6500
Wire Wire Line
	3150 6300 3250 6300
Wire Wire Line
	3250 6300 3250 6600
Connection ~ 3250 6600
Wire Wire Line
	3200 6300 3200 6100
Wire Wire Line
	3200 6100 3150 6100
Connection ~ 3200 6300
$Comp
L 10K/0.100W R79
U 1 1 593E6576
P 1550 5700
F 0 "R79" H 1550 5800 50  0000 C CNN
F 1 "10K/0.100W" H 1550 5600 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1550 5630 30  0001 C CNN
F 3 "" V 1550 5700 30  0000 C CNN
F 4 "RES SMD 10K OHM 5% 1/10W 0603" H 1550 5700 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1550 5700 60  0001 C CNN "Packaging"
F 6 "RC0603JR-0710KL" H 1550 5700 60  0001 C CNN "Part Number"
F 7 "Yageo" H 1550 5700 60  0001 C CNN "Manufacturer"
F 8 "311-10KGRTR-ND" H 1550 5700 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-0710KL/311-10KGRTR-ND/726700" H 1550 5700 60  0001 C CNN "1st Distributor Link"
	1    1550 5700
	0    1    1    0   
$EndComp
Text HLabel 1450 5400 0    50   Input ~ 0
+3.3V
Wire Wire Line
	1450 5400 2150 5400
Wire Wire Line
	2150 5400 2150 5500
Wire Wire Line
	1850 5500 1850 5400
Connection ~ 1850 5400
Wire Wire Line
	1550 5500 1550 5400
Connection ~ 1550 5400
Wire Wire Line
	2150 5900 2150 6000
Wire Wire Line
	1100 6000 2250 6000
Wire Wire Line
	1850 5900 1850 6100
Wire Wire Line
	1100 6100 2250 6100
Wire Wire Line
	1550 5900 1550 6700
$Comp
L 2.2K/0.100W R82
U 1 1 593E6D7C
P 1850 6500
F 0 "R82" H 1850 6600 50  0000 C CNN
F 1 "2.2K/0.100W" H 1850 6400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1850 6430 30  0001 C CNN
F 3 "" V 1850 6500 30  0000 C CNN
F 4 "RES SMD 2.2K OHM 5% 1/10W 0603" H 1850 6500 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1850 6500 60  0001 C CNN "Packaging"
F 6 "RC0603JR-072K2L" H 1850 6500 60  0001 C CNN "Part Number"
F 7 "Yageo" H 1850 6500 60  0001 C CNN "Manufacturer"
F 8 "311-2.2KGRTR-ND" H 1850 6500 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-072K2L/311-2.2KGRTR-ND/726729" H 1850 6500 60  0001 C CNN "1st Distributor Link"
	1    1850 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6300 2250 6700
Wire Wire Line
	2250 6700 1550 6700
Wire Wire Line
	2150 6200 2150 6500
Wire Wire Line
	1100 6200 2250 6200
Connection ~ 1550 6500
Wire Wire Line
	2150 6500 2050 6500
Wire Wire Line
	1550 6500 1650 6500
Connection ~ 2150 6200
Connection ~ 1850 6100
Connection ~ 2150 6000
Text Label 1100 6000 0    50   ~ 0
EECS
Text Label 1100 6100 0    50   ~ 0
EECLK
Text Label 1100 6200 0    50   ~ 0
EEDATA
$Comp
L CRYSTAL X3
U 1 1 593E8FD0
P 5300 4700
F 0 "X3" H 5300 4850 50  0000 C CNN
F 1 "12MHz" H 5300 4550 50  0000 C CNN
F 2 "Packages_HC:HC-18UV" H 5200 4750 60  0001 C CNN
F 3 "" H 5300 4700 60  0000 C CNN
F 4 "CRYSTAL 12.0000MHZ 20PF T/H" H 5300 4700 60  0001 C CNN "Description"
F 5 "Bulk" H 5300 4700 60  0001 C CNN "Packaging"
F 6 "9B-12.000MBBK-B" H 5300 4700 60  0001 C CNN "Part Number"
F 7 "TXC CORPORATION" H 5300 4700 60  0001 C CNN "Manufacturer"
F 8 "887-2011-ND" H 5300 4700 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/txc-corporation/9B-12.000MBBK-B/887-2011-ND/3522006" H 5300 4700 60  0001 C CNN "1st Distributor Link"
	1    5300 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4700 6100 4700
$Comp
L NP-27pF C36
U 1 1 593E9781
P 4900 5000
F 0 "C36" H 4940 5100 50  0000 L CNN
F 1 "27pF" H 4940 4890 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 4938 4850 30  0001 C CNN
F 3 "" H 4900 5000 60  0000 C CNN
F 4 "CAP CER 27PF 50V C0G/NP0 0603" H 4900 5000 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 4900 5000 60  0001 C CNN "Packaging"
F 6 "06035A270JAT2A" H 4900 5000 60  0001 C CNN "Part Number"
F 7 "AVX Corporation" H 4900 5000 60  0001 C CNN "Manufacturer"
F 8 "478-1168-2-ND" H 4900 5000 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/avx-corporation/06035A270JAT2A/478-1168-2-ND/563278" H 4900 5000 60  0001 C CNN "1st Distributor Link"
	1    4900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 4300 4900 4800
Wire Wire Line
	4900 4700 5000 4700
Wire Wire Line
	5700 4800 5700 4700
Connection ~ 5700 4700
Connection ~ 4900 4700
Wire Wire Line
	6100 4300 4900 4300
Wire Wire Line
	5700 5350 5700 5200
Connection ~ 6050 5350
Wire Wire Line
	4900 5350 4900 5200
Connection ~ 5700 5350
NoConn ~ 8500 4800
NoConn ~ 8500 4900
$Comp
L 12K/0.100W R83
U 1 1 593F2747
P 5800 2500
F 0 "R83" H 5800 2600 50  0000 C CNN
F 1 "12K/0.100W" H 5800 2400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5800 2430 30  0001 C CNN
F 3 "" V 5800 2500 30  0000 C CNN
F 4 "RES SMD 12K OHM 5% 1/10W 0603" H 5800 2500 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5800 2500 60  0001 C CNN "Packaging"
F 6 "RC0603JR-0712KL" H 5800 2500 60  0001 C CNN "Part Number"
F 7 "Yageo" H 5800 2500 60  0001 C CNN "Manufacturer"
F 8 "311-12KGRTR-ND" H 5800 2500 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-0712KL/311-12KGRTR-ND/726709" H 5800 2500 60  0001 C CNN "1st Distributor Link"
	1    5800 2500
	1    0    0    -1  
$EndComp
$Comp
L 1K/0.250W R74
U 1 1 593F30FC
P 5800 2850
F 0 "R74" H 5800 2950 50  0000 C CNN
F 1 "1K/0.250W" H 5800 2750 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_1206" H 5800 2780 30  0001 C CNN
F 3 "" V 5800 2850 30  0000 C CNN
F 4 "RES SMD 1K OHM 5% 1/4W 1206" H 5800 2850 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5800 2850 60  0001 C CNN "Packaging"
F 6 "RC1206JR-071KL" H 5800 2850 60  0001 C CNN "Part Number"
F 7 "Yageo" H 5800 2850 60  0001 C CNN "Manufacturer"
F 8 "311-1.0KERTR-ND" H 5800 2850 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC1206JR-071KL/311-1.0KERTR-ND/729186" H 5800 2850 60  0001 C CNN "1st Distributor Link"
	1    5800 2850
	1    0    0    -1  
$EndComp
$Comp
L 470/0.100W R75
U 1 1 595CB81D
P 9100 1200
F 0 "R75" H 9100 1300 50  0000 C CNN
F 1 "470/0.100W" H 9100 1100 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9100 1130 30  0001 C CNN
F 3 "" V 9100 1200 30  0000 C CNN
F 4 "RES SMD 470 OHM 5% 1/10W 0603" H 9100 1200 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 9100 1200 60  0001 C CNN "Packaging"
F 6 "RC0603JR-07470RL" H 9100 1200 60  0001 C CNN "Part Number"
F 7 "Yageo" H 9100 1200 60  0001 C CNN "Manufacturer"
F 8 "311-470GRTR-ND" H 9100 1200 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-07470RL/311-470GRTR-ND/726791" H 9100 1200 60  0001 C CNN "1st Distributor Link"
	1    9100 1200
	1    0    0    -1  
$EndComp
$Comp
L 0/0.100W R76
U 1 1 595CB8A6
P 9100 1500
F 0 "R76" H 9100 1600 50  0000 C CNN
F 1 "0/0.100W" H 9100 1400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9100 1430 30  0001 C CNN
F 3 "" V 9100 1500 30  0000 C CNN
F 4 "RES SMD 0.0OHM JUMPER 1/10W 0603" H 9100 1500 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 9100 1500 60  0001 C CNN "Packaging"
F 6 "RC0603JR-070RL" H 9100 1500 60  0001 C CNN "Part Number"
F 7 "Yageo" H 9100 1500 60  0001 C CNN "Manufacturer"
F 8 "311-0.0GRTR-ND" H 9100 1500 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-070RL/311-0.0GRTR-ND/726675" H 9100 1500 60  0001 C CNN "1st Distributor Link"
	1    9100 1500
	1    0    0    -1  
$EndComp
$Comp
L FERRITE-BEAD FB3
U 1 1 596E254B
P 3000 1000
F 0 "FB3" H 3000 1100 50  0000 C CNN
F 1 "FERRITE-BEAD" H 3000 900 50  0000 C CNN
F 2 "Ferrite_Beads:FB_0805" H 2900 1020 60  0001 C CNN
F 3 "" H 3000 1120 60  0000 C CNN
F 4 "FERRITE BEAD 40 OHM 0805 1LN" H 3000 1000 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3000 1000 60  0001 C CNN "Packaging"
F 6 "MI0805K400R-10" H 3000 1000 60  0001 C CNN "Part Number"
F 7 "Laird-Signal Integrity Products" H 3000 1000 60  0001 C CNN "Manufacturer"
F 8 "240-2389-2-ND" H 3000 1000 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/laird-signal-integrity-products/MI0805K400R-10/240-2389-2-ND/806627" H 3000 1000 60  0001 C CNN "1st Distributor Link"
	1    3000 1000
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C22
U 1 1 596E4778
P 3600 2050
F 0 "C22" H 3640 2150 50  0000 L CNN
F 1 "100nF" H 3640 1940 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3638 1900 30  0001 C CNN
F 3 "" H 3600 2050 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3600 2050 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3600 2050 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 3600 2050 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 3600 2050 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 3600 2050 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3600 2050 60  0001 C CNN "1st Distributor Link"
	1    3600 2050
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C41
U 1 1 596E4E6A
P 1100 2100
F 0 "C41" H 1140 2200 50  0000 L CNN
F 1 "100nF" H 1140 1990 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 1138 1950 30  0001 C CNN
F 3 "" H 1100 2100 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 1100 2100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1100 2100 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 1100 2100 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 1100 2100 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 1100 2100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 1100 2100 60  0001 C CNN "1st Distributor Link"
	1    1100 2100
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C39
U 1 1 596E4EF3
P 1450 1300
F 0 "C39" H 1490 1400 50  0000 L CNN
F 1 "100nF" H 1490 1190 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 1488 1150 30  0001 C CNN
F 3 "" H 1450 1300 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 1450 1300 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1450 1300 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 1450 1300 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 1450 1300 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 1450 1300 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 1450 1300 60  0001 C CNN "1st Distributor Link"
	1    1450 1300
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C42
U 1 1 596E4F87
P 1450 2100
F 0 "C42" H 1490 2200 50  0000 L CNN
F 1 "100nF" H 1490 1990 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 1488 1950 30  0001 C CNN
F 3 "" H 1450 2100 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 1450 2100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1450 2100 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 1450 2100 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 1450 2100 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 1450 2100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 1450 2100 60  0001 C CNN "1st Distributor Link"
	1    1450 2100
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C40
U 1 1 596E5020
P 1800 1300
F 0 "C40" H 1840 1400 50  0000 L CNN
F 1 "100nF" H 1840 1190 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 1838 1150 30  0001 C CNN
F 3 "" H 1800 1300 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 1800 1300 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1800 1300 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 1800 1300 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 1800 1300 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 1800 1300 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 1800 1300 60  0001 C CNN "1st Distributor Link"
	1    1800 1300
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C43
U 1 1 596E50B6
P 1800 2100
F 0 "C43" H 1840 2200 50  0000 L CNN
F 1 "100nF" H 1840 1990 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 1838 1950 30  0001 C CNN
F 3 "" H 1800 2100 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 1800 2100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1800 2100 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 1800 2100 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 1800 2100 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 1800 2100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 1800 2100 60  0001 C CNN "1st Distributor Link"
	1    1800 2100
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C44
U 1 1 596E5157
P 2150 2100
F 0 "C44" H 2190 2200 50  0000 L CNN
F 1 "100nF" H 2190 1990 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 2188 1950 30  0001 C CNN
F 3 "" H 2150 2100 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 2150 2100 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2150 2100 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 2150 2100 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 2150 2100 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 2150 2100 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 2150 2100 60  0001 C CNN "1st Distributor Link"
	1    2150 2100
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C35
U 1 1 596E6300
P 3550 6300
F 0 "C35" H 3590 6400 50  0000 L CNN
F 1 "100nF" H 3590 6190 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 3588 6150 30  0001 C CNN
F 3 "" H 3550 6300 60  0000 C CNN
F 4 "CAP CER 0.1UF 50V X7R 0603" H 3550 6300 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3550 6300 60  0001 C CNN "Packaging"
F 6 "GRM188R71H104KA93D" H 3550 6300 60  0001 C CNN "Part Number"
F 7 "Murata Electronics North America" H 3550 6300 60  0001 C CNN "Manufacturer"
F 8 "490-1519-2-ND" H 3550 6300 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/GRM188R71H104KA93D/490-1519-2-ND/587143" H 3550 6300 60  0001 C CNN "1st Distributor Link"
	1    3550 6300
	1    0    0    -1  
$EndComp
$Comp
L 10K/0.100W R80
U 1 1 596EB9A7
P 1850 5700
F 0 "R80" H 1850 5800 50  0000 C CNN
F 1 "10K/0.100W" H 1850 5600 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1850 5630 30  0001 C CNN
F 3 "" V 1850 5700 30  0000 C CNN
F 4 "RES SMD 10K OHM 5% 1/10W 0603" H 1850 5700 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 1850 5700 60  0001 C CNN "Packaging"
F 6 "RC0603JR-0710KL" H 1850 5700 60  0001 C CNN "Part Number"
F 7 "Yageo" H 1850 5700 60  0001 C CNN "Manufacturer"
F 8 "311-10KGRTR-ND" H 1850 5700 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-0710KL/311-10KGRTR-ND/726700" H 1850 5700 60  0001 C CNN "1st Distributor Link"
	1    1850 5700
	0    1    1    0   
$EndComp
$Comp
L 10K/0.100W R81
U 1 1 596EBA36
P 2150 5700
F 0 "R81" H 2150 5800 50  0000 C CNN
F 1 "10K/0.100W" H 2150 5600 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 2150 5630 30  0001 C CNN
F 3 "" V 2150 5700 30  0000 C CNN
F 4 "RES SMD 10K OHM 5% 1/10W 0603" H 2150 5700 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2150 5700 60  0001 C CNN "Packaging"
F 6 "RC0603JR-0710KL" H 2150 5700 60  0001 C CNN "Part Number"
F 7 "Yageo" H 2150 5700 60  0001 C CNN "Manufacturer"
F 8 "311-10KGRTR-ND" H 2150 5700 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-0710KL/311-10KGRTR-ND/726700" H 2150 5700 60  0001 C CNN "1st Distributor Link"
	1    2150 5700
	0    1    1    0   
$EndComp
$Comp
L PGB1010603 D6
U 1 1 596ED464
P 2350 3500
F 0 "D6" H 2350 3600 50  0000 C CNN
F 1 "PGB1010603" H 2350 3400 50  0000 C CNN
F 2 "TVSs:PGB1010603MR" H 2350 3500 60  0001 C CNN
F 3 "" H 2350 3500 60  0000 C CNN
F 4 "TVS DIODE 24VWM 150VC 0603" H 2350 3500 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2350 3500 60  0001 C CNN "Packaging"
F 6 "PGB1010603MR" H 2350 3500 60  0001 C CNN "Part Number"
F 7 "Littelfuse Inc." H 2350 3500 60  0001 C CNN "Manufacturer"
F 8 "F2594TR-ND" H 2350 3500 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/littelfuse-inc/PGB1010603MR/F2594TR-ND/715755" H 2350 3500 60  0001 C CNN "1st Distributor Link"
	1    2350 3500
	0    -1   -1   0   
$EndComp
$Comp
L PGB1010603 D7
U 1 1 596ED4ED
P 2650 3500
F 0 "D7" H 2650 3600 50  0000 C CNN
F 1 "PGB1010603" H 2650 3400 50  0000 C CNN
F 2 "TVSs:PGB1010603MR" H 2650 3500 60  0001 C CNN
F 3 "" H 2650 3500 60  0000 C CNN
F 4 "TVS DIODE 24VWM 150VC 0603" H 2650 3500 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 2650 3500 60  0001 C CNN "Packaging"
F 6 "PGB1010603MR" H 2650 3500 60  0001 C CNN "Part Number"
F 7 "Littelfuse Inc." H 2650 3500 60  0001 C CNN "Manufacturer"
F 8 "F2594TR-ND" H 2650 3500 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/littelfuse-inc/PGB1010603MR/F2594TR-ND/715755" H 2650 3500 60  0001 C CNN "1st Distributor Link"
	1    2650 3500
	0    -1   -1   0   
$EndComp
$Comp
L 330/0.100W R78
U 1 1 596F58A7
P 10100 3650
F 0 "R78" H 10100 3750 50  0000 C CNN
F 1 "330/0.100W" H 10100 3550 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 10100 3580 30  0001 C CNN
F 3 "" V 10100 3650 30  0000 C CNN
F 4 "RES SMD 330 OHM 5% 1/10W 0603" H 10100 3650 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 10100 3650 60  0001 C CNN "Packaging"
F 6 "RC0603JR-07330RL" H 10100 3650 60  0001 C CNN "Part Number"
F 7 "Yageo" H 10100 3650 60  0001 C CNN "Manufacturer"
F 8 "311-330GRTR-ND" H 10100 3650 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/yageo/RC0603JR-07330RL/311-330GRTR-ND/726769" H 10100 3650 60  0001 C CNN "1st Distributor Link"
	1    10100 3650
	0    1    1    0   
$EndComp
$Comp
L NP-27pF C37
U 1 1 596F2902
P 5700 5000
F 0 "C37" H 5740 5100 50  0000 L CNN
F 1 "27pF" H 5740 4890 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 5738 4850 30  0001 C CNN
F 3 "" H 5700 5000 60  0000 C CNN
F 4 "CAP CER 27PF 50V C0G/NP0 0603" H 5700 5000 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 5700 5000 60  0001 C CNN "Packaging"
F 6 "06035A270JAT2A" H 5700 5000 60  0001 C CNN "Part Number"
F 7 "AVX Corporation" H 5700 5000 60  0001 C CNN "Manufacturer"
F 8 "478-1168-2-ND" H 5700 5000 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/avx-corporation/06035A270JAT2A/478-1168-2-ND/563278" H 5700 5000 60  0001 C CNN "1st Distributor Link"
	1    5700 5000
	1    0    0    -1  
$EndComp
$Comp
L NP-4.7uF C20
U 1 1 596F3DB2
P 3300 2050
F 0 "C20" H 3340 2150 50  0000 L CNN
F 1 "4.7uF" H 3340 1940 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_1206" H 3338 1900 30  0001 C CNN
F 3 "" H 3300 2050 60  0000 C CNN
F 4 "CAP CER 4.7UF 16V X7R 1206" H 3300 2050 60  0001 C CNN "Description"
F 5 "Tape & Reel" H 3300 2050 60  0001 C CNN "Packaging"
F 6 "C1206C475K4RACTU" H 3300 2050 60  0001 C CNN "Part Number"
F 7 "KEMET" H 3300 2050 60  0001 C CNN "Manufacturer"
F 8 "399-3524-2-ND" H 3300 2050 60  0001 C CNN "1st Distributor Part Number"
F 9 "https://www.digikey.com/product-detail/en/kemet/C1206C475K4RACTU/399-3524-2-ND/789656" H 3300 2050 60  0001 C CNN "1st Distributor Link"
	1    3300 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1050 1100 1150
Wire Wire Line
	1100 1050 1800 1050
$Comp
L C C24
U 1 1 59AF916D
P 1100 1300
F 0 "C24" H 1125 1400 50  0000 L CNN
F 1 "100" H 1125 1200 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 1138 1150 50  0001 C CNN
F 3 "" H 1100 1300 50  0001 C CNN
	1    1100 1300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
