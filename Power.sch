EESchema Schematic File Version 2
LIBS:Remote_tester-rescue
LIBS:BJT
LIBS:Capacitor
LIBS:Diode_Rectifier
LIBS:Diode_Schottky
LIBS:Diode_Switching
LIBS:Diode_Zener
LIBS:EEPROM
LIBS:Inductor
LIBS:Microcontrollers
LIBS:Miscellaneous_Connectors
LIBS:Miscellaneous_Devices
LIBS:Modules
LIBS:MOSFET
LIBS:MOV
LIBS:Others
LIBS:Power
LIBS:PTC
LIBS:Regulator
LIBS:Resistor
LIBS:TBU
LIBS:TVS
LIBS:onsemi
LIBS:battery_management
LIBS:device
LIBS:opamp
LIBS:Remote_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title "Power Converter"
Date "2016-07-18"
Rev "1.1"
Comp "PT. Fusi Global Teknologi"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2550 2900 2    50   Output ~ 0
GND
$Comp
L PWR_FLAG #FLG08
U 1 1 5773D66F
P 2450 3050
F 0 "#FLG08" H 2450 3000 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 3250 50  0000 C CNN
F 2 "" H 2450 3050 50  0000 C CNN
F 3 "" H 2450 3050 50  0000 C CNN
	1    2450 3050
	-1   0    0    1   
$EndComp
Text HLabel 2550 2200 2    50   Output ~ 0
+5V
Text HLabel 2550 2700 2    50   Output ~ 0
+3.3V
$Comp
L DC-JACK J1
U 1 1 57741F9D
P 3600 4050
F 0 "J1" H 3250 4250 50  0000 L CNN
F 1 "DC-JACK" H 3250 3900 50  0000 L CNN
F 2 "Connectors_Power:DC_Jack_TH" H 3350 4150 10  0001 C CNN
F 3 "" H 3600 4050 60  0000 C CNN
	1    3600 4050
	1    0    0    -1  
$EndComp
Text Notes 4350 5350 2    50   ~ 0
DC Power Input
Text Notes 5200 5350 2    50   ~ 0
Power Indicator
Text Notes 9450 3500 2    50   ~ 0
12V -> 5V Voltage Regulator (Switching)
$Comp
L M7 D1
U 1 1 5774D363
P 7250 4000
F 0 "D1" H 7250 4090 50  0000 C CNN
F 1 "M7" H 7250 3900 50  0000 C CNN
F 2 "Packages_DO:DO-214AC" H 7150 3990 60  0001 C CNN
F 3 "" H 7250 4000 60  0000 C CNN
	1    7250 4000
	-1   0    0    1   
$EndComp
Text Notes 9600 5350 2    50   ~ 0
5V -> 3.3V Voltage Regulator (Linear)
Text Notes 2950 3500 2    50   ~ 0
To Module's Power Port
$Comp
L P-10uF/25V C2
U 1 1 57748DD8
P 5650 4700
F 0 "C2" H 5690 4805 50  0000 L CNN
F 1 "10uF/25V" H 5690 4555 50  0000 L CNN
F 2 "Capacitors:C_Tantalum_SMD_Kemet_Case_C" H 5650 4685 60  0001 C CNN
F 3 "" H 5650 4685 60  0000 C CNN
	1    5650 4700
	1    0    0    -1  
$EndComp
Text Notes 8100 4950 0    50   ~ 0
X7R
Text Notes 6500 4950 0    50   ~ 0
X7R
Text Notes 8500 5050 0    50   ~ 0
0.3 Ohm < ESR < 22 Ohm\nTantalum Type\n
Text Notes 5000 3100 0    50   ~ 0
Irms > 0.15A\nESR < 0.5 Ohm\n2000 Hours\nShort as possible
$Comp
L P-100uF/16V C1
U 1 1 57749D19
P 4950 2600
F 0 "C1" H 4990 2705 50  0000 L CNN
F 1 "100uF/16V" H 4990 2455 50  0000 L CNN
F 2 "Capacitors:C_Elec_SMD_D-6.3mm_L-5.5mm" H 4950 2585 60  0001 C CNN
F 3 "" H 4950 2585 60  0000 C CNN
	1    4950 2600
	1    0    0    -1  
$EndComp
Text Notes 3400 6000 0    50   ~ 0
NB:\n- Designed to be operated at 45 C environment\n- Isupply max = 0.3 A\n- Lifetime > 10 years
Text Notes 5800 2850 0    50   ~ 0
X7R
Text Notes 8300 2850 0    50   ~ 0
X7R
Text Notes 8700 3250 0    50   ~ 0
Irms > 0.15A\nESR < 0.5 Ohm\n2000 Hours\n680uF < C < 2000uF\nVcap > 20V\nShort as possible
Text Notes 3250 4300 0    50   ~ 0
2.1x5.5mm
$Comp
L LM2576-5.0U IC12
U 1 1 57750235
P 6550 2150
F 0 "IC12" H 6250 2340 50  0000 L CNN
F 1 "LM2576-5.0U" H 6250 1850 50  0000 L CNN
F 2 "Packages_TO:TO-263-5" H 6240 2310 60  0001 C CNN
F 3 "" H 6550 2150 60  0000 C CNN
	1    6550 2150
	1    0    0    -1  
$EndComp
$Comp
L LM1117M-3.3 IC13
U 1 1 5775032F
P 7250 4450
F 0 "IC13" H 6950 4650 50  0000 L CNN
F 1 "LM1117M-3.3" H 6950 4250 50  0000 L CNN
F 2 "Packages_SOT:SOT-223" H 7000 4610 60  0001 C CNN
F 3 "" H 7250 4450 60  0000 C CNN
	1    7250 4450
	1    0    0    -1  
$EndComp
Text Notes 4350 3500 2    50   ~ 0
Safety Circuit
Text Notes 7100 2900 0    50   ~ 0
Short as possible
$Comp
L +5P #PWR09
U 1 1 5776AF16
P 8650 2000
F 0 "#PWR09" H 8650 1950 50  0001 C CNN
F 1 "+5P" H 8650 2150 50  0000 C CNN
F 2 "" H 8650 2000 50  0000 C CNN
F 3 "" H 8650 2000 50  0000 C CNN
	1    8650 2000
	1    0    0    -1  
$EndComp
$Comp
L 330/0.100W R1
U 1 1 5776D7E6
P 4750 4300
F 0 "R1" H 4750 4400 50  0000 C CNN
F 1 "330/0.100W" H 4750 4200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 4750 4230 30  0001 C CNN
F 3 "" V 4750 4300 30  0000 C CNN
	1    4750 4300
	0    1    1    0   
$EndComp
$Comp
L NP-100nF C3
U 1 1 5776DB9E
P 5750 2600
F 0 "C3" H 5790 2700 50  0000 L CNN
F 1 "100nF" H 5790 2490 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 5788 2450 30  0001 C CNN
F 3 "" H 5750 2600 60  0000 C CNN
	1    5750 2600
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C6
U 1 1 5776E13D
P 8250 2600
F 0 "C6" H 8290 2700 50  0000 L CNN
F 1 "100nF" H 8290 2490 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 8288 2450 30  0001 C CNN
F 3 "" H 8250 2600 60  0000 C CNN
	1    8250 2600
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C5
U 1 1 5776E1A3
P 8050 4700
F 0 "C5" H 8090 4800 50  0000 L CNN
F 1 "100nF" H 8090 4590 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 8088 4550 30  0001 C CNN
F 3 "" H 8050 4700 60  0000 C CNN
	1    8050 4700
	1    0    0    -1  
$EndComp
$Comp
L NP-100nF C4
U 1 1 5776E220
P 6450 4700
F 0 "C4" H 6490 4800 50  0000 L CNN
F 1 "100nF" H 6490 4590 50  0000 L CNN
F 2 "Capacitors:C_Ceramic_SMD_0603" H 6488 4550 30  0001 C CNN
F 3 "" H 6450 4700 60  0000 C CNN
	1    6450 4700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG010
U 1 1 5776E6E9
P 2450 2050
F 0 "#FLG010" H 2450 2000 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 2250 50  0000 C CNN
F 2 "" H 2450 2050 50  0000 C CNN
F 3 "" H 2450 2050 50  0000 C CNN
	1    2450 2050
	1    0    0    -1  
$EndComp
$Comp
L +3.3P #PWR011
U 1 1 57776168
P 3450 3000
F 0 "#PWR011" H 3450 2950 50  0001 C CNN
F 1 "+3.3P" H 3450 3150 50  0000 C CNN
F 2 "" H 3450 3000 50  0000 C CNN
F 3 "" H 3450 3000 50  0000 C CNN
	1    3450 3000
	1    0    0    -1  
$EndComp
$Comp
L +3.3P #PWR012
U 1 1 579AC7EB
P 8450 3950
F 0 "#PWR012" H 8450 3900 50  0001 C CNN
F 1 "+3.3P" H 8450 4100 50  0000 C CNN
F 2 "" H 8450 3950 50  0000 C CNN
F 3 "" H 8450 3950 50  0000 C CNN
	1    8450 3950
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG013
U 1 1 579AD6B6
P 2450 2550
F 0 "#FLG013" H 2450 2500 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 2750 50  0000 C CNN
F 2 "" H 2450 2550 50  0000 C CNN
F 3 "" H 2450 2550 50  0000 C CNN
	1    2450 2550
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG014
U 1 1 57754BF3
P 2450 1550
F 0 "#FLG014" H 2450 1500 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 1750 50  0000 C CNN
F 2 "" H 2450 1550 50  0000 C CNN
F 3 "" H 2450 1550 50  0000 C CNN
	1    2450 1550
	1    0    0    -1  
$EndComp
Text HLabel 2550 1700 2    50   Output ~ 0
+12V
$Comp
L MBRS360T3 D2
U 1 1 579B6DE7
P 7050 2600
F 0 "D2" H 7050 2690 50  0000 C CNN
F 1 "MBRS360T3" H 7050 2500 50  0000 C CNN
F 2 "Packages_DO:DO-214AB" H 6950 2590 60  0001 C CNN
F 3 "" H 7050 2600 60  0000 C CNN
F 4 "ON Semiconductor" H 7050 2600 60  0001 C CNN "MFG Name"
F 5 "MBRS360T3G" H 7050 2600 60  0001 C CNN "MFG Part Number"
F 6 "MBRS360T3GOSCT-ND" H 7050 2600 60  0001 C CNN "1st Distributor Part Number"
F 7 "http://www.digikey.com/product-detail/en/on-semiconductor/MBRS360T3G/MBRS360T3GOSCT-ND/918010" H 7050 2600 60  0001 C CNN "1st Distributor Link"
	1    7050 2600
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG015
U 1 1 579C0964
P 2450 1050
F 0 "#FLG015" H 2450 1000 50  0001 C CNN
F 1 "PWR_FLAG" H 2450 1250 50  0000 C CNN
F 2 "" H 2450 1050 50  0000 C CNN
F 3 "" H 2450 1050 50  0000 C CNN
	1    2450 1050
	1    0    0    -1  
$EndComp
Text HLabel 2550 1200 2    50   Output ~ 0
+12P
$Comp
L MF-MSMF050 PTC1
U 1 1 579C377B
P 3750 2000
F 0 "PTC1" H 3750 2100 50  0000 C CNN
F 1 "MF-MSMF050" H 3750 1850 50  0000 C CNN
F 2 "PTCs:PTC_MF-MSMF-1-07" H 3750 1930 30  0001 C CNN
F 3 "" V 3750 2000 30  0000 C CNN
	1    3750 2000
	1    0    0    -1  
$EndComp
$Comp
L MF-MSMF030 PTC2
U 1 1 579C3AA0
P 3750 2550
F 0 "PTC2" H 3750 2650 50  0000 C CNN
F 1 "MF-MSMF030" H 3750 2400 50  0000 C CNN
F 2 "PTCs:PTC_MF-MSMF-1-07" H 3750 2480 30  0001 C CNN
F 3 "" V 3750 2550 30  0000 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
$Comp
L MF-MSMF030 PTC3
U 1 1 579C3AF8
P 3750 3100
F 0 "PTC3" H 3750 3200 50  0000 C CNN
F 1 "MF-MSMF030" H 3750 2950 50  0000 C CNN
F 2 "PTCs:PTC_MF-MSMF-1-07" H 3750 3030 30  0001 C CNN
F 3 "" V 3750 3100 30  0000 C CNN
	1    3750 3100
	1    0    0    -1  
$EndComp
$Comp
L 100uH/1.7A L1
U 1 1 579B50FB
P 7650 2300
F 0 "L1" H 7650 2400 50  0000 C CNN
F 1 "100uH/1.7A" H 7650 2250 50  0000 C CNN
F 2 "Inductors:L_SMD_SRN1060" H 7550 2300 60  0001 C CNN
F 3 "" V 7650 2300 60  0000 C CNN
	1    7650 2300
	1    0    0    -1  
$EndComp
Text HLabel 3450 2000 0    50   Output ~ 0
+12P
Text HLabel 4050 2000 2    50   Output ~ 0
+12V
$Comp
L +5P #PWR016
U 1 1 5776ADDE
P 3450 2450
F 0 "#PWR016" H 3450 2400 50  0001 C CNN
F 1 "+5P" H 3450 2600 50  0000 C CNN
F 2 "" H 3450 2450 50  0000 C CNN
F 3 "" H 3450 2450 50  0000 C CNN
	1    3450 2450
	1    0    0    -1  
$EndComp
Text HLabel 4050 2550 2    50   Output ~ 0
+5V
Text HLabel 4050 3100 2    50   Output ~ 0
+3.3V
Text HLabel 4850 2100 0    50   Output ~ 0
+12V
Text HLabel 4000 3950 2    50   Output ~ 0
+12P
Text HLabel 4100 4150 2    50   Output ~ 0
GND
Text HLabel 4850 4000 2    50   Output ~ 0
+3.3V
Text HLabel 4850 5050 2    50   Output ~ 0
GND
Text HLabel 5550 4000 0    50   Output ~ 0
+5V
Text HLabel 5550 5100 0    50   Output ~ 0
GND
Text HLabel 4850 3100 0    50   Output ~ 0
GND
$Comp
L LED LD_power1
U 1 1 57AC4B7A
P 4750 4750
F 0 "LD_power1" H 4750 4850 50  0000 C CNN
F 1 "LED" H 4750 4650 50  0000 C CNN
F 2 "LEDs:LED_Chip_SMD_0805" H 4650 4740 60  0001 C CNN
F 3 "" H 4750 4750 60  0000 C CNN
	1    4750 4750
	0    1    1    0   
$EndComp
Text Notes 5700 4950 0    50   ~ 0
Tantalum Type\n
Text Notes 7450 2450 0    50   ~ 0
I > 0.58 A
Wire Wire Line
	6450 4400 6450 4500
Wire Wire Line
	6750 4500 6800 4500
Wire Wire Line
	8050 4400 8050 4500
Connection ~ 8250 2300
Connection ~ 7050 2300
Wire Wire Line
	7050 2300 7050 2400
Wire Wire Line
	7000 2300 7450 2300
Wire Wire Line
	8650 2300 7850 2300
Wire Wire Line
	8250 2300 8250 2400
Wire Wire Line
	4850 3100 8650 3100
Connection ~ 5750 3100
Wire Wire Line
	4850 2100 6100 2100
Connection ~ 4000 4150
Wire Wire Line
	3900 4050 4000 4050
Wire Wire Line
	3900 4150 4100 4150
Wire Wire Line
	6450 5100 6450 4900
Wire Wire Line
	8050 5100 8050 4900
Wire Wire Line
	6750 5100 6750 4500
Wire Wire Line
	3900 3950 4000 3950
Connection ~ 8050 4400
Connection ~ 8250 3100
Wire Wire Line
	2450 2900 2450 3050
Wire Notes Line
	3150 3650 4350 3650
Wire Notes Line
	3150 5250 4350 5250
Wire Notes Line
	3150 5250 3150 3650
Wire Wire Line
	4750 4500 4750 4550
Wire Wire Line
	4750 4000 4750 4100
Wire Wire Line
	4750 4950 4750 5050
Wire Notes Line
	4450 3800 5200 3800
Wire Notes Line
	4450 5250 5200 5250
Connection ~ 7050 3100
Connection ~ 5750 2100
Wire Notes Line
	4550 1750 9600 1750
Connection ~ 6450 4400
Connection ~ 8050 5100
Connection ~ 6750 5100
Wire Wire Line
	5650 4400 6800 4400
Wire Wire Line
	8650 2000 8650 2400
Wire Wire Line
	7000 2100 8650 2100
Connection ~ 8650 2300
Connection ~ 8650 2100
Wire Wire Line
	8450 3950 8450 4500
Connection ~ 8450 4400
Connection ~ 6450 5100
Wire Notes Line
	5300 3650 9600 3650
Wire Notes Line
	5300 5250 9600 5250
Wire Notes Line
	4450 3800 4450 5250
Wire Notes Line
	2950 3400 2050 3400
Wire Wire Line
	6100 2200 6050 2200
Wire Wire Line
	6050 2200 6050 3100
Connection ~ 6050 3100
Wire Wire Line
	6050 2300 6100 2300
Connection ~ 6050 2300
Connection ~ 5650 4400
Wire Wire Line
	5650 4900 5650 5100
Connection ~ 4950 2100
Wire Wire Line
	5750 2100 5750 2400
Wire Wire Line
	7700 4500 7750 4500
Wire Wire Line
	7750 4500 7750 4400
Wire Wire Line
	7700 4400 8450 4400
Connection ~ 7750 4400
Wire Wire Line
	5550 4000 7050 4000
Connection ~ 5650 4000
Wire Wire Line
	7450 4000 8450 4000
Connection ~ 8450 4000
Wire Wire Line
	3450 2000 3550 2000
Wire Wire Line
	3950 2000 4050 2000
Wire Notes Line
	3150 3400 4350 3400
Wire Wire Line
	3450 2550 3550 2550
Wire Wire Line
	3950 2550 4050 2550
Wire Wire Line
	3450 3100 3550 3100
Wire Wire Line
	3950 3100 4050 3100
Wire Notes Line
	3150 1650 4350 1650
Wire Wire Line
	2450 2050 2450 2200
Wire Wire Line
	2450 2700 2450 2550
Wire Wire Line
	2450 1700 2450 1550
Wire Notes Line
	2950 750  2950 3400
Wire Wire Line
	2450 1200 2450 1050
Wire Wire Line
	2550 2900 2450 2900
Wire Wire Line
	2450 2700 2550 2700
Wire Wire Line
	2450 2200 2550 2200
Wire Wire Line
	2450 1700 2550 1700
Wire Wire Line
	2450 1200 2550 1200
Wire Notes Line
	2050 750  2950 750 
Wire Notes Line
	2050 3400 2050 750 
Wire Notes Line
	4350 1650 4350 3400
Wire Notes Line
	3150 3400 3150 1650
Wire Wire Line
	4950 2100 4950 2400
Wire Notes Line
	4550 1750 4550 3400
Wire Wire Line
	3450 2450 3450 2550
Wire Wire Line
	3450 3000 3450 3100
Wire Notes Line
	4350 5250 4350 3650
Wire Wire Line
	4000 4050 4000 4150
Wire Wire Line
	4850 4000 4750 4000
Wire Wire Line
	4750 5050 4850 5050
Wire Notes Line
	5200 5250 5200 3800
Wire Wire Line
	5650 4000 5650 4500
Wire Notes Line
	5300 3650 5300 5250
Connection ~ 5650 5100
Connection ~ 4950 3100
Wire Notes Line
	9600 5250 9600 3650
Wire Wire Line
	5550 5100 8450 5100
Wire Wire Line
	8450 5100 8450 4900
Wire Wire Line
	4950 2800 4950 3100
Wire Wire Line
	5750 3100 5750 2800
Wire Wire Line
	8650 3100 8650 2800
Wire Wire Line
	8250 2800 8250 3100
Wire Wire Line
	7050 2800 7050 3100
Wire Notes Line
	4550 3400 9600 3400
Wire Notes Line
	9600 3400 9600 1750
$Comp
L P-22uF/20V C7
U 1 1 57B5EF27
P 8450 4700
F 0 "C7" H 8490 4805 50  0000 L CNN
F 1 "22uF/20V" H 8490 4555 50  0000 L CNN
F 2 "Capacitors:C_Tantalum_SMD_Vishay_Case_B" H 8450 4685 60  0001 C CNN
F 3 "" H 8450 4685 60  0000 C CNN
	1    8450 4700
	1    0    0    -1  
$EndComp
$Comp
L P-1000uF/25V C8
U 1 1 57B6A23A
P 8650 2600
F 0 "C8" H 8690 2705 50  0000 L CNN
F 1 "1000uF/25V" H 8690 2455 50  0000 L CNN
F 2 "Capacitors:C_Elec_SMD_D-12.5mm_L-14.0mm" H 8650 2585 60  0001 C CNN
F 3 "" H 8650 2585 60  0000 C CNN
	1    8650 2600
	1    0    0    -1  
$EndComp
$Comp
L USB-A USB2
U 1 1 59BA51DD
P 4000 4750
F 0 "USB2" H 3800 5050 50  0000 L CNN
F 1 "USB-A" H 3800 4450 50  0000 L CNN
F 2 "Connectors:USB_A" H 3850 4800 10  0001 C CNN
F 3 "" H 3850 4800 60  0000 C CNN
	1    4000 4750
	1    0    0    -1  
$EndComp
$Comp
L +5P #PWR017
U 1 1 59BA5EBA
P 3450 4500
F 0 "#PWR017" H 3450 4450 50  0001 C CNN
F 1 "+5P" H 3450 4650 50  0000 C CNN
F 2 "" H 3450 4500 50  0000 C CNN
F 3 "" H 3450 4500 50  0000 C CNN
	1    3450 4500
	1    0    0    -1  
$EndComp
Text HLabel 3400 4850 0    50   Output ~ 0
GND
Wire Wire Line
	3650 4550 3450 4550
Wire Wire Line
	3450 4550 3450 4500
Wire Wire Line
	3650 4850 3400 4850
Wire Wire Line
	3650 4950 3550 4950
Wire Wire Line
	3550 4950 3550 4850
Connection ~ 3550 4850
NoConn ~ 3650 4650
NoConn ~ 3650 4750
$EndSCHEMATC
