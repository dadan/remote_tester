EESchema Schematic File Version 2
LIBS:Remote_tester-rescue
LIBS:BJT
LIBS:Capacitor
LIBS:Diode_Rectifier
LIBS:Diode_Schottky
LIBS:Diode_Switching
LIBS:Diode_Zener
LIBS:EEPROM
LIBS:Inductor
LIBS:Microcontrollers
LIBS:Miscellaneous_Connectors
LIBS:Miscellaneous_Devices
LIBS:Modules
LIBS:MOSFET
LIBS:MOV
LIBS:Others
LIBS:Power
LIBS:PTC
LIBS:Regulator
LIBS:Resistor
LIBS:TBU
LIBS:TVS
LIBS:onsemi
LIBS:battery_management
LIBS:device
LIBS:opamp
LIBS:Remote_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1500 1200 2100 4500
U 599C14AC
F0 "STM32_System_Minimum" 60
F1 "STM32_System_Minimum.sch" 60
F2 "+3.3V" I L 1500 1300 60 
F3 "GND" I R 3600 1300 60 
F4 "PB5" O L 1500 4150 60 
F5 "PB13" I L 1500 5100 60 
F6 "SPI1_MOSI" I L 1500 1600 60 
F7 "SPI1_SCK" O L 1500 1750 60 
F8 "PC8" O R 3600 3700 60 
F9 "PC9" O R 3600 3850 60 
F10 "VCC" I L 1500 1450 60 
F11 "PC12" O R 3600 4300 60 
F12 "SPI1_~SS" O L 1500 1900 60 
F13 "PA11" O L 1500 3150 60 
F14 "PA12" I L 1500 3300 60 
F15 "PA1" I L 1500 2250 60 
F16 "PA2" I L 1500 2400 60 
F17 "PA3" I L 1500 2550 60 
F18 "PA4" I L 1500 2700 60 
F19 "PA6" I L 1500 2900 60 
F20 "PA8" I L 1500 3050 60 
F21 "RX" I R 3600 1650 60 
F22 "TX" I R 3600 1800 60 
F23 "PA15" I L 1500 3450 60 
F24 "PC0" I R 3600 2550 60 
F25 "SCL" I R 3600 2050 60 
F26 "SDA" I R 3600 2200 60 
F27 "PC1" I R 3600 2700 60 
F28 "PC2" I R 3600 2900 60 
F29 "PC3" I R 3600 3100 60 
F30 "PC5" I R 3600 3250 60 
F31 "PC6" I R 3600 3400 60 
F32 "PC7" I R 3600 3550 60 
F33 "PC13" I R 3600 4450 60 
F34 "PC14" I R 3600 4600 60 
F35 "PC15" I R 3600 4750 60 
F36 "PB0" I L 1500 3700 60 
F37 "PB1" I L 1500 3850 60 
F38 "PB4" I L 1500 4000 60 
F39 "PB8" I L 1500 4350 60 
F40 "PB9" I L 1500 4500 60 
F41 "PB12" I L 1500 4950 60 
F42 "PB14" I L 1500 5250 60 
F43 "PB15" I L 1500 5400 60 
F44 "PD2" I R 3600 5000 60 
F45 "SCL2" I R 3600 5250 50 
F46 "SDA2" I R 3600 5400 50 
$EndSheet
$Sheet
S 4750 1200 1100 850 
U 599F2976
F0 "Mux_LED" 60
F1 "Mux_LED.sch" 60
F2 "VCC" O L 4750 1350 50 
F3 "GND" O R 5850 1350 50 
F4 "PC9" I L 4750 1550 50 
F5 "PC8" I L 4750 1650 50 
F6 "PC7" I L 4750 1750 50 
F7 "PC6" I R 5850 1500 50 
F8 "PB15" I R 5850 1600 50 
F9 "PB14" I R 5850 1700 50 
F10 "PB13" I R 5850 1800 50 
F11 "PB12" I R 5850 1900 50 
$EndSheet
$Sheet
S 5250 4300 1300 1350
U 59A0F043
F0 "Mux_Keypad" 50
F1 "Mux_Keypad.sch" 50
F2 "VCC" O L 5250 4400 50 
F3 "GND" I R 6550 4400 50 
F4 "PC13" I L 5250 5000 50 
F5 "PC14" I L 5250 5100 50 
F6 "PC15" I L 5250 5200 50 
F7 "PC0" I L 5250 4600 50 
F8 "PC1" I L 5250 4700 50 
F9 "PC2" I L 5250 4800 50 
F10 "PC3" I L 5250 4900 50 
F11 "SDA2" I R 6550 4600 50 
F12 "SCL2" I R 6550 4800 50 
F13 "PB4" I R 6550 5000 50 
F14 "PB5" I R 6550 5100 50 
F15 "PD2" I R 6550 5350 50 
F16 "+3.3V" I L 5250 5400 50 
$EndSheet
$Sheet
S 4850 2650 1050 850 
U 59A83085
F0 "Power" 50
F1 "Power.sch" 50
F2 "GND" O R 5900 2800 50 
F3 "+5V" O L 4850 2800 50 
F4 "+3.3V" O L 4850 2950 50 
F5 "+12V" O L 4850 3150 60 
F6 "+12P" O L 4850 3300 60 
$EndSheet
Wire Wire Line
	1500 1300 1200 1300
Wire Wire Line
	1500 1450 1200 1450
Wire Wire Line
	1500 1600 1200 1600
Wire Wire Line
	1500 1750 1200 1750
Wire Wire Line
	1500 1900 1200 1900
Wire Wire Line
	1500 2250 1200 2250
Wire Wire Line
	1500 2400 1200 2400
Wire Wire Line
	1500 2550 1200 2550
Wire Wire Line
	1500 2700 1200 2700
Wire Wire Line
	1500 2900 1200 2900
Wire Wire Line
	1500 3050 1200 3050
Wire Wire Line
	1500 3150 1200 3150
Wire Wire Line
	1500 3300 1200 3300
Wire Wire Line
	1500 3450 1200 3450
Wire Wire Line
	1500 3700 1200 3700
Wire Wire Line
	1500 3850 1200 3850
Wire Wire Line
	1500 4000 1200 4000
Wire Wire Line
	1500 4150 1200 4150
Wire Wire Line
	1500 4350 1200 4350
Wire Wire Line
	1500 4500 1200 4500
Wire Wire Line
	1500 4950 1200 4950
Wire Wire Line
	1500 5100 1200 5100
Wire Wire Line
	1500 5250 1200 5250
Wire Wire Line
	1500 5400 1200 5400
Wire Wire Line
	3600 1300 3950 1300
Wire Wire Line
	3600 1650 3950 1650
Wire Wire Line
	3600 1800 3950 1800
Wire Wire Line
	3600 2050 3950 2050
Wire Wire Line
	3600 2200 3950 2200
Wire Wire Line
	3600 2550 3950 2550
Wire Wire Line
	3600 2700 3950 2700
Wire Wire Line
	3600 2900 3950 2900
Wire Wire Line
	3600 3100 3950 3100
Wire Wire Line
	3600 3250 3950 3250
Wire Wire Line
	3600 3400 3950 3400
Wire Wire Line
	3600 3550 3950 3550
Wire Wire Line
	3600 3700 3950 3700
Wire Wire Line
	3600 3850 3950 3850
Wire Wire Line
	3600 4300 3950 4300
Wire Wire Line
	3600 4450 3950 4450
Wire Wire Line
	3600 4600 3950 4600
Wire Wire Line
	3600 4750 3950 4750
Wire Wire Line
	3600 5000 3950 5000
Wire Wire Line
	3600 5250 3950 5250
Wire Wire Line
	3600 5400 3950 5400
Wire Wire Line
	4500 1350 4750 1350
Wire Wire Line
	4500 1550 4750 1550
Wire Wire Line
	4500 1650 4750 1650
Wire Wire Line
	4500 1750 4750 1750
Wire Wire Line
	5850 1350 6100 1350
Wire Wire Line
	5850 1500 6100 1500
Wire Wire Line
	5850 1600 6100 1600
Wire Wire Line
	5850 1700 6100 1700
Wire Wire Line
	5850 1800 6100 1800
Wire Wire Line
	5850 1900 6100 1900
Wire Wire Line
	5250 4400 5050 4400
Wire Wire Line
	5250 4600 5050 4600
Wire Wire Line
	5250 4700 5050 4700
Wire Wire Line
	5250 4800 5050 4800
Wire Wire Line
	5250 4900 5050 4900
Wire Wire Line
	5250 5000 5050 5000
Wire Wire Line
	5250 5100 5050 5100
Wire Wire Line
	5250 5200 5050 5200
Wire Wire Line
	6550 4400 6800 4400
Wire Wire Line
	6550 4600 6800 4600
Wire Wire Line
	6550 5000 6800 5000
Wire Wire Line
	6550 5100 6800 5100
Wire Wire Line
	6550 5350 6800 5350
Wire Wire Line
	7100 1450 6850 1450
Wire Wire Line
	7100 1550 6850 1550
Wire Wire Line
	7100 1700 6850 1700
Wire Wire Line
	7100 2150 6850 2150
Wire Wire Line
	7100 2250 6850 2250
Wire Wire Line
	7100 2350 6850 2350
Wire Wire Line
	8300 1950 8550 1950
Wire Wire Line
	8300 2050 8550 2050
Wire Wire Line
	8300 2150 8550 2150
Wire Wire Line
	8300 2800 8550 2800
Wire Wire Line
	8300 2900 8550 2900
Wire Wire Line
	4850 2800 4600 2800
Wire Wire Line
	4850 2950 4600 2950
Wire Wire Line
	5900 2800 6200 2800
Text Label 1200 1300 0    60   ~ 0
+3.3V
Text Label 1200 1450 0    60   ~ 0
VCC
Text Label 1200 1600 0    60   ~ 0
MOSI
Text Label 1200 1750 0    60   ~ 0
SCK
Text Label 1200 1900 0    60   ~ 0
SS
Text Label 1200 2250 2    60   ~ 0
VBAT_CONV
Text Label 1200 2400 2    60   ~ 0
VBAT_ADC
Text Label 1200 2550 0    60   ~ 0
PA3
Text Label 1200 2700 0    60   ~ 0
PA4
Text Label 1200 2900 0    60   ~ 0
PA6
Text Label 1200 3050 0    60   ~ 0
PA8
Text Label 1200 3150 0    60   ~ 0
PA11
Text Label 1200 3300 0    60   ~ 0
PA12
Text Label 1200 3450 0    60   ~ 0
PA15
Text Label 1200 3700 0    60   ~ 0
PB0
Text Label 1200 3850 0    60   ~ 0
PB1
Text Label 1200 4000 0    60   ~ 0
PB4
Text Label 1200 4150 0    60   ~ 0
PB5
Text Label 1200 4350 0    60   ~ 0
PB8
Text Label 1200 4500 0    60   ~ 0
PB9
Text Label 1200 4950 0    60   ~ 0
PB12
Text Label 1200 5100 0    60   ~ 0
PB13
Text Label 1200 5250 0    60   ~ 0
PB14
Text Label 1200 5400 0    60   ~ 0
PB15
Text Label 3950 1300 2    60   ~ 0
GND
Text Label 3950 1650 2    60   ~ 0
RX
Text Label 3950 1800 2    60   ~ 0
TX
Text Label 3950 2050 2    60   ~ 0
SCL
Text Label 3950 2200 2    60   ~ 0
SDA
Text Label 3950 2550 2    60   ~ 0
PC0
Text Label 3950 2700 2    60   ~ 0
PC1
Text Label 3950 2900 2    60   ~ 0
PC2
Text Label 3950 3100 2    60   ~ 0
PC3
Text Label 3950 3250 2    60   ~ 0
PC5
Text Label 3950 3400 2    60   ~ 0
PC6
Text Label 3950 3550 2    60   ~ 0
PC7
Text Label 3950 3700 2    60   ~ 0
PC8
Text Label 3950 3850 2    60   ~ 0
PC9
Text Label 3950 4300 2    60   ~ 0
PC12
Text Label 3950 4450 2    60   ~ 0
PC13
Text Label 3950 4600 2    60   ~ 0
PC14
Text Label 3950 4750 2    60   ~ 0
PC15
Text Label 3950 5000 2    60   ~ 0
PD2
Text Label 3950 5250 2    60   ~ 0
SCL2
Text Label 3950 5400 2    60   ~ 0
SDA2
Text Label 4500 1350 0    60   ~ 0
VCC
Text Label 4500 1550 0    60   ~ 0
PC9
Text Label 4500 1650 0    60   ~ 0
PC8
Text Label 4500 1750 0    60   ~ 0
PC7
Text Label 6100 1350 2    60   ~ 0
GND
Text Label 6100 1500 2    60   ~ 0
PC6
Text Label 6100 1600 2    60   ~ 0
PB15
Text Label 6100 1700 2    60   ~ 0
PB14
Text Label 6100 1800 2    60   ~ 0
PB13
Text Label 6100 1900 2    60   ~ 0
PB12
Text Label 6800 4400 2    60   ~ 0
GND
Text Label 6800 5000 2    60   ~ 0
PB4
Text Label 6800 5100 2    60   ~ 0
PB5
Text Label 6800 5350 2    60   ~ 0
PD2
Text Label 8550 1950 2    60   ~ 0
PA8
Text Label 8550 2050 2    60   ~ 0
PA11
Text Label 8550 2150 2    60   ~ 0
PA12
Text Label 8550 2800 2    60   ~ 0
PB8
Text Label 8550 2900 2    60   ~ 0
PB9
Text Label 6850 1450 0    60   ~ 0
SDA
Text Label 6850 1550 0    60   ~ 0
SCL
Text Label 6850 1700 0    60   ~ 0
PA15
Text Label 6850 2150 0    60   ~ 0
SCK
Text Label 6850 2250 0    60   ~ 0
MOSI
Text Label 6850 2350 0    60   ~ 0
SS
Text Label 4600 2800 0    60   ~ 0
VCC
Text Label 4600 2950 0    60   ~ 0
+3.3V
Text Label 6200 2800 2    60   ~ 0
GND
Text Label 5050 4400 0    60   ~ 0
VCC
Text Label 5050 4600 0    60   ~ 0
PC0
Text Label 5050 4700 0    60   ~ 0
PC1
Text Label 5050 4800 0    60   ~ 0
PC2
Text Label 5050 4900 0    60   ~ 0
PC3
Text Label 5050 5000 0    60   ~ 0
PC13
Text Label 5050 5100 0    60   ~ 0
PC14
Text Label 5050 5200 0    60   ~ 0
PC15
Wire Wire Line
	8300 1400 8550 1400
Wire Wire Line
	7100 1350 6850 1350
Wire Wire Line
	8300 2250 8550 2250
Text Label 6850 1350 0    50   ~ 0
VCC
Text Label 8550 2250 2    50   ~ 0
PA4
Text Label 8550 1400 2    50   ~ 0
GND
Wire Wire Line
	4850 3150 4600 3150
Wire Wire Line
	4850 3300 4600 3300
NoConn ~ 4600 3150
NoConn ~ 4600 3300
$Comp
L Jumper JP1
U 1 1 59A850FA
P 7100 4600
F 0 "JP1" H 7100 4750 50  0000 C CNN
F 1 "Jumper" H 7350 4750 50  0000 C CNN
F 2 "Jumpers:Jumper_2-way_SMD" H 7100 4600 50  0001 C CNN
F 3 "" H 7100 4600 50  0001 C CNN
	1    7100 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 4800 6800 4800
$Comp
L Jumper JP2
U 1 1 59A867DB
P 7100 4800
F 0 "JP2" H 7100 4950 50  0000 C CNN
F 1 "Jumper" H 7350 4950 50  0000 C CNN
F 2 "Jumpers:Jumper_2-way_SMD" H 7100 4800 50  0001 C CNN
F 3 "" H 7100 4800 50  0001 C CNN
	1    7100 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4600 7700 4600
Wire Wire Line
	7400 4800 7700 4800
Text Label 7700 4600 2    50   ~ 0
SDA
Text Label 7700 4800 2    50   ~ 0
SCL
Wire Wire Line
	5250 5400 5050 5400
Text Label 5050 5400 0    50   ~ 0
+3.3V
Wire Wire Line
	7100 3000 6900 3000
Text Label 6900 3000 0    50   ~ 0
+3.3V
$Sheet
S 7100 1300 1200 2050
U 59A71036
F0 "Test_LCD_BLE_Vbat_Programmer_LED_EEPROM" 50
F1 "Test_LCD_BLE_Vbat_Programmer_LED_EEPROM.sch" 50
F2 "SCK" I L 7100 2150 50 
F3 "MOSI" I L 7100 2250 50 
F4 "SS" I L 7100 2350 50 
F5 "PA12" I R 8300 2150 50 
F6 "PA11" I R 8300 2050 50 
F7 "PA8" I R 8300 1950 50 
F8 "PB9" I R 8300 2900 50 
F9 "PB8" I R 8300 2800 50 
F10 "SDA" I L 7100 1450 50 
F11 "SCL" I L 7100 1550 50 
F12 "PA4" I R 8300 2250 50 
F13 "GND" I R 8300 1400 50 
F14 "VCC" O L 7100 1350 50 
F15 "+3.3V" I L 7100 3000 50 
F16 "PC13" I L 7100 1800 50 
F17 "PC14" I L 7100 1900 50 
F18 "PC15" I L 7100 2000 50 
F19 "VBAT_CONV" I L 7100 2650 50 
F20 "VBT_ADC" I R 8300 3100 50 
F21 "RX" I R 8300 1600 50 
F22 "TX" I R 8300 1700 50 
F23 "PWM_LCD" I L 7100 1700 60 
$EndSheet
Wire Wire Line
	7100 1800 6850 1800
Wire Wire Line
	7100 1900 6850 1900
Wire Wire Line
	7100 2000 6850 2000
Wire Wire Line
	7100 2650 6850 2650
Text Label 6850 1800 0    50   ~ 0
PC13
Text Label 6850 1900 0    50   ~ 0
PC14
Text Label 6850 2000 0    50   ~ 0
PC15
Text Label 6850 2650 2    50   ~ 0
VBAT_CONV
Wire Wire Line
	8300 3100 8600 3100
Text Label 8600 3100 0    50   ~ 0
VBAT_ADC
Wire Wire Line
	8300 1600 8550 1600
Wire Wire Line
	8300 1700 8550 1700
Text Label 8550 1600 2    50   ~ 0
TX
Text Label 8550 1700 2    50   ~ 0
RX
$EndSCHEMATC
