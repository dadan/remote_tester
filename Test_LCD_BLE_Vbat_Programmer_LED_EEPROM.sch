EESchema Schematic File Version 2
LIBS:Remote_tester-rescue
LIBS:BJT
LIBS:Capacitor
LIBS:Diode_Rectifier
LIBS:Diode_Schottky
LIBS:Diode_Switching
LIBS:Diode_Zener
LIBS:EEPROM
LIBS:Inductor
LIBS:Microcontrollers
LIBS:Miscellaneous_Connectors
LIBS:Miscellaneous_Devices
LIBS:Modules
LIBS:MOSFET
LIBS:MOV
LIBS:Others
LIBS:Power
LIBS:PTC
LIBS:Regulator
LIBS:Resistor
LIBS:TBU
LIBS:TVS
LIBS:onsemi
LIBS:battery_management
LIBS:device
LIBS:opamp
LIBS:Remote_tester-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TEST-RESCUE-Remote_tester TP44
U 1 1 59A712E8
P 1600 1000
AR Path="/59A712E8" Ref="TP44"  Part="1" 
AR Path="/59A71036/59A712E8" Ref="TP44"  Part="1" 
F 0 "TP44" H 1600 1100 50  0000 C CNN
F 1 "TEST" H 1600 900 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 1600 900 10  0001 C CNN
F 3 "" H 1800 1000 60  0000 C CNN
	1    1600 1000
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP45
U 1 1 59A71326
P 1600 1350
AR Path="/59A71326" Ref="TP45"  Part="1" 
AR Path="/59A71036/59A71326" Ref="TP45"  Part="1" 
F 0 "TP45" H 1600 1450 50  0000 C CNN
F 1 "TEST" H 1600 1250 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 1600 1250 10  0001 C CNN
F 3 "" H 1800 1350 60  0000 C CNN
	1    1600 1350
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP46
U 1 1 59A71355
P 1600 1650
AR Path="/59A71355" Ref="TP46"  Part="1" 
AR Path="/59A71036/59A71355" Ref="TP46"  Part="1" 
F 0 "TP46" H 1600 1750 50  0000 C CNN
F 1 "TEST" H 1600 1550 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 1600 1550 10  0001 C CNN
F 3 "" H 1800 1650 60  0000 C CNN
	1    1600 1650
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP47
U 1 1 59A71384
P 1600 2000
AR Path="/59A71384" Ref="TP47"  Part="1" 
AR Path="/59A71036/59A71384" Ref="TP47"  Part="1" 
F 0 "TP47" H 1600 2100 50  0000 C CNN
F 1 "TEST" H 1600 1900 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 1600 1900 10  0001 C CNN
F 3 "" H 1800 2000 60  0000 C CNN
	1    1600 2000
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP48
U 1 1 59A713A9
P 1600 2350
AR Path="/59A713A9" Ref="TP48"  Part="1" 
AR Path="/59A71036/59A713A9" Ref="TP48"  Part="1" 
F 0 "TP48" H 1600 2450 50  0000 C CNN
F 1 "TEST" H 1600 2250 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 1600 2250 10  0001 C CNN
F 3 "" H 1800 2350 60  0000 C CNN
	1    1600 2350
	1    0    0    -1  
$EndComp
Text HLabel 1100 1350 0    50   Input ~ 0
PWM_LCD
Text HLabel 1100 1650 0    50   Input ~ 0
SCK
Text HLabel 1100 2000 0    50   Input ~ 0
MOSI
Text HLabel 1100 2350 0    50   Input ~ 0
SS
Text Notes 1000 2800 0    50   ~ 0
Test point LCD
$Comp
L TEST-RESCUE-Remote_tester TP39
U 1 1 59A7222E
P 3250 950
AR Path="/59A7222E" Ref="TP39"  Part="1" 
AR Path="/59A71036/59A7222E" Ref="TP39"  Part="1" 
F 0 "TP39" H 3250 1050 50  0000 C CNN
F 1 "TEST" H 3250 850 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 3250 850 10  0001 C CNN
F 3 "" H 3450 950 60  0000 C CNN
	1    3250 950 
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP40
U 1 1 59A722C7
P 3250 1300
AR Path="/59A722C7" Ref="TP40"  Part="1" 
AR Path="/59A71036/59A722C7" Ref="TP40"  Part="1" 
F 0 "TP40" H 3250 1400 50  0000 C CNN
F 1 "TEST" H 3250 1200 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 3250 1200 10  0001 C CNN
F 3 "" H 3450 1300 60  0000 C CNN
	1    3250 1300
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP41
U 1 1 59A722EC
P 3250 1600
AR Path="/59A722EC" Ref="TP41"  Part="1" 
AR Path="/59A71036/59A722EC" Ref="TP41"  Part="1" 
F 0 "TP41" H 3250 1700 50  0000 C CNN
F 1 "TEST" H 3250 1500 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 3250 1500 10  0001 C CNN
F 3 "" H 3450 1600 60  0000 C CNN
	1    3250 1600
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP42
U 1 1 59A7238B
P 3250 1950
AR Path="/59A7238B" Ref="TP42"  Part="1" 
AR Path="/59A71036/59A7238B" Ref="TP42"  Part="1" 
F 0 "TP42" H 3250 2050 50  0000 C CNN
F 1 "TEST" H 3250 1850 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 3250 1850 10  0001 C CNN
F 3 "" H 3450 1950 60  0000 C CNN
	1    3250 1950
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP43
U 1 1 59A723BA
P 3250 2300
AR Path="/59A723BA" Ref="TP43"  Part="1" 
AR Path="/59A71036/59A723BA" Ref="TP43"  Part="1" 
F 0 "TP43" H 3250 2400 50  0000 C CNN
F 1 "TEST" H 3250 2200 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 3250 2200 10  0001 C CNN
F 3 "" H 3450 2300 60  0000 C CNN
	1    3250 2300
	1    0    0    -1  
$EndComp
Text HLabel 2750 950  0    50   Input ~ 0
PA12
Text HLabel 2750 1300 0    50   Input ~ 0
PA11
Text HLabel 2750 1600 0    50   Input ~ 0
PA8
Text Notes 2700 2800 0    50   ~ 0
Test point BLE
$Comp
L TEST-RESCUE-Remote_tester TP5
U 1 1 59A77030
P 4850 950
AR Path="/59A77030" Ref="TP5"  Part="1" 
AR Path="/59A71036/59A77030" Ref="TP5"  Part="1" 
F 0 "TP5" H 4850 1050 50  0000 C CNN
F 1 "TEST" H 4850 850 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 4850 850 10  0001 C CNN
F 3 "" H 5050 950 60  0000 C CNN
	1    4850 950 
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP36
U 1 1 59A771FA
P 4850 1300
AR Path="/59A771FA" Ref="TP36"  Part="1" 
AR Path="/59A71036/59A771FA" Ref="TP36"  Part="1" 
F 0 "TP36" H 4850 1400 50  0000 C CNN
F 1 "TEST" H 4850 1200 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 4850 1200 10  0001 C CNN
F 3 "" H 5050 1300 60  0000 C CNN
	1    4850 1300
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP37
U 1 1 59A77227
P 4850 1650
AR Path="/59A77227" Ref="TP37"  Part="1" 
AR Path="/59A71036/59A77227" Ref="TP37"  Part="1" 
F 0 "TP37" H 4850 1750 50  0000 C CNN
F 1 "TEST" H 4850 1550 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 4850 1550 10  0001 C CNN
F 3 "" H 5050 1650 60  0000 C CNN
	1    4850 1650
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP38
U 1 1 59A77256
P 4850 2000
AR Path="/59A77256" Ref="TP38"  Part="1" 
AR Path="/59A71036/59A77256" Ref="TP38"  Part="1" 
F 0 "TP38" H 4850 2100 50  0000 C CNN
F 1 "TEST" H 4850 1900 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 4850 1900 10  0001 C CNN
F 3 "" H 5050 2000 60  0000 C CNN
	1    4850 2000
	1    0    0    -1  
$EndComp
Text Notes 4250 2450 0    50   ~ 0
Test point Vbat & USB\n
$Comp
L TEST-RESCUE-Remote_tester TP30
U 1 1 59A7848B
P 6900 2350
AR Path="/59A7848B" Ref="TP30"  Part="1" 
AR Path="/59A71036/59A7848B" Ref="TP30"  Part="1" 
F 0 "TP30" H 6900 2450 50  0000 C CNN
F 1 "TEST" H 6900 2250 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6900 2250 10  0001 C CNN
F 3 "" H 7100 2350 60  0000 C CNN
	1    6900 2350
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP31
U 1 1 59A78538
P 6900 2700
AR Path="/59A78538" Ref="TP31"  Part="1" 
AR Path="/59A71036/59A78538" Ref="TP31"  Part="1" 
F 0 "TP31" H 6900 2800 50  0000 C CNN
F 1 "TEST" H 6900 2600 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6900 2600 10  0001 C CNN
F 3 "" H 7100 2700 60  0000 C CNN
	1    6900 2700
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP28
U 1 1 59A78645
P 8250 950
AR Path="/59A78645" Ref="TP28"  Part="1" 
AR Path="/59A71036/59A78645" Ref="TP28"  Part="1" 
F 0 "TP28" H 8250 1050 50  0000 C CNN
F 1 "TEST" H 8250 850 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 8250 850 10  0001 C CNN
F 3 "" H 8450 950 60  0000 C CNN
	1    8250 950 
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP29
U 1 1 59A78684
P 9350 950
AR Path="/59A78684" Ref="TP29"  Part="1" 
AR Path="/59A71036/59A78684" Ref="TP29"  Part="1" 
F 0 "TP29" H 9350 1050 50  0000 C CNN
F 1 "TEST" H 9350 850 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 9350 850 10  0001 C CNN
F 3 "" H 9550 950 60  0000 C CNN
	1    9350 950 
	1    0    0    -1  
$EndComp
Text Notes 6600 3000 0    50   ~ 0
Test Led
Text Notes 7750 2100 0    50   ~ 0
Test EEPROM
Text HLabel 5950 2350 0    50   Input ~ 0
PB9
Text HLabel 5950 2700 0    50   Input ~ 0
PB8
Text HLabel 7800 950  0    50   Input ~ 0
SDA
Text HLabel 8900 950  0    50   Input ~ 0
SCL
$Comp
L TEST-RESCUE-Remote_tester TP1
U 1 1 59A043BC
P 6350 800
AR Path="/59A043BC" Ref="TP1"  Part="1" 
AR Path="/59A71036/59A043BC" Ref="TP1"  Part="1" 
F 0 "TP1" H 6350 900 50  0000 C CNN
F 1 "TEST" H 6350 700 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6350 700 10  0001 C CNN
F 3 "" H 6550 800 60  0000 C CNN
	1    6350 800 
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP2
U 1 1 59A04421
P 6350 1150
AR Path="/59A04421" Ref="TP2"  Part="1" 
AR Path="/59A71036/59A04421" Ref="TP2"  Part="1" 
F 0 "TP2" H 6350 1250 50  0000 C CNN
F 1 "TEST" H 6350 1050 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6350 1050 10  0001 C CNN
F 3 "" H 6550 1150 60  0000 C CNN
	1    6350 1150
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP3
U 1 1 59A04492
P 6350 1500
AR Path="/59A04492" Ref="TP3"  Part="1" 
AR Path="/59A71036/59A04492" Ref="TP3"  Part="1" 
F 0 "TP3" H 6350 1600 50  0000 C CNN
F 1 "TEST" H 6350 1400 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6350 1400 10  0001 C CNN
F 3 "" H 6550 1500 60  0000 C CNN
	1    6350 1500
	1    0    0    -1  
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP4
U 1 1 59A04505
P 6350 1900
AR Path="/59A04505" Ref="TP4"  Part="1" 
AR Path="/59A71036/59A04505" Ref="TP4"  Part="1" 
F 0 "TP4" H 6350 2000 50  0000 C CNN
F 1 "TEST" H 6350 1800 50  0000 C CNN
F 2 "Measurement_Points:test_hole" H 6350 1800 10  0001 C CNN
F 3 "" H 6550 1900 60  0000 C CNN
	1    6350 1900
	1    0    0    -1  
$EndComp
$Comp
L LM358 U1
U 1 1 59A063DD
P 2250 4000
F 0 "U1" H 2250 4200 50  0000 L CNN
F 1 "LM358" H 2250 3800 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 2250 4000 50  0001 C CNN
F 3 "" H 2250 4000 50  0001 C CNN
	1    2250 4000
	1    0    0    -1  
$EndComp
Text HLabel 5950 1900 0    50   Input ~ 0
GND
$Comp
L LM358 U1
U 2 1 59A07DF0
P 2250 5800
F 0 "U1" H 2250 6000 50  0000 L CNN
F 1 "LM358" H 2250 5600 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 2250 5800 50  0001 C CNN
F 3 "" H 2250 5800 50  0001 C CNN
	2    2250 5800
	1    0    0    -1  
$EndComp
Text HLabel 1550 3600 1    50   Output ~ 0
VCC
Text HLabel 2150 4500 3    50   Input ~ 0
GND
Text HLabel 2150 6250 3    50   Input ~ 0
GND
Text HLabel 2150 5000 1    50   Output ~ 0
VCC
$Comp
L RES R66
U 1 1 59A9C534
P 8050 1200
F 0 "R66" H 8050 1300 50  0000 C CNN
F 1 "2.2K" H 8050 1200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 8050 1130 30  0001 C CNN
F 3 "" V 8050 1200 30  0000 C CNN
	1    8050 1200
	0    1    1    0   
$EndComp
$Comp
L RES R88
U 1 1 59A9C602
P 9200 1200
F 0 "R88" H 9200 1300 50  0000 C CNN
F 1 "2.2K" H 9200 1200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9200 1130 30  0001 C CNN
F 3 "" V 9200 1200 30  0000 C CNN
	1    9200 1200
	0    1    1    0   
$EndComp
Text HLabel 7800 1600 0    50   Input ~ 0
+3.3V
$Comp
L 4051 IC16
U 1 1 59A82AB6
P 9350 2850
F 0 "IC16" H 8950 3350 50  0000 L CNN
F 1 "4051" H 8950 2350 50  0000 L CNN
F 2 "Packages_DIP:DIP-16_300" H 9800 2650 60  0001 C CNN
F 3 "" H 9800 2650 60  0000 C CNN
	1    9350 2850
	1    0    0    -1  
$EndComp
Text Label 5950 800  2    60   ~ 0
3.3V_Test
Text Label 5950 1150 2    50   ~ 0
VCC_Test
Text Label 5950 1500 2    50   ~ 0
+5V_Test
$Comp
L RES R92
U 1 1 59A84F65
P 1550 4400
F 0 "R92" H 1550 4500 50  0000 C CNN
F 1 "100K" H 1550 4400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1550 4330 30  0001 C CNN
F 3 "" V 1550 4400 30  0000 C CNN
	1    1550 4400
	0    1    1    0   
$EndComp
$Comp
L RES R91
U 1 1 59A84FE8
P 1550 3900
F 0 "R91" H 1550 4000 50  0000 C CNN
F 1 "4.7K" H 1550 3900 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1550 3830 30  0001 C CNN
F 3 "" V 1550 3900 30  0000 C CNN
	1    1550 3900
	0    1    1    0   
$EndComp
$Comp
L RES R90
U 1 1 59A85AEE
P 1450 6150
F 0 "R90" H 1450 6250 50  0000 C CNN
F 1 "100K" H 1450 6150 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1450 6080 30  0001 C CNN
F 3 "" V 1450 6150 30  0000 C CNN
	1    1450 6150
	0    1    1    0   
$EndComp
$Comp
L RES R89
U 1 1 59A85AF4
P 1450 5650
F 0 "R89" H 1450 5750 50  0000 C CNN
F 1 "4.7K" H 1450 5650 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 1450 5580 30  0001 C CNN
F 3 "" V 1450 5650 30  0000 C CNN
	1    1450 5650
	0    1    1    0   
$EndComp
Text Label 1050 3900 2    50   ~ 0
VCC_Test
Text Label 850  5700 2    50   ~ 0
+5V_Test
$Comp
L LM358 U2
U 2 1 59A87AF4
P 4350 4000
F 0 "U2" H 4350 4200 50  0000 L CNN
F 1 "LM358" H 4350 3800 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 4350 4000 50  0001 C CNN
F 3 "" H 4350 4000 50  0001 C CNN
	2    4350 4000
	1    0    0    -1  
$EndComp
$Comp
L LM358 U2
U 1 1 59A87B6F
P 4300 5900
F 0 "U2" H 4300 6100 50  0000 L CNN
F 1 "LM358" H 4300 5700 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 4300 5900 50  0001 C CNN
F 3 "" H 4300 5900 50  0001 C CNN
	1    4300 5900
	1    0    0    -1  
$EndComp
$Comp
L RES R96
U 1 1 59A88770
P 3650 4400
F 0 "R96" H 3650 4500 50  0000 C CNN
F 1 "100K" H 3650 4400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 3650 4330 30  0001 C CNN
F 3 "" V 3650 4400 30  0000 C CNN
	1    3650 4400
	0    1    1    0   
$EndComp
$Comp
L RES R95
U 1 1 59A88776
P 3650 3900
F 0 "R95" H 3650 4000 50  0000 C CNN
F 1 "4.7K" H 3650 3900 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 3650 3830 30  0001 C CNN
F 3 "" V 3650 3900 30  0000 C CNN
	1    3650 3900
	0    1    1    0   
$EndComp
Text HLabel 2800 3550 2    50   Input ~ 0
+3.3V
Text HLabel 4300 4700 3    50   Input ~ 0
GND
$Comp
L RES R94
U 1 1 59A89813
P 3600 6300
F 0 "R94" H 3600 6400 50  0000 C CNN
F 1 "1K" H 3600 6300 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 3600 6230 30  0001 C CNN
F 3 "" V 3600 6300 30  0000 C CNN
	1    3600 6300
	0    1    1    0   
$EndComp
$Comp
L RES R93
U 1 1 59A89819
P 3600 5800
F 0 "R93" H 3600 5900 50  0000 C CNN
F 1 "1K" H 3600 5800 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 3600 5730 30  0001 C CNN
F 3 "" V 3600 5800 30  0000 C CNN
	1    3600 5800
	0    1    1    0   
$EndComp
Text HLabel 4200 6800 3    50   Input ~ 0
GND
$Comp
L LM358 U3
U 2 1 59A8B57F
P 6550 4000
F 0 "U3" H 6550 4200 50  0000 L CNN
F 1 "LM358" H 6550 3800 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 6550 4000 50  0001 C CNN
F 3 "" H 6550 4000 50  0001 C CNN
	2    6550 4000
	1    0    0    -1  
$EndComp
$Comp
L LM358 U3
U 1 1 59A8B585
P 6500 5800
F 0 "U3" H 6500 6000 50  0000 L CNN
F 1 "LM358" H 6500 5600 50  0000 L CNN
F 2 "Packages_SOIC:SOIC-8" H 6500 5800 50  0001 C CNN
F 3 "" H 6500 5800 50  0001 C CNN
	1    6500 5800
	1    0    0    -1  
$EndComp
$Comp
L RES R100
U 1 1 59A8B58B
P 5850 4400
F 0 "R100" H 5850 4500 50  0000 C CNN
F 1 "1K" H 5850 4400 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5850 4330 30  0001 C CNN
F 3 "" V 5850 4400 30  0000 C CNN
	1    5850 4400
	0    1    1    0   
$EndComp
$Comp
L RES R99
U 1 1 59A8B591
P 5850 3900
F 0 "R99" H 5850 4000 50  0000 C CNN
F 1 "1K" H 5850 3900 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5850 3830 30  0001 C CNN
F 3 "" V 5850 3900 30  0000 C CNN
	1    5850 3900
	0    1    1    0   
$EndComp
Text HLabel 3650 3450 1    50   Input ~ 0
+3.3V
Text HLabel 6500 4700 3    50   Input ~ 0
GND
$Comp
L RES R98
U 1 1 59A8B5AA
P 5800 6200
F 0 "R98" H 5800 6300 50  0000 C CNN
F 1 "100K" H 5800 6200 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5800 6130 30  0001 C CNN
F 3 "" V 5800 6200 30  0000 C CNN
	1    5800 6200
	0    1    1    0   
$EndComp
$Comp
L RES R97
U 1 1 59A8B5B0
P 5800 5700
F 0 "R97" H 5800 5800 50  0000 C CNN
F 1 "4.7K" H 5800 5700 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 5800 5630 30  0001 C CNN
F 3 "" V 5800 5700 30  0000 C CNN
	1    5800 5700
	0    1    1    0   
$EndComp
Text HLabel 6400 6700 3    50   Input ~ 0
GND
Text Label 3150 3900 0    60   ~ 0
3.3V_Test
Text Label 2750 4000 3    50   ~ 0
VCC_TEST_M
Text Label 2900 5800 2    50   ~ 0
+5V_M
Text Label 5050 4000 2    50   ~ 0
+3.3V_M
Text Label 4350 950  2    50   ~ 0
Vbat_F
Text Label 4350 1300 2    50   ~ 0
USB_Plug
Text Label 3100 5800 0    50   ~ 0
Vbat_F
Text Label 5350 3900 0    50   ~ 0
USB_Plug
Text Label 5050 5900 2    50   ~ 0
Vbat_M
Text Label 7250 4000 2    50   ~ 0
USB_M
Text Label 8500 2500 2    50   ~ 0
+3.3V_M
Text Label 8500 2600 2    50   ~ 0
VCC_TEST_M
Text Label 8500 2700 2    50   ~ 0
+5V_M
Text Label 8500 2800 2    50   ~ 0
Vbat_F
Text HLabel 10700 1850 1    50   Output ~ 0
VCC
Text HLabel 10700 4650 3    50   Input ~ 0
GND
NoConn ~ 8500 3200
Text HLabel 9900 2700 2    50   Input ~ 0
PC13
Text HLabel 9900 2800 2    50   Input ~ 0
PC14
Text HLabel 9900 2900 2    50   Input ~ 0
PC15
Text HLabel 10200 2600 2    50   Input ~ 0
PA4
Wire Wire Line
	1100 1000 1600 1000
Wire Wire Line
	1100 1350 1600 1350
Wire Wire Line
	1100 1650 1600 1650
Wire Wire Line
	1100 2000 1600 2000
Wire Wire Line
	1100 2350 1600 2350
Wire Notes Line
	600  700  2100 700 
Wire Notes Line
	2100 700  2100 2650
Wire Notes Line
	2100 2650 600  2650
Wire Notes Line
	600  2650 600  700 
Wire Wire Line
	3250 950  2750 950 
Wire Wire Line
	3250 1300 2750 1300
Wire Wire Line
	3250 1600 2750 1600
Wire Notes Line
	2300 700  3650 700 
Wire Notes Line
	3650 700  3650 2650
Wire Notes Line
	3650 2650 2300 2650
Wire Notes Line
	2300 2650 2300 700 
Wire Notes Line
	3800 700  5250 700 
Wire Notes Line
	5250 700  5250 2300
Wire Notes Line
	5250 2300 3800 2300
Wire Notes Line
	3800 2300 3800 700 
Wire Wire Line
	4850 950  4350 950 
Wire Wire Line
	4850 1300 4350 1300
Wire Wire Line
	4850 1650 4350 1650
Wire Wire Line
	4850 2000 4350 2000
Wire Notes Line
	5650 2150 7400 2150
Wire Notes Line
	7400 2150 7400 2850
Wire Notes Line
	7400 2850 5650 2850
Wire Notes Line
	5650 2850 5650 2150
Wire Notes Line
	7350 650  9650 650 
Wire Notes Line
	9650 650  9650 2000
Wire Notes Line
	9650 2000 7350 2000
Wire Notes Line
	7350 2000 7350 650 
Wire Wire Line
	6900 2350 6450 2350
Wire Wire Line
	6900 2700 6450 2700
Wire Wire Line
	8250 950  7800 950 
Wire Wire Line
	9350 950  8900 950 
Wire Wire Line
	6350 800  5950 800 
Wire Wire Line
	6350 1150 5950 1150
Wire Wire Line
	6350 1500 5950 1500
Wire Wire Line
	6350 1900 5950 1900
Wire Wire Line
	1050 3900 1950 3900
Wire Wire Line
	850  5700 1950 5700
Wire Wire Line
	2550 5800 2900 5800
Wire Wire Line
	2550 4000 2750 4000
Wire Wire Line
	2150 6100 2150 6250
Wire Wire Line
	2150 4300 2150 4500
Wire Wire Line
	7800 1600 9200 1600
Wire Wire Line
	9200 1600 9200 1400
Wire Wire Line
	8050 1400 8050 1600
Connection ~ 8050 1600
Wire Wire Line
	8050 1000 8050 950 
Connection ~ 8050 950 
Wire Wire Line
	9200 1000 9200 950 
Connection ~ 9200 950 
Wire Wire Line
	1550 4100 1550 4200
Wire Wire Line
	1550 4600 1550 4700
Wire Wire Line
	1550 4700 1850 4700
Wire Wire Line
	1850 4700 1850 4400
Wire Wire Line
	1850 4400 2150 4400
Connection ~ 2150 4400
Wire Wire Line
	1550 3700 1550 3600
Wire Wire Line
	1450 5850 1450 5950
Wire Wire Line
	1450 6350 1450 6450
Wire Wire Line
	1450 5450 1450 5350
Wire Wire Line
	1450 6450 1850 6450
Wire Wire Line
	1850 6450 1850 6150
Wire Wire Line
	1850 6150 2150 6150
Connection ~ 2150 6150
Wire Wire Line
	1450 5900 1950 5900
Connection ~ 1450 5900
Wire Wire Line
	1550 4150 1850 4150
Wire Wire Line
	1850 4150 1850 4100
Wire Wire Line
	1850 4100 1950 4100
Connection ~ 1550 4150
Wire Wire Line
	3150 3900 4050 3900
Wire Wire Line
	3650 4100 3650 4200
Wire Wire Line
	3650 4150 3950 4150
Connection ~ 3650 4150
Wire Wire Line
	3650 3450 3650 3700
Wire Wire Line
	3950 4150 3950 4100
Wire Wire Line
	3950 4100 4050 4100
Wire Wire Line
	4250 4300 4250 4600
Wire Wire Line
	4250 4600 4300 4600
Wire Wire Line
	4300 4600 4300 4700
Wire Wire Line
	3650 4600 3650 4700
Wire Wire Line
	3650 4700 3950 4700
Wire Wire Line
	3950 4700 3950 4500
Wire Wire Line
	3950 4500 4250 4500
Connection ~ 4250 4500
Wire Wire Line
	3100 5800 4000 5800
Wire Wire Line
	3600 6000 3600 6100
Wire Wire Line
	3600 6050 3900 6050
Connection ~ 3600 6050
Wire Wire Line
	3600 6500 3600 6600
Wire Wire Line
	3600 6600 4200 6600
Wire Wire Line
	4000 6000 3900 6000
Wire Wire Line
	3900 6000 3900 6050
Wire Wire Line
	4200 6200 4200 6800
Connection ~ 4200 6600
Wire Wire Line
	4600 5900 5050 5900
Wire Wire Line
	4650 4000 5050 4000
Wire Wire Line
	5350 3900 6250 3900
Wire Wire Line
	5850 4100 5850 4200
Wire Wire Line
	5850 4150 6150 4150
Connection ~ 5850 4150
Wire Wire Line
	5850 3450 5850 3700
Wire Wire Line
	6150 4150 6150 4100
Wire Wire Line
	6150 4100 6250 4100
Wire Wire Line
	6450 4300 6450 4600
Wire Wire Line
	6450 4600 6500 4600
Wire Wire Line
	6500 4600 6500 4700
Wire Wire Line
	5850 4600 5850 4700
Wire Wire Line
	5850 4700 6150 4700
Wire Wire Line
	6150 4700 6150 4500
Wire Wire Line
	6150 4500 6450 4500
Connection ~ 6450 4500
Wire Wire Line
	5300 5700 6200 5700
Wire Wire Line
	5800 5900 5800 6000
Wire Wire Line
	5800 5950 6100 5950
Connection ~ 5800 5950
Wire Wire Line
	5800 6400 5800 6500
Wire Wire Line
	5800 6500 6400 6500
Wire Wire Line
	6200 5900 6100 5900
Wire Wire Line
	6100 5900 6100 5950
Wire Wire Line
	6400 6100 6400 6700
Connection ~ 6400 6500
Wire Wire Line
	6800 5800 7250 5800
Wire Wire Line
	6850 4000 7250 4000
Wire Notes Line
	350  3050 5150 3050
Wire Notes Line
	5150 3050 5150 4950
Wire Notes Line
	5150 4950 2950 4950
Wire Notes Line
	2950 4950 2950 6700
Wire Notes Line
	2950 6700 350  6700
Wire Notes Line
	350  6700 350  3050
Wire Notes Line
	3000 5000 3000 7100
Wire Notes Line
	3000 7100 6900 7100
Wire Notes Line
	6900 7100 6900 6100
Wire Notes Line
	5200 3050 5200 5000
Wire Notes Line
	5200 5000 3000 5000
Wire Wire Line
	8800 2500 8500 2500
Wire Wire Line
	8800 2600 8500 2600
Wire Wire Line
	8800 2700 8500 2700
Wire Wire Line
	8800 2800 8500 2800
Wire Wire Line
	8800 2900 8500 2900
Wire Wire Line
	8800 3000 8500 3000
Wire Wire Line
	8800 3100 8500 3100
Wire Wire Line
	8800 3200 8500 3200
Wire Wire Line
	9900 2500 10700 2500
Wire Wire Line
	10700 2500 10700 1850
Wire Wire Line
	9900 3000 10700 3000
Wire Wire Line
	10700 3000 10700 4650
Wire Wire Line
	9900 3100 10700 3100
Connection ~ 10700 3100
Wire Wire Line
	9900 3200 10700 3200
Connection ~ 10700 3200
Wire Wire Line
	9900 2600 10200 2600
$Comp
L RES R103
U 1 1 59A9ECE2
P 6250 2350
F 0 "R103" H 6250 2450 50  0000 C CNN
F 1 "220" H 6250 2350 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6250 2280 30  0001 C CNN
F 3 "" V 6250 2350 30  0000 C CNN
	1    6250 2350
	-1   0    0    1   
$EndComp
$Comp
L RES R104
U 1 1 59A9EEA9
P 6250 2700
F 0 "R104" H 6250 2800 50  0000 C CNN
F 1 "220" H 6250 2700 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 6250 2630 30  0001 C CNN
F 3 "" V 6250 2700 30  0000 C CNN
	1    6250 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 2350 6050 2350
Wire Wire Line
	5950 2700 6050 2700
Wire Wire Line
	3600 5600 3600 5500
Wire Notes Line
	5200 3050 7800 3050
Wire Notes Line
	6900 6100 7800 6100
Text HLabel 4350 1650 0    50   Input ~ 0
VBAT_CONV
Text Label 1100 1000 2    50   ~ 0
VLCD
Wire Notes Line
	7800 6100 7800 3050
Text Label 8500 3000 2    50   ~ 0
VLCD_M
Text Notes 5350 7050 2    50   ~ 0
VBAT_USB Tester
Text Notes 1900 6850 2    50   ~ 0
Power Tester
Text Notes 3550 7350 0    50   ~ 0
vbat full 1.7V\nVusbplug 1.7
Text Label 8500 2900 2    50   ~ 0
USB_Plug
NoConn ~ 8500 3100
Text HLabel 4350 2000 0    50   Input ~ 0
VBT_ADC
Text Label 5300 5700 2    50   ~ 0
VLCD
Text Label 7250 5800 2    50   ~ 0
VLCD_M
Text HLabel 3600 5500 1    50   Input ~ 0
+3.3V
Wire Wire Line
	5800 5500 5800 5400
Text HLabel 5850 3450 1    50   Input ~ 0
+3.3V
Text HLabel 7150 5100 1    50   Output ~ 0
VCC
Text HLabel 5800 5400 1    50   Output ~ 0
VCC
Text HLabel 7250 5400 2    50   Input ~ 0
+3.3V
Text HLabel 4850 5500 2    50   Input ~ 0
+3.3V
$Comp
L Jumper_NC_Dual JP3
U 1 1 59B95E11
P 2350 3450
F 0 "JP3" H 2400 3350 50  0000 L CNN
F 1 "Jumper_NC_Dual" H 2350 3550 50  0000 C BNN
F 2 "Jumpers:Jumper_3-way_SMD" H 2350 3450 50  0001 C CNN
F 3 "" H 2350 3450 50  0001 C CNN
	1    2350 3450
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Dual JP4
U 1 1 59B95EAA
P 4500 5350
F 0 "JP4" H 4550 5250 50  0000 L CNN
F 1 "Jumper_NC_Dual" H 4500 5450 50  0000 C BNN
F 2 "Jumpers:Jumper_3-way_SMD" H 4500 5350 50  0001 C CNN
F 3 "" H 4500 5350 50  0001 C CNN
	1    4500 5350
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Dual JP5
U 1 1 59B96560
P 6650 5250
F 0 "JP5" H 6700 5150 50  0000 L CNN
F 1 "Jumper_NC_Dual" H 6650 5350 50  0000 C BNN
F 2 "Jumpers:Jumper_3-way_SMD" H 6650 5250 50  0001 C CNN
F 3 "" H 6650 5250 50  0001 C CNN
	1    6650 5250
	1    0    0    -1  
$EndComp
Text HLabel 4950 5250 1    50   Output ~ 0
VCC
Wire Wire Line
	6900 5250 7150 5250
Wire Wire Line
	7150 5250 7150 5100
Wire Wire Line
	4750 5350 4950 5350
Wire Wire Line
	4950 5350 4950 5250
Text HLabel 1450 5350 1    50   Output ~ 0
VCC
Text HLabel 2750 3350 1    50   Output ~ 0
VCC
Wire Wire Line
	2600 3450 2750 3450
Wire Wire Line
	2750 3450 2750 3350
$Comp
L Jumper JP6
U 1 1 59BA1AC1
P 2800 1950
F 0 "JP6" H 2800 2100 50  0000 C CNN
F 1 "Jumper" H 2800 1870 50  0000 C CNN
F 2 "Jumpers:Jumper_2-way_SMD" H 2800 1950 50  0001 C CNN
F 3 "" H 2800 1950 50  0001 C CNN
	1    2800 1950
	1    0    0    -1  
$EndComp
$Comp
L Jumper JP7
U 1 1 59BA1B52
P 2800 2300
F 0 "JP7" H 2800 2450 50  0000 C CNN
F 1 "Jumper" H 2800 2220 50  0000 C CNN
F 2 "Jumpers:Jumper_2-way_SMD" H 2800 2300 50  0001 C CNN
F 3 "" H 2800 2300 50  0001 C CNN
	1    2800 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1950 3100 1950
Wire Wire Line
	3250 2300 3100 2300
Text HLabel 2400 2300 0    50   Input ~ 0
RX
Text HLabel 2400 1950 0    50   Input ~ 0
TX
Wire Wire Line
	2500 2300 2400 2300
Wire Wire Line
	2500 1950 2400 1950
Wire Wire Line
	2150 3700 2150 3650
Wire Wire Line
	2150 3650 2000 3650
Wire Wire Line
	2000 3650 2000 3450
Wire Wire Line
	2000 3450 2100 3450
Wire Wire Line
	2350 3550 2350 3600
Wire Wire Line
	2350 3600 2650 3600
Wire Wire Line
	2650 3600 2650 3550
Wire Wire Line
	2650 3550 2800 3550
Wire Wire Line
	4250 5350 4200 5350
Wire Wire Line
	4200 5350 4200 5600
Wire Wire Line
	4850 5500 4500 5500
Wire Wire Line
	4500 5500 4500 5450
Wire Wire Line
	7250 5400 6650 5400
Wire Wire Line
	6650 5400 6650 5350
Wire Wire Line
	6400 5250 6400 5500
$Comp
L TEST-RESCUE-Remote_tester TP50
U 1 1 59BB332D
P 8350 4100
AR Path="/59BB332D" Ref="TP50"  Part="1" 
AR Path="/59A71036/59BB332D" Ref="TP50"  Part="1" 
F 0 "TP50" H 8350 4200 50  0000 C CNN
F 1 "TEST" H 8350 4000 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 8350 4000 10  0001 C CNN
F 3 "" H 8550 4100 60  0000 C CNN
	1    8350 4100
	0    -1   -1   0   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP52
U 1 1 59BB33FE
P 8750 4100
AR Path="/59BB33FE" Ref="TP52"  Part="1" 
AR Path="/59A71036/59BB33FE" Ref="TP52"  Part="1" 
F 0 "TP52" H 8750 4200 50  0000 C CNN
F 1 "TEST" H 8750 4000 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 8750 4000 10  0001 C CNN
F 3 "" H 8950 4100 60  0000 C CNN
	1    8750 4100
	0    -1   -1   0   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP54
U 1 1 59BB3C9F
P 9100 4100
AR Path="/59BB3C9F" Ref="TP54"  Part="1" 
AR Path="/59A71036/59BB3C9F" Ref="TP54"  Part="1" 
F 0 "TP54" H 9100 4200 50  0000 C CNN
F 1 "TEST" H 9100 4000 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 9100 4000 10  0001 C CNN
F 3 "" H 9300 4100 60  0000 C CNN
	1    9100 4100
	0    -1   -1   0   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP56
U 1 1 59BB3E0A
P 9500 4100
AR Path="/59BB3E0A" Ref="TP56"  Part="1" 
AR Path="/59A71036/59BB3E0A" Ref="TP56"  Part="1" 
F 0 "TP56" H 9500 4200 50  0000 C CNN
F 1 "TEST" H 9500 4000 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 9500 4000 10  0001 C CNN
F 3 "" H 9700 4100 60  0000 C CNN
	1    9500 4100
	0    -1   -1   0   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP58
U 1 1 59BB3E10
P 9800 4100
AR Path="/59BB3E10" Ref="TP58"  Part="1" 
AR Path="/59A71036/59BB3E10" Ref="TP58"  Part="1" 
F 0 "TP58" H 9800 4200 50  0000 C CNN
F 1 "TEST" H 9800 4000 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 9800 4000 10  0001 C CNN
F 3 "" H 10000 4100 60  0000 C CNN
	1    9800 4100
	0    -1   -1   0   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP60
U 1 1 59BB3E16
P 10200 4100
AR Path="/59BB3E16" Ref="TP60"  Part="1" 
AR Path="/59A71036/59BB3E16" Ref="TP60"  Part="1" 
F 0 "TP60" H 10200 4200 50  0000 C CNN
F 1 "TEST" H 10200 4000 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 10200 4000 10  0001 C CNN
F 3 "" H 10400 4100 60  0000 C CNN
	1    10200 4100
	0    -1   -1   0   
$EndComp
$Comp
L RES R105
U 1 1 59BB44AA
P 8350 4450
F 0 "R105" H 8350 4550 50  0000 C CNN
F 1 "RES" H 8350 4450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 8350 4380 30  0001 C CNN
F 3 "" V 8350 4450 30  0000 C CNN
	1    8350 4450
	0    1    1    0   
$EndComp
$Comp
L RES R106
U 1 1 59BB456B
P 8350 5050
F 0 "R106" H 8350 5150 50  0000 C CNN
F 1 "RES" H 8350 5050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 8350 4980 30  0001 C CNN
F 3 "" V 8350 5050 30  0000 C CNN
	1    8350 5050
	0    1    1    0   
$EndComp
$Comp
L RES R107
U 1 1 59BB4630
P 8750 4450
F 0 "R107" H 8750 4550 50  0000 C CNN
F 1 "RES" H 8750 4450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 8750 4380 30  0001 C CNN
F 3 "" V 8750 4450 30  0000 C CNN
	1    8750 4450
	0    1    1    0   
$EndComp
$Comp
L RES R108
U 1 1 59BB474F
P 8750 5050
F 0 "R108" H 8750 5150 50  0000 C CNN
F 1 "RES" H 8750 5050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 8750 4980 30  0001 C CNN
F 3 "" V 8750 5050 30  0000 C CNN
	1    8750 5050
	0    1    1    0   
$EndComp
$Comp
L RES R109
U 1 1 59BB48CE
P 9100 4450
F 0 "R109" H 9100 4550 50  0000 C CNN
F 1 "RES" H 9100 4450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9100 4380 30  0001 C CNN
F 3 "" V 9100 4450 30  0000 C CNN
	1    9100 4450
	0    1    1    0   
$EndComp
$Comp
L RES R110
U 1 1 59BB48D4
P 9100 5050
F 0 "R110" H 9100 5150 50  0000 C CNN
F 1 "RES" H 9100 5050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9100 4980 30  0001 C CNN
F 3 "" V 9100 5050 30  0000 C CNN
	1    9100 5050
	0    1    1    0   
$EndComp
$Comp
L RES R111
U 1 1 59BB48DA
P 9500 4450
F 0 "R111" H 9500 4550 50  0000 C CNN
F 1 "RES" H 9500 4450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9500 4380 30  0001 C CNN
F 3 "" V 9500 4450 30  0000 C CNN
	1    9500 4450
	0    1    1    0   
$EndComp
$Comp
L RES R112
U 1 1 59BB48E0
P 9500 5050
F 0 "R112" H 9500 5150 50  0000 C CNN
F 1 "RES" H 9500 5050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9500 4980 30  0001 C CNN
F 3 "" V 9500 5050 30  0000 C CNN
	1    9500 5050
	0    1    1    0   
$EndComp
$Comp
L RES R113
U 1 1 59BB4AC2
P 9800 4450
F 0 "R113" H 9800 4550 50  0000 C CNN
F 1 "RES" H 9800 4450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9800 4380 30  0001 C CNN
F 3 "" V 9800 4450 30  0000 C CNN
	1    9800 4450
	0    1    1    0   
$EndComp
$Comp
L RES R114
U 1 1 59BB4AC8
P 9800 5050
F 0 "R114" H 9800 5150 50  0000 C CNN
F 1 "RES" H 9800 5050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 9800 4980 30  0001 C CNN
F 3 "" V 9800 5050 30  0000 C CNN
	1    9800 5050
	0    1    1    0   
$EndComp
$Comp
L RES R115
U 1 1 59BB4ACE
P 10200 4450
F 0 "R115" H 10200 4550 50  0000 C CNN
F 1 "RES" H 10200 4450 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 10200 4380 30  0001 C CNN
F 3 "" V 10200 4450 30  0000 C CNN
	1    10200 4450
	0    1    1    0   
$EndComp
$Comp
L RES R116
U 1 1 59BB4AD4
P 10200 5050
F 0 "R116" H 10200 5150 50  0000 C CNN
F 1 "RES" H 10200 5050 50  0000 C CNN
F 2 "Resistors:R_Chip_SMD_0603" H 10200 4980 30  0001 C CNN
F 3 "" V 10200 5050 30  0000 C CNN
	1    10200 5050
	0    1    1    0   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP49
U 1 1 59BB4CD6
P 8150 4750
AR Path="/59BB4CD6" Ref="TP49"  Part="1" 
AR Path="/59A71036/59BB4CD6" Ref="TP49"  Part="1" 
F 0 "TP49" H 8150 4850 50  0000 C CNN
F 1 "TEST" H 8150 4650 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 8150 4650 10  0001 C CNN
F 3 "" H 8350 4750 60  0000 C CNN
	1    8150 4750
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP51
U 1 1 59BB4DDE
P 8600 4750
AR Path="/59BB4DDE" Ref="TP51"  Part="1" 
AR Path="/59A71036/59BB4DDE" Ref="TP51"  Part="1" 
F 0 "TP51" H 8600 4850 50  0000 C CNN
F 1 "TEST" H 8600 4650 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 8600 4650 10  0001 C CNN
F 3 "" H 8800 4750 60  0000 C CNN
	1    8600 4750
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP53
U 1 1 59BB4ECE
P 9000 4750
AR Path="/59BB4ECE" Ref="TP53"  Part="1" 
AR Path="/59A71036/59BB4ECE" Ref="TP53"  Part="1" 
F 0 "TP53" H 9000 4850 50  0000 C CNN
F 1 "TEST" H 9000 4650 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 9000 4650 10  0001 C CNN
F 3 "" H 9200 4750 60  0000 C CNN
	1    9000 4750
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP55
U 1 1 59BB4FC5
P 9400 4750
AR Path="/59BB4FC5" Ref="TP55"  Part="1" 
AR Path="/59A71036/59BB4FC5" Ref="TP55"  Part="1" 
F 0 "TP55" H 9400 4850 50  0000 C CNN
F 1 "TEST" H 9400 4650 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 9400 4650 10  0001 C CNN
F 3 "" H 9600 4750 60  0000 C CNN
	1    9400 4750
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP57
U 1 1 59BB50B9
P 9700 4750
AR Path="/59BB50B9" Ref="TP57"  Part="1" 
AR Path="/59A71036/59BB50B9" Ref="TP57"  Part="1" 
F 0 "TP57" H 9700 4850 50  0000 C CNN
F 1 "TEST" H 9700 4650 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 9700 4650 10  0001 C CNN
F 3 "" H 9900 4750 60  0000 C CNN
	1    9700 4750
	-1   0    0    1   
$EndComp
$Comp
L TEST-RESCUE-Remote_tester TP59
U 1 1 59BB51B4
P 10150 4750
AR Path="/59BB51B4" Ref="TP59"  Part="1" 
AR Path="/59A71036/59BB51B4" Ref="TP59"  Part="1" 
F 0 "TP59" H 10150 4850 50  0000 C CNN
F 1 "TEST" H 10150 4650 50  0000 C CNN
F 2 "Measurement_Points:Testpoint_SMD" H 10150 4650 10  0001 C CNN
F 3 "" H 10350 4750 60  0000 C CNN
	1    10150 4750
	-1   0    0    1   
$EndComp
Text HLabel 9200 5750 3    50   Input ~ 0
GND
Wire Wire Line
	8350 4100 8350 4250
Wire Wire Line
	8750 4100 8750 4250
Wire Wire Line
	9100 4100 9100 4250
Wire Wire Line
	9500 4100 9500 4250
Wire Wire Line
	9800 4100 9800 4250
Wire Wire Line
	10200 4100 10200 4250
Wire Wire Line
	8350 4650 8350 4850
Wire Wire Line
	8750 4650 8750 4850
Wire Wire Line
	9100 4650 9100 4850
Wire Wire Line
	9500 4650 9500 4850
Wire Wire Line
	9800 4650 9800 4850
Wire Wire Line
	10200 4650 10200 4850
Wire Wire Line
	10150 4750 10200 4750
Connection ~ 10200 4750
Wire Wire Line
	9700 4750 9800 4750
Connection ~ 9800 4750
Wire Wire Line
	9400 4750 9500 4750
Connection ~ 9500 4750
Wire Wire Line
	9000 4750 9100 4750
Connection ~ 9100 4750
Wire Wire Line
	8600 4750 8750 4750
Connection ~ 8750 4750
Wire Wire Line
	8150 4750 8350 4750
Connection ~ 8350 4750
Wire Wire Line
	8350 5250 8350 5500
Wire Wire Line
	8350 5500 10200 5500
Wire Wire Line
	10200 5500 10200 5250
Wire Wire Line
	9800 5250 9800 5500
Connection ~ 9800 5500
Wire Wire Line
	9500 5250 9500 5500
Connection ~ 9500 5500
Wire Wire Line
	9100 5250 9100 5500
Connection ~ 9100 5500
Wire Wire Line
	8750 5250 8750 5500
Connection ~ 8750 5500
Wire Wire Line
	9200 5750 9200 5500
Connection ~ 9200 5500
$EndSCHEMATC
